/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.ObjectLargeArray;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class TimeDataTest
{

    public TimeDataTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testTimeDataCloning()
    {
        FloatLargeArray t0 = new FloatLargeArray(3);
        t0.set(0, 0.0f);
        t0.set(1, 1.0f);
        t0.set(2, 2.0f);
        FloatLargeArray t1 = new FloatLargeArray(3);
        t1.set(0, 10.0f);
        t1.set(1, 11.0f);
        t1.set(2, 12.0f);

        TimeData td = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        td.setValue(t0, 0.0f);
        td.setValue(t1, 1.0f);
        td.setCurrentTime(0.5f);
        //get expected results
        LargeArray expectedResult = (LargeArray) td.getCurrentValue().clone();
        float expectedCurrentTime = td.getCurrentTime();

        //clone
        TimeData tdShallowClone = td.cloneShallow();
        TimeData tdDeepClone = td.cloneDeep();

        //change sth in original to check if changes are not propagating to clone
        td.setCurrentTime(0);

        //compare results for shallow clone
        assertEquals(expectedCurrentTime, tdShallowClone.getCurrentTime(), 0.001);
        LargeArray shallowResult = (LargeArray) tdShallowClone.getCurrentValue().clone();
        for (long i = 0; i < expectedResult.length(); i++) {
            assertEquals(expectedResult.get(i), shallowResult.get(i));
        }

        //compare results for deep clone
        assertEquals(expectedCurrentTime, tdDeepClone.getCurrentTime(), 0.001);
        LargeArray deepResult = (LargeArray) tdDeepClone.getCurrentValue().clone();
        for (long i = 0; i < expectedResult.length(); i++) {
            assertEquals(expectedResult.get(i), deepResult.get(i));
        }

    }
    
    @Test
    public void testGetSetUserData()
    {
        FloatLargeArray t0 = new FloatLargeArray(new float[] {1, 2, 3});
        String[] ud0 = new String[] {"1", "2", "3"};
        FloatLargeArray t1 = new FloatLargeArray(new float[] {10, 20, 30});
        String[] ud1 = new String[] {"10", "20", "30"};
        ArrayList<String[]> ud = new ArrayList<>(2);
        ud.add(ud0);
        ud.add(ud1);        

        TimeData td = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        td.setValue(t0, 0.0f);
        td.setValue(t1, 1.0f);
        td.setUserData(ud);
        
        String[] expectedResult0 = td.getUserData(0.0f);
        String[] expectedResult1 = td.getUserData(1.0f);

        assertArrayEquals(expectedResult0, ud0); 
        assertArrayEquals(expectedResult1, ud1); 
        
        Throwable e = null;
        ObjectLargeArray ola = new ObjectLargeArray(1,LargeArray.DEFAULT_MAX_OBJECT_SIZE, new String("foo")); 
        td = new TimeData(DataArrayType.FIELD_DATA_OBJECT);
        try {
            td.setValue(ola, 0.0f);
        } catch (Throwable ex) {
            e = ex;
        }
        assertTrue(e instanceof IllegalArgumentException);
    }
}
