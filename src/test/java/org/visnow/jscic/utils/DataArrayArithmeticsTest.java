/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.ArrayList;
import java.util.Collection;
import org.apache.commons.math3.util.FastMath;
import static org.apache.commons.math3.util.FastMath.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayArithmetics;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.ComplexDataArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.visnow.jlargearrays.ConcurrencyUtils;
import static org.visnow.jlargearrays.FloatingPointAsserts.*;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
@RunWith(value = Parameterized.class)
public class DataArrayArithmeticsTest
{

    @Parameterized.Parameters
    public static Collection<Object[]> getParameters()
    {
        final int[] threads = {1, 2, 8};

        final ArrayList<Object[]> parameters = new ArrayList<>();
        for (int i = 0; i < threads.length; i++) {
            parameters.add(new Object[]{threads[i]});
        }
        return parameters;
    }

    public DataArrayArithmeticsTest(int nthreads)
    {
        ConcurrencyUtils.setNumberOfThreads(nthreads);
        if (nthreads > 1) {
            ConcurrencyUtils.setConcurrentThreshold(1);
        }
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testAddF()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_SHORT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.addF(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, n), 1, "da2");
        res = DataArrayArithmetics.addF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.addF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[v], res.getFloatElement(i)[v]);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.addF(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[v], res.getFloatElement(i)[v]);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.addF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1c.getFloatRealArray().getFloat(i * 3 + v) + da2.getFloatElement(i)[v], resc.getFloatRealArray().getFloat(i * 3 + v));
                assertRelativeEquals(da1c.getFloatImaginaryArray().getFloat(i * 3 + v), resc.getFloatImaginaryArray().getFloat(i * 3 + v));
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.addF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToFloat();
        td2 = td2.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(a.getFloat(i) + b.getFloat(i * 2 + v), c.getFloat(i * 2 + v));
                }
            }
        }

        //compatible units
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.addF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * da1.getFloatElement(i)[0] + 0.01 * da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //Kelvin to Celsius
        da1 = DataArray.create(new float[]{1.7f, 20.5f, 3.8f}, 1, "da1", "Kelvin", null);
        da2 = DataArray.create(new float[]{11.3f, -10.2f, 20.3f}, 1, "da2", "Celsius", null);
        da1.setPreferredRanges(243, 303, 243, 303);
        da2.setPreferredRanges(-30, 30, -30, 30);
        res = DataArrayArithmetics.addF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("K", res.getUnit());
        for (int i = 0; i < da1.getNElements(); i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[0] + 273.15, res.getFloatElement(i)[0]);
        }
        da2.setPreferredRanges(243.15, 303.15, -30, 30);
        res = DataArrayArithmetics.addF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("K", res.getUnit());
        for (int i = 0; i < da1.getNElements(); i++) {
            assertRelativeEquals(da1.getPhysicalFloatElement(i)[0] + da2.getPhysicalFloatElement(i)[0], res.getPhysicalFloatElement(i)[0]);
        }

        //incompatible units
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "m", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "kg", null);
        res = DataArrayArithmetics.addF(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.addF(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }

        //unitless OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "kg", null);
        res = DataArrayArithmetics.addF(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.addF(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }

        //unit OP unitless
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "kg", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "", null);
        res = DataArrayArithmetics.addF(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.addF(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testAddD()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.addD(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, n), 1, "da2");
        assertEquals(1, res.getVectorLength());
        res = DataArrayArithmetics.addD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(n, res.getNElements());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.addD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[v], res.getDoubleElement(i)[v]);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.addD(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[v], res.getDoubleElement(i)[v]);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.addD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1c.getFloatRealArray().getFloat(i * 3 + v) + da2.getFloatElement(i)[v], resc.getFloatRealArray().getFloat(i * 3 + v));
                assertRelativeEquals(da1c.getFloatImaginaryArray().getFloat(i * 3 + v), resc.getFloatImaginaryArray().getFloat(i * 3 + v));
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.addD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToDouble();
        td2 = td2.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(a.getDouble(i) + b.getDouble(i * 2 + v), c.getDouble(i * 2 + v));
                }
            }
        }

        //compatible units
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.addD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * da1.getDoubleElement(i)[0] + 0.01 * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //Kelvin to Celsius
        da1 = DataArray.create(new double[]{1.7, 20.5, 3.8}, 1, "da1", "Kelvin", null);
        da2 = DataArray.create(new double[]{11.3, -10.2, 20.3}, 1, "da2", "Celsius", null);
        da1.setPreferredRanges(243, 303, 243, 303);
        da2.setPreferredRanges(-30, 30, -30, 30);
        res = DataArrayArithmetics.addD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("K", res.getUnit());
        for (int i = 0; i < da1.getNElements(); i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[0] + 273.15, res.getDoubleElement(i)[0]);
        }
        da2.setPreferredRanges(243.15, 303.15, -30, 30);
        res = DataArrayArithmetics.addD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("K", res.getUnit());
        for (int i = 0; i < da1.getNElements(); i++) {
            assertRelativeEquals(da1.getPhysicalDoubleElement(i)[0] + da2.getPhysicalDoubleElement(i)[0], res.getPhysicalDoubleElement(i)[0]);
        }

        //incompatible units
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "m", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "kg", null);
        res = DataArrayArithmetics.addD(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.addD(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }

        //unitless OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "kg", null);
        res = DataArrayArithmetics.addD(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.addD(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }

        //unit OP unitless
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "kg", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "", null);
        res = DataArrayArithmetics.addD(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.addD(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testDiffF()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_SHORT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.diffF(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] - da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, n), 1, "da2");
        res = DataArrayArithmetics.diffF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] - da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.diffF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1.getFloatElement(i)[0] - da2.getFloatElement(i)[v], res.getFloatElement(i)[v]);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.diffF(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da2.getFloatElement(i)[v] - da1.getFloatElement(i)[0], res.getFloatElement(i)[v]);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.diffF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1c.getFloatRealArray().getFloat(i * 3 + v) - da2.getFloatElement(i)[v], resc.getFloatRealArray().getFloat(i * 3 + v));
                assertRelativeEquals(da1c.getFloatImaginaryArray().getFloat(i * 3 + v), resc.getFloatImaginaryArray().getFloat(i * 3 + v));
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.diffF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToFloat();
        td2 = td2.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(a.getFloat(i) - b.getFloat(i * 2 + v), c.getFloat(i * 2 + v));
                }
            }
        }

        //compatible units
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.diffF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * da1.getFloatElement(i)[0] - 0.01 * da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //Kelvin to Celsius
        da1 = DataArray.create(new float[]{1.7f, 20.5f, 3.8f}, 1, "da1", "Kelvin", null);
        da2 = DataArray.create(new float[]{11.3f, -10.2f, 20.3f}, 1, "da2", "Celsius", null);
        da1.setPreferredRanges(243, 303, 243, 303);
        da2.setPreferredRanges(-30, 30, -30, 30);
        res = DataArrayArithmetics.diffF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("K", res.getUnit());
        for (int i = 0; i < da1.getNElements(); i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] - da2.getFloatElement(i)[0] - 273.15, res.getFloatElement(i)[0]);
        }
        da2.setPreferredRanges(243.15, 303.15, -30, 30);
        res = DataArrayArithmetics.diffF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("K", res.getUnit());
        for (int i = 0; i < da1.getNElements(); i++) {
            assertRelativeEquals(da1.getPhysicalFloatElement(i)[0] - da2.getPhysicalFloatElement(i)[0], res.getPhysicalFloatElement(i)[0]);
        }

        //incompatible units
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "m", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "kg", null);
        res = DataArrayArithmetics.diffF(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] - da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.diffF(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }

        //unitless OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "kg", null);
        res = DataArrayArithmetics.diffF(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] - da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.diffF(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }

        //unit OP unitless
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "kg", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "", null);
        res = DataArrayArithmetics.diffF(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] - da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.diffF(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testDiffD()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.diffD(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] - da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, n), 1, "da2");
        res = DataArrayArithmetics.diffD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] - da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.diffD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1.getDoubleElement(i)[0] - da2.getDoubleElement(i)[v], res.getDoubleElement(i)[v]);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.diffD(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da2.getDoubleElement(i)[v] - da1.getDoubleElement(i)[0], res.getDoubleElement(i)[v]);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.diffD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1c.getFloatRealArray().getFloat(i * 3 + v) - da2.getFloatElement(i)[v], resc.getFloatRealArray().getFloat(i * 3 + v));
                assertRelativeEquals(da1c.getFloatImaginaryArray().getFloat(i * 3 + v), resc.getFloatImaginaryArray().getFloat(i * 3 + v));
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.diffD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToDouble();
        td2 = td2.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(a.getDouble(i) - b.getDouble(i * 2 + v), c.getDouble(i * 2 + v));
                }
            }
        }

        //compatible units
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.diffD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * da1.getDoubleElement(i)[0] - 0.01 * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //Kelvin to Celsius
        da1 = DataArray.create(new double[]{1.7, 20.5, 3.8}, 1, "da1", "Kelvin", null);
        da2 = DataArray.create(new double[]{11.3, -10.2, 20.3}, 1, "da2", "Celsius", null);
        da1.setPreferredRanges(243, 303, 243, 303);
        da2.setPreferredRanges(-30, 30, -30, 30);
        res = DataArrayArithmetics.diffD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("K", res.getUnit());
        for (int i = 0; i < da1.getNElements(); i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] - da2.getDoubleElement(i)[0] - 273.15, res.getDoubleElement(i)[0]);
        }
        da2.setPreferredRanges(243.15, 303.15, -30, 30);
        res = DataArrayArithmetics.diffD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("K", res.getUnit());
        for (int i = 0; i < da1.getNElements(); i++) {
            assertRelativeEquals(da1.getPhysicalDoubleElement(i)[0] - da2.getPhysicalDoubleElement(i)[0], res.getPhysicalDoubleElement(i)[0]);
        }

        //incompatible units
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "m", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "kg", null);
        res = DataArrayArithmetics.diffD(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] - da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.diffD(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }

        //unitless OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "kg", null);
        res = DataArrayArithmetics.diffD(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] - da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.diffD(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }

        //unit OP unitless
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "kg", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "", null);
        res = DataArrayArithmetics.diffD(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] - da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.diffD(da1, da2, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testMultF()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_FLOAT, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, n, 100, "da2");
        DataArray res = DataArrayArithmetics.multF(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] * da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2");
        res = DataArrayArithmetics.multF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] * da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da2");
        res = DataArrayArithmetics.multF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());
        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1.getFloatElement(i)[0] * da2.getFloatElement(i)[v], res.getFloatElement(i)[v]);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.multF(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());
        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da2.getFloatElement(i)[v] * da1.getFloatElement(i)[0], res.getFloatElement(i)[v]);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.multF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());
        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;
        ComplexFloatLargeArray ca1 = da1c.getRawArray();
        ComplexFloatLargeArray resca = resc.getRawArray();

        for (int i = 0; i < n; i++) {
            float[] dot = new float[2];
            for (int v = 0; v < 3; v++) {
                float[] elem_a = ca1.getComplexFloat(i * 3 + v);
                float[] elem_b = new float[]{da2.getFloatElement(i)[v], 0};
                dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(elem_a, elem_b));
            }
            assertRelativeArrayEquals(dot, resca.getComplexFloat(i));
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.multF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToFloat();
        td2 = td2.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(a.getFloat(i) * b.getFloat(i * 2 + v), c.getFloat(i * 2 + v));
                }
            }
        }

        //unit OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.multF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("m2", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * da1.getFloatElement(i)[0] * 0.01 * da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
        res = DataArrayArithmetics.multF(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] * da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "1/cm", null);
        res = DataArrayArithmetics.multF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * da1.getFloatElement(i)[0] * 100 * da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //unit OP unitless
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "", null);
        res = DataArrayArithmetics.multF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * da1.getFloatElement(i)[0] * da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
        res = DataArrayArithmetics.multF(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] * da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //unitless OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.multF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] * 0.01 * da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
        res = DataArrayArithmetics.multF(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] * da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
    }

    @Test
    public void testMultD()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_FLOAT, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, n, 100, "da2");
        DataArray res = DataArrayArithmetics.multD(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2");
        res = DataArrayArithmetics.multD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da2");
        res = DataArrayArithmetics.multD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da1.getDoubleElement(i)[0] * da2.getDoubleElement(i)[v], res.getDoubleElement(i)[v]);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.multD(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(da2.getDoubleElement(i)[v] * da1.getDoubleElement(i)[0], res.getDoubleElement(i)[v]);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.multD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        ComplexFloatLargeArray ca1 = da1c.getRawArray();
        ComplexFloatLargeArray resca = resc.getRawArray();

        for (int i = 0; i < n; i++) {
            float[] dot = new float[2];
            for (int v = 0; v < 3; v++) {
                float[] elem_a = ca1.getComplexFloat(i * 3 + v);
                float[] elem_b = new float[]{da2.getFloatElement(i)[v], 0};
                dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(elem_a, elem_b));
            }
            assertRelativeArrayEquals(dot, resca.getComplexFloat(i));
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.multD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToDouble();
        td2 = td2.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(a.getDouble(i) * b.getDouble(i * 2 + v), c.getDouble(i * 2 + v));
                }
            }
        }

        //unit OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.multD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("m2", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * da1.getDoubleElement(i)[0] * 0.01 * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
        res = DataArrayArithmetics.multD(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "1/cm", null);
        res = DataArrayArithmetics.multD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * da1.getDoubleElement(i)[0] * 100 * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //unit OP unitless
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "", null);
        res = DataArrayArithmetics.multD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * da1.getDoubleElement(i)[0] * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
        res = DataArrayArithmetics.multD(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //unitless OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.multD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] * 0.01 * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
        res = DataArrayArithmetics.multD(da1, da2, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
    }

    @Test
    public void testDivF()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_SHORT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.divF(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(da1.getFloatElement(i)[0] / da2.getFloatElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, n), 1, "da2");
        res = DataArrayArithmetics.divF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(da1.getFloatElement(i)[0] / da2.getFloatElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.divF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(da1.getFloatElement(i)[0] / da2.getFloatElement(i)[v], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.divF(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(da2.getFloatElement(i)[v] / da1.getFloatElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.divF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;
        ComplexFloatLargeArray ca1 = da1c.getRawArray();
        ComplexFloatLargeArray resca = resc.getRawArray();

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(LargeArrayArithmetics.complexDiv(ca1.getComplexFloat(i * 3 + v), new float[]{da2.getFloatElement(i)[v], 0})[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), resca.getComplexFloat(i * 3 + v)[0]);
                assertRelativeEquals(FloatingPointUtils.processNaNs(LargeArrayArithmetics.complexDiv(ca1.getComplexFloat(i * 3 + v), new float[]{da2.getFloatElement(i)[v], 0})[1], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), resca.getComplexFloat(i * 3 + v)[1]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.divF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToFloat();
        td2 = td2.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(a.getFloat(i) / b.getFloat(i * 2 + v), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //unit OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.divF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals((0.1f * da1.getFloatElement(i)[0]) / (0.01f * da2.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }

        //unit OP unitless
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "", null);
        res = DataArrayArithmetics.divF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals((0.1f * da1.getFloatElement(i)[0]) / da2.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //unitless OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.divF(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("m-1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getFloatElement(i)[0] / (0.01f * da2.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
    }

    @Test
    public void testDivD()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.divD(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(da1.getDoubleElement(i)[0] / da2.getDoubleElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, n), 1, "da2");
        res = DataArrayArithmetics.divD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(da1.getDoubleElement(i)[0] / da2.getDoubleElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.divD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(da1.getDoubleElement(i)[0] / da2.getDoubleElement(i)[v], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.divD(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(da2.getDoubleElement(i)[v] / da1.getDoubleElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.divD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        ComplexFloatLargeArray ca1 = da1c.getRawArray();
        ComplexFloatLargeArray resca = resc.getRawArray();

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(LargeArrayArithmetics.complexDiv(ca1.getComplexFloat(i * 3 + v), new float[]{da2.getFloatElement(i)[v], 0})[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), resca.getComplexFloat(i * 3 + v)[0]);
                assertRelativeEquals(FloatingPointUtils.processNaNs(LargeArrayArithmetics.complexDiv(ca1.getComplexFloat(i * 3 + v), new float[]{da2.getFloatElement(i)[v], 0})[1], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), resca.getComplexFloat(i * 3 + v)[1]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.divD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToDouble();
        td2 = td2.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(a.getDouble(i) / b.getDouble(i * 2 + v), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //unit OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.divD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals((0.1 * da1.getDoubleElement(i)[0]) / (0.01 * da2.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }

        //unit OP unitless
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "", null);
        res = DataArrayArithmetics.divD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals((0.1 * da1.getDoubleElement(i)[0]) / da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //unitless OP unit
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "", null);
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2", "cm", null);
        res = DataArrayArithmetics.divD(da1, da2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("m-1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(da1.getDoubleElement(i)[0] / (0.01 * da2.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
    }

    @Test
    public void testPowF()
    {
        int n = 10;
        double e = 2.5;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.powF(da, e);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(pow(da.getFloatElement(i)[0], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.powF(da, e);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(pow(da.getFloatElement(i)[0], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        res = DataArrayArithmetics.powF(da, da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(pow(da.getFloatElement(i)[0], da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.powF(da, e);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(pow(da.getFloatElement(i)[v], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        res = DataArrayArithmetics.powF(da, da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(pow(da.getFloatElement(i)[v], da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.powF(da, e);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(pow(a.getFloat(i * 2 + v), e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        res = DataArrayArithmetics.powF(da, da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(pow(a.getFloat(i * 2 + v), a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.powF(da, 2, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("m2", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.pow(0.1 * da.getFloatElement(i)[0], 2), res.getFloatElement(i)[0]);
        }
        res = DataArrayArithmetics.powF(da, 2.5, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.pow(da.getFloatElement(i)[0], 2.5), res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.powF(da, 2.5, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }

        res = DataArrayArithmetics.powF(da, da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.pow(da.getFloatElement(i)[0], da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }

        try {
            DataArrayArithmetics.powF(da, da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testPowD()
    {
        int n = 10;
        double e = 2.5;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.powD(da, e);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(pow(da.getDoubleElement(i)[0], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.powD(da, e);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(pow(da.getDoubleElement(i)[0], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        res = DataArrayArithmetics.powD(da, da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(pow(da.getDoubleElement(i)[0], da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.powD(da, e);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(pow(da.getDoubleElement(i)[v], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        res = DataArrayArithmetics.powD(da, da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(pow(da.getDoubleElement(i)[v], da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.powD(da, e);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(pow(a.getDouble(i * 2 + v), e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        res = DataArrayArithmetics.powD(da, da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(pow(a.getDouble(i * 2 + v), a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.powD(da, 2, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("m2", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.pow(0.1 * da.getDoubleElement(i)[0], 2), res.getDoubleElement(i)[0]);
        }
        res = DataArrayArithmetics.powD(da, 2.5, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.pow(da.getDoubleElement(i)[0], 2.5), res.getDoubleElement(i)[0]);
        }

        try {
            DataArrayArithmetics.powD(da, 2.5, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }

        res = DataArrayArithmetics.powD(da, da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.pow(da.getDoubleElement(i)[0], da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }

        try {
            DataArrayArithmetics.powD(da, da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }

    }

    @Test
    public void testNegF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.negF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(-da.getFloatElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.negF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(-da.getFloatElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.negF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(-da.getFloatElement(i)[v], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.negF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(-a.getFloat(i * 2 + v), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }
    }

    @Test
    public void testNegD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.negD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(-da.getDoubleElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.negD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(-da.getDoubleElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.negD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(-da.getDoubleElement(i)[v], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.negD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(-a.getDouble(i * 2 + v), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }
    }

    @Test
    public void testSqrtF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.sqrtF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.sqrtF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.sqrtF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.sqrtF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.sqrtF(da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.sqrt(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.sqrtF(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testSqrtD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.sqrtD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.sqrtD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.sqrtD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.sqrtD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.sqrtD(da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.sqrt(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.sqrtD(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testLogF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.logF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(log(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.logF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(log(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.logF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(log(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.logF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(log(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.logF(da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.log(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.logF(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testLogD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.logD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(log(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.logD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(log(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.logD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(log(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.logD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(log(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.logD(da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.log(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.logD(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testLog10F()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.log10F(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(log10(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.log10F(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(log10(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.log10F(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(log10(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.log10F(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(log10(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.log10F(da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.log10(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.log10F(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testLog10D()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.log10D(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(log10(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.log10D(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(log10(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.log10D(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(log10(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.log10D(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(log10(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.log10D(da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.log10(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.log10D(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testExpF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.expF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(exp(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.expF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(exp(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.expF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(exp(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.expF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(exp(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.expF(da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.exp(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.expF(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testExpD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.expD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(exp(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.expD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(exp(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.expD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(exp(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.expD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(exp(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.expD(da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.exp(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.expD(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testAbsF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.absF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(abs(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.absF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(abs(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.absF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(1, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            double norm = 0;
            for (int v = 0; v < 3; v++) {
                norm += da.getFloatElement(i)[v] * da.getFloatElement(i)[v];
            }
            assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(norm), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);

        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.absF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                double norm = 0;
                for (int v = 0; v < 2; v++) {
                    norm += a.getFloat(i * 2 + v) * a.getFloat(i * 2 + v);
                }
                assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(norm), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i));
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.absF(da);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.abs(0.1 * da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
    }

    @Test
    public void testAbsD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.absD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(abs(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.absD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(abs(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.absD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(1, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            double norm = 0;
            for (int v = 0; v < 3; v++) {
                norm += da.getDoubleElement(i)[v] * da.getDoubleElement(i)[v];
            }
            assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(norm), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);

        }
        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.absD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                double norm = 0;
                for (int v = 0; v < 2; v++) {
                    norm += a.getDouble(i * 2 + v) * a.getDouble(i * 2 + v);
                }
                assertRelativeEquals(FloatingPointUtils.processNaNs(sqrt(norm), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i));

            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.absD(da);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("m", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.abs(0.1 * da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
    }

    @Test
    public void testSinF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.sinF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(sin(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.sinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(sin(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.sinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(sin(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.sinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(sin(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.sinF(da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.sin(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.sinF(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testSinD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.sinD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(sin(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.sinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(sin(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.sinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(sin(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.sinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(sin(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.sinD(da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.sin(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.sinD(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testCosF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.cosF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(cos(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.cosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(cos(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.cosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(cos(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.cosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(cos(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.cosF(da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.cos(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.cosF(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testCosD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.cosD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(cos(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.cosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(cos(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.cosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(cos(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.cosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(cos(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.cosD(da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.cos(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.cosD(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testTanF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.tanF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(tan(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.tanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(tan(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.tanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(tan(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.tanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(tan(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.tanF(da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.tan(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.tanF(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testTanD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.tanD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(tan(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.tanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(tan(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.tanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(tan(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.tanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(tan(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.tanD(da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.tan(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.tanD(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testAsinF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.asinF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(asin(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.asinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(asin(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.asinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(asin(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.asinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(asin(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.asinF(da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.asin(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.asinF(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testAsinD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.asinD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(asin(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.asinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(asin(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.asinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(asin(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.asinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(asin(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.asinD(da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.asin(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.asinD(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testAcosF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.acosF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(acos(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.acosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(acos(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.acosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(acos(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.acosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(acos(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.acosF(da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.acos(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.acosF(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testAcosD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.acosD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(acos(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.acosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(acos(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.acosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(acos(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.acosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(acos(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.acosD(da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.acos(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.acosD(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testAtanF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.atanF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(atan(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.atanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(atan(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.atanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(atan(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.atanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(atan(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.atanF(da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.atan(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.atanF(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testAtanD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.atanD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(atan(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.atanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(atan(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.atanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(atan(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.atanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(atan(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.atanD(da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.atan(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.atanD(da, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testAxpyF()
    {
        int n = 10;

        //a and b real
        DataArray a = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 2, "a");
        DataArray b = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 3, "b");

        //veclen = 1 real
        DataArray x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "x");
        DataArray res = DataArrayArithmetics.axpyF(a, x, b);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(a.getFloatElement(i)[0] * x.getFloatElement(i)[0] + b.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //veclen > 1 real
        x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, 3 * n), 3, "x");
        res = DataArrayArithmetics.axpyF(a, x, b);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(a.getFloatElement(i)[0] * x.getFloatElement(i)[v] + b.getFloatElement(i)[0], res.getFloatElement(i)[v]);
            }
        }

        //a and b complex
        a = DataArray.createConstant(DataArrayType.FIELD_DATA_COMPLEX, n, new float[]{2, -3}, "a");
        b = DataArray.createConstant(DataArrayType.FIELD_DATA_COMPLEX, n, new float[]{-2, 5}, "b");

        //veclen = 1 complex
        x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, n), 1, "x");
        res = DataArrayArithmetics.axpyF(a, x, b);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());
        ComplexDataArray ac = (ComplexDataArray) a;
        ComplexDataArray xc = (ComplexDataArray) x;
        ComplexDataArray bc = (ComplexDataArray) b;
        ComplexDataArray resc = (ComplexDataArray) res;

        for (int i = 0; i < n; i++) {
            assertRelativeArrayEquals(LargeArrayArithmetics.complexAdd(LargeArrayArithmetics.complexMult(ac.getComplexFloatElement(i)[0], xc.getComplexFloatElement(i)[0]), bc.getComplexFloatElement(i)[0]), resc.getComplexFloatElement(i)[0]);
        }

        //veclen > 1 complex
        x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "x");
        xc = (ComplexDataArray) x;
        res = DataArrayArithmetics.axpyF(a, x, b);
        resc = (ComplexDataArray) res;
        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeArrayEquals(LargeArrayArithmetics.complexAdd(LargeArrayArithmetics.complexMult(ac.getComplexFloatElement(i)[0], xc.getComplexFloatElement(i)[v]), bc.getComplexFloatElement(i)[0]), resc.getComplexFloatElement(i)[v]);
            }
        }

        //time steps > 1
        a = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 2, "a");
        b = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 3, "b");
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        x = DataArray.create(td1, 1, "x");

        res = DataArrayArithmetics.axpyF(a, x, b);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        for (Float time : timeSeries) {
            LargeArray xla = td1.getValue(time);
            LargeArray resla = td.getValue(time);
            for (int i = 0; i < n; i++) {
                assertRelativeEquals(a.getFloatElement(i)[0] * xla.getFloat(i) + b.getFloatElement(i)[0], resla.getFloat(i));
            }
        }

        //compatible units
        a = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 2, "a", "dm", null);
        b = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 3, "b", "cm2", null);
        x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "x", "m", null);
        res = DataArrayArithmetics.axpyF(a, x, b, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("m2", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * a.getFloatElement(i)[0] * x.getFloatElement(i)[0] + 0.0001 * b.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }

        //incompatible units
        x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "x", "kg", null);
        res = DataArrayArithmetics.axpyF(a, x, b, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(a.getFloatElement(i)[0] * x.getFloatElement(i)[0] + b.getFloatElement(i)[0], res.getFloatElement(i)[0]);
        }
        try {
            DataArrayArithmetics.axpyF(a, x, b, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testAxpyD()
    {
        int n = 10;

        //a and b real
        DataArray a = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 2, "a");
        DataArray b = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 3, "b");

        //veclen = 1 real
        DataArray x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "x");
        DataArray res = DataArrayArithmetics.axpyD(a, x, b);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(a.getDoubleElement(i)[0] * x.getDoubleElement(i)[0] + b.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //veclen > 1 real
        x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, 3 * n), 3, "x");
        res = DataArrayArithmetics.axpyD(a, x, b);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(a.getDoubleElement(i)[0] * x.getDoubleElement(i)[v] + b.getDoubleElement(i)[0], res.getDoubleElement(i)[v]);
            }
        }

        //a and b complex
        a = DataArray.createConstant(DataArrayType.FIELD_DATA_COMPLEX, n, new float[]{2, -3}, "a");
        b = DataArray.createConstant(DataArrayType.FIELD_DATA_COMPLEX, n, new float[]{-2, 5}, "b");

        //veclen = 1 complex
        x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, n), 1, "x");
        res = DataArrayArithmetics.axpyD(a, x, b);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(n, res.getNElements());
        ComplexDataArray ac = (ComplexDataArray) a;
        ComplexDataArray xc = (ComplexDataArray) x;
        ComplexDataArray bc = (ComplexDataArray) b;
        ComplexDataArray resc = (ComplexDataArray) res;

        for (int i = 0; i < n; i++) {
            assertRelativeArrayEquals(LargeArrayArithmetics.complexAdd(LargeArrayArithmetics.complexMult(ac.getComplexFloatElement(i)[0], xc.getComplexFloatElement(i)[0]), bc.getComplexFloatElement(i)[0]), resc.getComplexFloatElement(i)[0]);
        }

        //veclen > 1 complex
        x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "x");
        xc = (ComplexDataArray) x;
        res = DataArrayArithmetics.axpyD(a, x, b);
        resc = (ComplexDataArray) res;
        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());
        assertEquals(n, res.getNElements());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeArrayEquals(LargeArrayArithmetics.complexAdd(LargeArrayArithmetics.complexMult(ac.getComplexFloatElement(i)[0], xc.getComplexFloatElement(i)[v]), bc.getComplexFloatElement(i)[0]), resc.getComplexFloatElement(i)[v]);
            }
        }

        //time steps > 1
        a = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 2, "a");
        b = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 3, "b");
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        x = DataArray.create(td1, 1, "x");

        res = DataArrayArithmetics.axpyD(a, x, b);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        for (Float time : timeSeries) {
            LargeArray xla = td1.getValue(time);
            LargeArray resla = td.getValue(time);
            for (int i = 0; i < n; i++) {
                assertRelativeEquals(a.getDoubleElement(i)[0] * xla.getDouble(i) + b.getDoubleElement(i)[0], resla.getDouble(i));
            }
        }

        //compatible units
        a = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 2, "a", "dm", null);
        b = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 3, "b", "cm2", null);
        x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "x", "m", null);
        res = DataArrayArithmetics.axpyD(a, x, b, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("m2", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(0.1 * a.getDoubleElement(i)[0] * x.getDoubleElement(i)[0] + 0.0001 * b.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }

        //incompatible units
        x = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "x", "kg", null);
        res = DataArrayArithmetics.axpyD(a, x, b, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(a.getDoubleElement(i)[0] * x.getDoubleElement(i)[0] + b.getDoubleElement(i)[0], res.getDoubleElement(i)[0]);
        }
        try {
            DataArrayArithmetics.axpyD(a, x, b, false);
            assert (false);
        } catch (IllegalArgumentException ex) {
            assert (true);
        }
    }

    @Test
    public void testSignumF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.signumF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(signum(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.signumF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(signum(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.signumF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(signum(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.signumF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(signum(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.signumF(da, true);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.signum(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
        res = DataArrayArithmetics.signumF(da, false);
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.signum(da.getFloatElement(i)[0]), res.getFloatElement(i)[0]);
        }
    }

    @Test
    public void testSignumD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.signumD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(signum(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.signumD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FloatingPointUtils.processNaNs(signum(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0]);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.signumD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertRelativeEquals(FloatingPointUtils.processNaNs(signum(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v]);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.signumD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertRelativeEquals(FloatingPointUtils.processNaNs(signum(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v));
                }
            }
        }

        //units 
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da1", "dm", null);
        res = DataArrayArithmetics.signumD(da, true);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("dm", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.signum(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }
        res = DataArrayArithmetics.signumD(da, false);
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals("1", res.getUnit());
        for (int i = 0; i < n; i++) {
            assertRelativeEquals(FastMath.signum(da.getDoubleElement(i)[0]), res.getDoubleElement(i)[0]);
        }

    }
}
