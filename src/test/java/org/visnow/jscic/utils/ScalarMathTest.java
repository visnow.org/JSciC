/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ScalarMathTest
{

    public ScalarMathTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testNextPower2Int()
    {
        assertEquals(1, ScalarMath.nextPower2(1));
        assertEquals(2, ScalarMath.nextPower2(2));
        assertEquals(4, ScalarMath.nextPower2(3));
        assertEquals(4, ScalarMath.nextPower2(4));
        assertEquals(8, ScalarMath.nextPower2(5));
        assertEquals(8, ScalarMath.nextPower2(6));
        assertEquals(8, ScalarMath.nextPower2(7));
        assertEquals(8, ScalarMath.nextPower2(8));
        assertEquals(16, ScalarMath.nextPower2(9));
        assertEquals(16, ScalarMath.nextPower2(12));
        assertEquals(16, ScalarMath.nextPower2(16));
        assertEquals(32, ScalarMath.nextPower2(24));
        assertEquals(32, ScalarMath.nextPower2(32));
        assertEquals(128, ScalarMath.nextPower2(77));
        assertEquals(256, ScalarMath.nextPower2(171));
        assertEquals(256, ScalarMath.nextPower2(184));
        assertEquals(512, ScalarMath.nextPower2(475));
        assertEquals(512, ScalarMath.nextPower2(512));
        assertEquals(1<<30, ScalarMath.nextPower2(1<<30));
    }

    @Test
    public void testNextPower2Long()
    {    
        assertEquals(1L, ScalarMath.nextPower2(1L));
        assertEquals(2L, ScalarMath.nextPower2(2L));
        assertEquals(4L, ScalarMath.nextPower2(3L));
        assertEquals(4L, ScalarMath.nextPower2(4L));
        assertEquals(8L, ScalarMath.nextPower2(5L));
        assertEquals(8L, ScalarMath.nextPower2(6L));
        assertEquals(8L, ScalarMath.nextPower2(7L));
        assertEquals(8L, ScalarMath.nextPower2(8L));
        assertEquals(16L, ScalarMath.nextPower2(9L));
        assertEquals(16L, ScalarMath.nextPower2(12L));
        assertEquals(16L, ScalarMath.nextPower2(16L));
        assertEquals(32L, ScalarMath.nextPower2(24L));
        assertEquals(32L, ScalarMath.nextPower2(32L));
        assertEquals(128L, ScalarMath.nextPower2(77L));
        assertEquals(256L, ScalarMath.nextPower2(171L));
        assertEquals(256L, ScalarMath.nextPower2(184L));
        assertEquals(512L, ScalarMath.nextPower2(475L));
        assertEquals(512L, ScalarMath.nextPower2(512L));
        assertEquals(1<<62, ScalarMath.nextPower2(1<<62));
    }
}
