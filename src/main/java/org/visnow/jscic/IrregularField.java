/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.Tetra;
import org.visnow.jscic.cells.SimplexPosition;
import org.visnow.jscic.cells.Triangle;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LongLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.utils.ArrayUtils;
import org.visnow.jscic.utils.EngineeringFormattingUtils;
import org.visnow.jscic.utils.FloatingPointUtils;

/**
 * A universal data structure describing a polyhedron as collection of elementary cells.
 *
 * @author Krzysztof S. Nowinski
 *
 * University of Warsaw, ICM
 */
public class IrregularField extends Field
{

    private static final long serialVersionUID = 1728066965898700139L;
    private final ArrayList<CellSet> cellSets = new ArrayList<>();
    private int[][][] cellGuide;
    private int[][][] cellGuideTop;
    private int[] cellGuideSizes = new int[4];

    /**
     * Create a new instance of IrregularField.
     *
     * @param schema field schema
     */
    public IrregularField(IrregularFieldSchema schema)
    {
        if (schema == null) {
            throw new IllegalArgumentException("schema cannot be null");
        }
        this.schema = schema;
        type = FieldType.FIELD_IRREGULAR;
        timeCoords = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        coordsTimestamp = System.nanoTime();
        timestamp = System.nanoTime();
    }

    /**
     * Create a new instance of IrregularField.
     *
     * @param nNodes number of nodes
     */
    public IrregularField(long nNodes)
    {
        if (nNodes <= 0) {
            throw new IllegalArgumentException("The number of nodes must be positive.");
        }
        schema = new IrregularFieldSchema();
        schema.setNElements(nNodes);
        type = FieldType.FIELD_IRREGULAR;
        timeCoords = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        timestamp = coordsTimestamp = System.nanoTime();
    }

    @Override
    public IrregularFieldSchema getSchema()
    {
        return (IrregularFieldSchema) schema;
    }

    @Override
    public int hashCode(float quality)
    {
        int fprint = super.hashCode(quality);
        fprint = 61 * fprint + Arrays.deepHashCode(this.cellGuide);
        fprint = 61 * fprint + Arrays.deepHashCode(this.cellGuideTop);
        fprint = 61 * fprint + Arrays.hashCode(this.cellGuideSizes);
        if (this.cellSets != null) {
            for (CellSet cs : this.cellSets) {
                fprint = 61 * fprint + cs.hashCode(quality);
            }
        }
        return fprint;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || !(o instanceof IrregularField))
            return false;
        IrregularField f = (IrregularField) o;
        boolean equal = super.equals(o) && this.cellSets.equals(f.cellSets) && Arrays.equals(this.cellGuideSizes, f.cellGuideSizes);
        if (equal == false) {
            return false;
        }
        if (this.cellGuide != null && f.cellGuide != null) {
            if (!Arrays.deepEquals(this.cellGuide, f.cellGuide)) return false;
        } else if (this.cellGuide != f.cellGuide) {
            return false;
        }
        if (this.cellGuideTop != null && f.cellGuideTop != null) {
            if (!Arrays.deepEquals(this.cellGuideTop, f.cellGuideTop)) return false;
        } else if (this.cellGuideTop != f.cellGuideTop) {
            return false;
        }
        return equal;
    }

    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        if (isLarge()) {
            s.append("Large ");
        }
        s.append(String.format("Irregular field, %d nodes, ", getNElements()));
        if (getNFrames() > 1) {
            s.append(getNFrames()).append(" time frames, ");
        }
        s.append(components.size()).append(" data components");
        return s.toString();
    }

    @Override
    public String toMultilineString()
    {
        StringBuilder s = new StringBuilder();
        if (isLarge()) {
            s.append(String.format("Large irregular field, %d nodes", getNElements()));
        } else {
            s.append(String.format("Irregular field, %d nodes", getNElements()));
        }
        if (getNFrames() > 1) {
            s.append(getNFrames()).append("<p> time frames, ");
        }
        s.append(components.size()).append(" data components");
        return s.toString();
    }

    @Override
    public String shortDescription()
    {
        StringBuilder s = new StringBuilder();
        s.append("<html>");
        s.append(getNElements()).append(" nodes");
        int n = 0;
        for (CellSet cs : cellSets) {
            n += cs.getNCells();
        }
        s.append("<br>").append(n).append(" cells");
        if (getNFrames() > 1) {
            s.append("<br>").append(getNFrames()).append(" timesteps");
        }
        s.append("<br>").append(components.size()).append(" components");
        s.append("</html>");
        return s.toString();
    }

    @Override
    public String description()
    {
        return description(false);
    }

    @Override
    public String conciseDescription()
    {
        StringBuffer s = new StringBuffer();
        s.append("<p>Name: ").append(schema.getName()).append("</p>");
        if (isLarge())
            s.append("Large ");        
        s.append("Irregular field, ");
        s.append(NumberFormat.getNumberInstance(Locale.US).format(getNElements())).append(" nodes");
        if (trueNSpace > 0) {
            s.append(", true ").append(trueNSpace).append("-dim ");
        }
        if (getNFrames() > 1) 
            s.append(", ").append(getNFrames()).append(" timesteps");
        s.append("<p>");
        
        s.append("<p>Geometric extents:</p>");
        s.append(asXYZTable(getPreferredExtents(), null, 3, 2, true, true));
        
        if (components.size() > 0) {
            s.append("<p><BR>Node data components:");
            s.append("<TABLE border=\"0\" cellspacing=\"5\">");
                s.append("<TR valign='top' align='right'><TD align = 'left'>Name</td><TD>Vector<br/>length</td><td>Type</td></tr>");
            
            for (int i = 0; i < components.size(); i++) {
                s.append(getComponent(i).conciseDescription());
            }
            s.append("</TABLE>");
        }
        
        if (cellSets != null && cellSets.size() > 1) {
            s.append("<p>" + cellSets.size() + "cell sets:");
        }
        return "<html>" + s + "</html>";
    }

    @Override
    public String description(boolean debug)
    {
        StringBuffer s = new StringBuffer();
        s.append("<p>Name: ").append(schema.getName()).append("</p>");
        if (isLarge())
            s.append("Large ");        
        s.append("Irregular field, ");
        s.append(NumberFormat.getNumberInstance(Locale.US).format(getNElements())).append(" nodes");
        if (trueNSpace > 0) {
            s.append(", true ").append(trueNSpace).append("-dim ");
        }
        if (getNFrames() > 1) {
            s.append(", ").append(getNFrames()).append(" timesteps<p>");
            s.append("Time range ");
            s.append(EngineeringFormattingUtils.formatHtml(getStartTime()));
            if (!isTimeUnitless()) {
                s.append(" ").append(getTimeUnit());
            }
            s.append(":");
            s.append(EngineeringFormattingUtils.formatHtml(getEndTime()));
            if (!isTimeUnitless()) {
                s.append(" ").append(getTimeUnit());
            }
            s.append("</p><p>Current time: ");
            s.append(EngineeringFormattingUtils.formatHtml(getCurrentTime()));
            if (!isTimeUnitless()) {
                s.append(" ").append(getTimeUnit());
            }
            s.append("</p>");
        } 
        
        if (schema.getUserData() != null) {
            s.append("<p>user data:");
            for (String u : schema.getUserData()) {
                s.append("<p>").append(u);
            }
        }   
        
        if (timeCoords.getNSteps() > 1)
            s.append("<p>" + timeCoords.getNSteps() + " coordinate time steps</p>");        
       
        if (timeMask != null && !timeMask.isEmpty()) {
            s.append("<p>with mask</p>");
            if (timeMask.getNSteps() > 1)
                s.append("<p>" + timeMask.getNSteps()   + " mask time steps</p>");
        }
        
        

        s.append("<p>Geometric extents:</p>");
        s.append(asXYZTable(getPreferredExtents(), null, 3, 2, true, true));

        s.append("<p>Physical extents:</p>");
        String[] axisLabels = getAxesNames().clone();
        if(!axisLabels[0].equals("x"))
            axisLabels[0] += " (x)";
        if(!axisLabels[1].equals("y"))
            axisLabels[1] += " (y)";
        if(!axisLabels[2].equals("z"))
            axisLabels[2] += " (z)";        
        s.append(asTable(getPreferredPhysicalExtents(), getCoordsUnits(), axisLabels, 3, 2, true, true));
        
        if (debug) {
            s.append("<p>True geometric extents:</p>");
            s.append(asXYZTable(getExtents(), null, 3, 2, true, true));

            s.append("<p>True physical extents:</p>");
            s.append(asTable(getPhysicalExtents(), getCoordsUnits(), axisLabels, 3, 2, true, true));
        }                
        
        if (components.size() > 0) {
            s.append("<p><BR>Node data components:");
            s.append("<TABLE border=\"0\" cellspacing=\"5\">");
        if (debug) {
            s.append("<TR valign='top' align='right'>").
              append("<TD align = 'left'>Name</td><TD>Vector<br/>length</td><td>Type</td><td>Time<br/>steps</td>").
              append("<td>Min</td><td>Max</td><td>Physical<br/>min</td><td>Physical<br/>max</td>").
              append("<td>Preferred<br/>min</td><td>Preferred<br/>max</td>").
              append("<td>Preferred<br/>physical<br/>min</td><td>Preferred<br/>physical<br/>max</td>").
              append("<td>Unit</td><td align=\"left\" valign=\"top\">User data</td></tr>");
        } else {
            s.append("<TR valign='top' align='right'>").
              append("<TD align = 'left'>Name</td><TD>Vector<br/>length</td><td>Type</td><td>Time<br/>steps</td>").
              append("<td>Min</td><td>Max</td><td>Physical<br/>min</td><td>Physical<br/>max</td><td>Unit</td></tr>");
        }
            for (int i = 0; i < components.size(); i++) {
                s.append(getComponent(i).description(debug));
            }
            s.append("</TABLE>");
        }
        
        if (cellSets != null) {
            s.append("<p><br>Cell sets:");
            s.append("<table border=\"0\" cellspacing=\"5\">");
            for (CellSet set : cellSets) {
                s.append("<tr><td>");
                s.append(set.description(debug));
                s.append("</td></tr>");
            }
            s.append("</table>");
        }
        return "<html>" + s + "</html>";
    }

    @Override
    public IrregularField cloneShallow()
    {
        IrregularField clone = new IrregularField((IrregularFieldSchema) schema.cloneDeep());
        clone.type = type;
        clone.geoTree = this.geoTree;
        clone.geoTree = this.geoTree != null ? this.geoTree.cloneShallow() : null;
        clone.cellExtents = ArrayUtils.cloneDeep(this.cellExtents);
        clone.axesNames = this.axesNames != null ? this.axesNames.clone() : null;
        clone.statisticsComputed = this.statisticsComputed;
        clone.trueNSpace = this.trueNSpace;
        clone.cellGuide = ArrayUtils.cloneDeep(this.cellGuide);
        clone.cellGuideTop = ArrayUtils.cloneDeep(this.cellGuideTop);
        clone.cellGuideSizes = this.cellGuideSizes != null ? this.cellGuideSizes.clone() : null;
        clone.normals = this.normals;

        if (this.timeCoords != null && !timeCoords.isEmpty()) {
            clone.setCoords(this.timeCoords.cloneShallow());
        }
        if (this.timeMask != null && !timeMask.isEmpty()) {
            clone.setMask(this.timeMask.cloneShallow());
        }

        if (components != null && !components.isEmpty()) {
            ArrayList<DataArray> componentsClone = new ArrayList<>();
            for (DataArray dataArray : components) {
                componentsClone.add(dataArray.cloneShallow());
            }
            clone.components = componentsClone;
        }
        if (cellSets != null && !cellSets.isEmpty()) {
            for (CellSet cellSet : cellSets) {
                clone.addCellSet(cellSet.cloneShallow());
            }
        }
        clone.setCurrentTime(this.currentTime);
        clone.coordsTimestamp = this.coordsTimestamp;
        clone.maskTimestamp = this.maskTimestamp;
        clone.timestamp = this.timestamp;
        return clone;
    }

    @Override
    public IrregularField cloneDeep()
    {
        IrregularField clone = new IrregularField((IrregularFieldSchema) schema.cloneDeep());
        clone.type = type;
        clone.geoTree = this.geoTree != null ? this.geoTree.cloneDeep() : null;
        clone.cellExtents = ArrayUtils.cloneDeep(this.cellExtents);
        clone.axesNames = this.axesNames != null ? this.axesNames.clone() : null;
        clone.statisticsComputed = this.statisticsComputed;
        clone.trueNSpace = this.trueNSpace;
        clone.cellGuide = ArrayUtils.cloneDeep(this.cellGuide);
        clone.cellGuideTop = ArrayUtils.cloneDeep(this.cellGuideTop);
        clone.cellGuideSizes = this.cellGuideSizes != null ? this.cellGuideSizes.clone() : null;

        clone.normals = this.normals != null ? this.normals.clone() : null;

        if (this.timeCoords != null && !timeCoords.isEmpty()) {
            clone.setCoords(this.timeCoords.cloneDeep());
        }
        if (this.timeMask != null && !timeMask.isEmpty()) {
            clone.setMask(this.timeMask.cloneDeep());
        }

        if (components != null && !components.isEmpty()) {
            ArrayList<DataArray> componentsClone = new ArrayList<>();
            for (DataArray dataArray : components) {
                componentsClone.add(dataArray.cloneDeep());
            }
            clone.components = componentsClone;
        }
        if (cellSets != null && !cellSets.isEmpty()) {
            for (CellSet cellSet : cellSets) {
                clone.addCellSet(cellSet.cloneDeep());
            }
        }
        clone.setCurrentTime(this.currentTime);
        clone.coordsTimestamp = this.coordsTimestamp;
        clone.maskTimestamp = this.maskTimestamp;
        clone.timestamp = this.timestamp;
        return clone;
    }

    @Override
    public DataArray interpolateDataToMesh(Field mesh, DataArray da)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Returns the number of cell sets.
     *
     * @return number of cell sets.
     */
    public int getNCellSets()
    {
        return cellSets.size();
    }

    /**
     * Returns a list of cell sets. This method always returns a reference to
     * the internal object.
     *
     * @return list of cell sets
     */
    public ArrayList<CellSet> getCellSets()
    {
        return cellSets;
    }

    /**
     * Returns a cell set corresponding to a given index.
     *
     * @param n index of a cell set
     *
     * @return cell set
     */
    public CellSet getCellSet(int n)
    {
        if (n < 0 || n >= cellSets.size()) {
            throw new IllegalArgumentException("n < 0 || n >= cellSets.size()");
        }
        return cellSets.get(n);
    }

    /**
     * Returns cell data schema. This method always returns a reference to the
     * internal object.
     *
     * @return cell data schema
     */
    public DataContainerSchema getCellDataSchema()
    {
        return ((IrregularFieldSchema) schema).getCellDataSchema();
    }

    /**
     * Collects all cell data arrays that occur in each cell set.
     */
    public void updateCellDataSchema()
    {
        DataContainerSchema cellDataSchema = new DataContainerSchema("ContainerSchema");
        if (cellSets == null || cellSets.isEmpty()) {
            return;
        }
        CellSet c0 = cellSets.get(0);
        if (c0.getNComponents() < 1) {
            return;
        }

        for (DataArray component : c0.getComponents()) {
            boolean retainComponent = true;
            for (int i = 1; i < this.getNCellSets(); i++) {
                CellSet cs = cellSets.get(i);
                boolean isCompatibleComponent = false;
                for (DataArraySchema csch : cs.getSchema().componentSchemas) {
                    if (component.getSchema().isCompatibleWith(csch, true)) {
                        isCompatibleComponent = true;
                        break;
                    }
                }
                if (!isCompatibleComponent) {
                    retainComponent = false;
                    break;
                }
            }
            if (retainComponent) {
                cellDataSchema.addDataArraySchema(component.getSchema());
            }
        }
        ((IrregularFieldSchema) schema).setCellDataSchema(cellDataSchema);
        timestamp = System.nanoTime();
    }

    /**
     * Addds a new cell set.
     *
     * @param newSet new cell set
     */
    public void addCellSet(CellSet newSet)
    {
//        if (cellSets.size() < 200) {
            newSet.setCurrentTime(this.currentTime);
            cellSets.add(newSet);
            updateCellDataSchema();
            timestamp = System.nanoTime();
//        } else {
//            //TODO: remove this restriction
//            throw new IllegalArgumentException("Irregular field can not have more than 200 cell sets");
//        }
    }

    private void replaceCellSet(int n, CellSet newSet)
    {
        if (n < 0) {
            throw new IllegalArgumentException("n < 0");
        }
        newSet.setCurrentTime(this.currentTime);
        if (n >= cellSets.size()) {
            cellSets.add(newSet);
        } else {
            cellSets.set(n, newSet);
        }
        updateCellDataSchema();
        timestamp = System.nanoTime();
    }

    @Override
    public boolean isStructureCompatibleWith(Field f)
    {
        if (!(f instanceof IrregularField) || f.getNNodes() != getNElements()) {
            return false;
        }
        IrregularField irf = (IrregularField) f;
        if (irf.getNCellSets() != cellSets.size()) {
            return false;
        }
        for (int i = 0; i < cellSets.size(); i++) {
            if (!cellSets.get(i).isStructCompatibleWith(irf.getCellSet(i))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isDataCompatibleWith(Field f)
    {
        if (f == null || !(f instanceof IrregularField) || !super.isDataCompatibleWith(f)) {
            return false;
        }
        IrregularField irf = (IrregularField) f;
        if (irf.getNCellSets() != cellSets.size()) {
            return false;
        }
        for (int i = 0; i < cellSets.size(); i++) {
            if (!cellSets.get(i).isCompatibleWith(irf.getCellSet(i))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void checkTrueNSpace()
    {
        trueNSpace = -1;
        int maxCellDim = 0;
        for (CellSet cs : cellSets) {
            if (cs.getCellArray(CellType.TETRA) != null && cs.getCellArray(CellType.TETRA).getNCells() > 0 ||
                cs.getCellArray(CellType.PYRAMID) != null && cs.getCellArray(CellType.PYRAMID).getNCells() > 0 ||
                cs.getCellArray(CellType.PRISM) != null && cs.getCellArray(CellType.PRISM).getNCells() > 0 ||
                cs.getCellArray(CellType.HEXAHEDRON) != null && cs.getCellArray(CellType.HEXAHEDRON).getNCells() > 0) {
                trueNSpace = 3;
                timestamp = System.nanoTime();
                return;
            }
            if (cs.getCellArray(CellType.TRIANGLE) != null && cs.getCellArray(CellType.TRIANGLE).getNCells() > 0 ||
                cs.getCellArray(CellType.QUAD) != null && cs.getCellArray(CellType.QUAD).getNCells() > 0) {
                maxCellDim = max(maxCellDim, 2);
            }
            if (cs.getCellArray(CellType.SEGMENT) != null && cs.getCellArray(CellType.SEGMENT).getNCells() > 0) {
                maxCellDim = max(maxCellDim, 1);
            }
        }
        switch (maxCellDim) {
            case 0:
                return;
            case 1:
                trueNSpace = 1;
                for (int i = 0; i < getNElements(); i++) {
                    if (coords.getFloat(3 * i + 1) != 0 || coords.getFloat(3 * i + 2) != 0) {
                        trueNSpace = -1;
                        timestamp = System.nanoTime();
                        return;
                    }
                }
                break;
            case 2:
                trueNSpace = 2;
                for (int i = 0; i < getNElements(); i++) {
                    if (coords.getFloat(3 * i + 2) != 0) {
                        trueNSpace = -1;
                        timestamp = System.nanoTime();
                        return;
                    }
                }

                break;
        }
        timestamp = System.nanoTime();
    }

    @Override
    public void createGeoTree()
    {
        int[] cellArrayBegin = new int[]{
            0, 1, 2, 4
        };
        cellGuide = new int[4][][];
        cellGuideTop = new int[4][][];
        int n = cellSets.size();
        cellGuide[0] = new int[n][1];
        cellGuide[1] = new int[n][1];
        cellGuide[2] = new int[n][2];
        cellGuide[3] = new int[n][4];
        cellGuideTop[0] = new int[n][1];
        cellGuideTop[1] = new int[n][1];
        cellGuideTop[2] = new int[n][2];
        cellGuideTop[3] = new int[n][4];
        FloatLargeArray c = (FloatLargeArray) timeCoords.getValue(currentTime);
        for (int i = 0; i < cellGuide.length; i++) {
            int m = 0;
            for (int j = 0; j < n; j++) {
                CellSet cs = cellSets.get(j);
                if (cs != null) {
                    for (int k = 0; k < cellGuide[i][j].length; k++) {
                        cellGuide[i][j][k] = m;
                        CellArray ca = cs.getCellArray(CellType.getType(cellArrayBegin[i] + k));
                        if (ca != null) {
                            m += ca.getNCells();
                        }
                        cellGuideTop[i][j][k] = m;
                    }
                }
            }
            cellGuideSizes[i] = m;
        }
        int[] cells = new int[cellGuideSizes[trueNSpace]];
        for (int i = 0; i < cells.length; i++) {
            cells[i] = i;
        }
        cellExtents = new float[2 * trueNSpace][cellGuideSizes[trueNSpace]];
        float[] cellLow = new float[trueNSpace];
        float[] cellUp = new float[trueNSpace];
        for (int ics = 0, m = 0; ics < cellSets.size(); ics++) {
            CellSet cs = cellSets.get(ics);
            if (cs != null) {
                switch (trueNSpace) {
                    case 3:
                        for (int k = 0; k < 4; k++) {
                            CellArray ca = cs.getCellArray(CellType.getType(4 + k));
                            if (ca == null) {
                                continue;
                            }
                            int cellNodes = CellType.getType(4 + k).getNVertices();
                            int[] nodes = ca.getNodes();
                            for (int i = 0; i < ca.getNCells(); i++, m++) {
                                for (int j = 0; j < trueNSpace; j++) {
                                    cellLow[j] = FloatingPointUtils.MAX_NUMBER_FLOAT;
                                    cellUp[j] = FloatingPointUtils.MIN_NUMBER_FLOAT;
                                }
                                for (int node = 0; node < cellNodes; node++) {
                                    int l = 3 * nodes[i * cellNodes + node];
                                    for (int j = 0; j < trueNSpace; j++) {
                                        float x = c.getFloat(l + j);
                                        if (x < cellLow[j]) {
                                            cellLow[j] = x;
                                        }
                                        if (x > cellUp[j]) {
                                            cellUp[j] = x;
                                        }
                                    }
                                }
                                for (int j = 0; j < trueNSpace; j++) {
                                    cellExtents[j][m] = cellLow[j];
                                    cellExtents[j + trueNSpace][m] = cellUp[j];
                                }
                            }
                        }
                        break;
                    case 2:
                        for (int k = 0; k < 2; k++) {
                            CellArray ca = cs.getCellArray(CellType.getType(2 + k));
                            if (ca == null) {
                                continue;
                            }
                            int cellNodes = CellType.getType(2 + k).getNVertices();
                            int[] nodes = ca.getNodes();
                            for (int i = 0; i < ca.getNCells(); i++, m++) {
                                for (int j = 0; j < trueNSpace; j++) {
                                    cellLow[j] = FloatingPointUtils.MAX_NUMBER_FLOAT;
                                    cellUp[j] = FloatingPointUtils.MIN_NUMBER_FLOAT;
                                }
                                for (int node = 0; node < cellNodes; node++) {
                                    int l = 3 * nodes[i * cellNodes + node];
                                    for (int j = 0; j < trueNSpace; j++) {
                                        float x = c.getFloat(l + j);
                                        if (x < cellLow[j]) {
                                            cellLow[j] = x;
                                        }
                                        if (x > cellUp[j]) {
                                            cellUp[j] = x;
                                        }
                                    }
                                }
                                for (int j = 0; j < trueNSpace; j++) {
                                    cellExtents[j][m] = cellLow[j];
                                    cellExtents[j + trueNSpace][m] = cellUp[j];
                                }
                            }
                        }
                        break;
                }

            }
        }

        int maxThr = Runtime.getRuntime().availableProcessors() - 1;
        if (maxThr < 1) {
            maxThr = 1;
        }
        int maxParallelLevel = 1 + (int) (log(maxThr) / log(2.));
        geoTree = new GeoTreeNode(trueNSpace, cells, cellExtents, 0, maxParallelLevel);
        pCreateGeoTree(geoTree, maxThr);
        timestamp = System.nanoTime();
    }

    private void pCreateGeoTree(GeoTreeNode root, int maxThr)
    {
        int inProcess = 0;
        Thread[] threadPool = new Thread[maxThr];
        Queue<GeoTreeNode> processQueue = new LinkedBlockingQueue<>();
        GeoTreeNode[] nodesInProcess = new GeoTreeNode[maxThr];
        processQueue.add(root);
        while (!processQueue.isEmpty() || inProcess > 0) {
            inProcess = 0;
            for (int i = 0; i < nodesInProcess.length; i++) {
                if (threadPool[i] == null) {
                    continue;
                } else if (threadPool[i].isAlive()) {
                    inProcess += 1;
                } else {
                    GeoTreeNode lastProcessedNode = nodesInProcess[i];
                    if (!lastProcessedNode.isFullySplit() && lastProcessedNode.getCells() == null) {
                        processQueue.add(lastProcessedNode.getNodeAbove());
                        processQueue.add(lastProcessedNode.getNodeBelow());
                    }
                    threadPool[i] = null;
                    nodesInProcess[i] = null;
                }
            }
            if (inProcess < maxThr) {
                for (int i = 0; i < nodesInProcess.length && !processQueue.isEmpty(); i++) {
                    if (threadPool[i] == null) {
                        GeoTreeNode nodeToProcess = processQueue.poll();
                        nodesInProcess[i] = nodeToProcess;
                        inProcess += 1;
                        threadPool[i] = new Thread(nodeToProcess);
                        threadPool[i].start();
                    }
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
            }
        }
    }

    private Cell getCell(int n)
    {
        int off = 4;
        if (trueNSpace == 2) {
            off = 2;
        }
        for (int j = cellSets.size() - 1; j >= 0; j--) {
            for (int k = cellGuide[trueNSpace][j].length - 1; k >= 0; k--) {
                if (n >= cellGuide[trueNSpace][j][k] && n < cellGuideTop[trueNSpace][j][k]) {
                    CellArray c = cellSets.get(j).getCellArray(CellType.getType(off + k));
                    int m = c.getType().getNVertices();
                    int[] verts = new int[m];
                    System.arraycopy(c.getNodes(), m * (n - cellGuide[trueNSpace][j][k]), verts, 0, m);
                    return Cell.createCell(c.getType(), verts, (byte) 1);
                }
            }
        }
        return null;
    }

    @Override
    public SimplexPosition getFieldCoords(float[] p)
    {
        if (coords == null) {
            coords = (FloatLargeArray) timeCoords.getValue(currentTime);
        }
        if (geoTree == null)
            createGeoTree();
        int[] cells = geoTree.getCells(p);
        cellsLoop:
        for (int i = 0; i < cells.length; i++) {
            int c = cells[i];
            for (int j = 0; j < trueNSpace; j++) {
                if (cellExtents[j][c] > p[j]) {
                    continue cellsLoop;
                }
                if (cellExtents[j + trueNSpace][c] < p[j]) {
                    continue cellsLoop;
                }
            }
            Cell cell = getCell(c);
            Cell[] simplices = cell.triangulation();
            switch (trueNSpace) {
                case 3:
                    for (Cell tetra : simplices) {
                        if (tetra.getType() == CellType.TETRA) {
                            SimplexPosition result = ((Tetra) tetra).barycentricCoords(p, coords);
                            if (result != null) {
                                result.setCell(cell);
                                result.setCells(cells.clone());
                                return result;
                            }
                        }
                    }
                    break;
                case 2:
                    for (Cell simplex : simplices) {
                        if (simplex.getType() == CellType.TRIANGLE) {
                            SimplexPosition result = ((Triangle) simplex).barycentricCoords(p, coords);
                            if (result != null) {
                                result.setCell(cell);
                                result.setCells(cells.clone());
                                return result;
                            }
                        }
                    }
            }
        }
        return null;
    }

    @Override
    public boolean getFieldCoords(float[] p, SimplexPosition result)
    {
        Cell[] simplices;
        float[] res;
        int[] cells;
        switch (trueNSpace) {
            case 3:
                res = getBarycentricCoords((Tetra) result.getSimplex(), p);
                if (res != null) {
                    result.setVertices(result.getSimplex().getVertices());
                    result.setCoords(res);
                    return true;
                }
                if (result.getCell() != null && result.getCell().getType() != CellType.TETRA) {
                    simplices = result.getCell().triangulation();
                    for (Cell simplex : simplices) {
                        if (simplex.getType() == CellType.TETRA) {
                            res = getBarycentricCoords((Tetra) simplex, p);
                            if (res != null) {
                                result.setSimplex(simplex);
                                result.setVertices(result.getSimplex().getVertices());
                                result.setCoords(res);
                                return true;
                            }
                        }
                    }
                }
                if (result.getCells() != null) {
                    cLoop:
                    for (int i = 0; i < result.getCells().length; i++) {
                        int c = result.getCells()[i];
                        if (getCell(c) == result.getCell()) {
                            continue;
                        }
                        for (int j = 0; j < trueNSpace; j++) {
                            if (cellExtents[j][c] > p[j] || cellExtents[j + trueNSpace][c] < p[j]) {
                                continue cLoop;
                            }
                        }
                        Cell cell = getCell(c);
                        simplices = cell.triangulation();
                        for (Cell simplex : simplices) {
                            if (simplex.getType() == CellType.TETRA) {
                                res = getBarycentricCoords((Tetra) simplex, p);
                                if (res != null) {
                                    result.setSimplex(simplex);
                                    result.setCell(cell);
                                    result.setVertices(result.getSimplex().getVertices());
                                    result.setCoords(res);
                                    return true;
                                }
                            }
                        }
                    }
                }
                cells = geoTree.getCells(p);
                cellsLoop:
                for (int i = 0; i < cells.length; i++) {
                    int c = cells[i];
                    for (int j = 0; j < trueNSpace; j++) {
                        if (cellExtents[j][c] > p[j] || cellExtents[j + trueNSpace][c] < p[j]) {
                            continue cellsLoop;
                        }
                    }
                    simplices = getCell(c).triangulation();
                    for (Cell simplex : simplices) {
                        if (simplex.getType() == CellType.TETRA) {
                            res = getBarycentricCoords((Tetra) simplex, p);
                            if (res != null) {
                                result.setSimplex(simplex);
                                result.setCell(getCell(c));
                                result.setCells(cells);
                                result.setVertices(result.getSimplex().getVertices());
                                result.setCoords(res);
                                return true;
                            }
                        }
                    }
                }
                break;
            case 2:
                res = getBarycentricCoords((Triangle) result.getSimplex(), p);
                if (res != null) {
                    result.setVertices(result.getSimplex().getVertices());
                    result.setCoords(res);
                    return true;
                }
                if (result.getCell() != null && result.getCell().getType() != CellType.TRIANGLE) {
                    simplices = result.getCell().triangulation();
                    for (Cell simplice : simplices) {
                        if (simplice.getType() == CellType.TETRA) {
                            res = getBarycentricCoords((Triangle) simplice, p);
                            if (res != null) {
                                result.setSimplex(simplice);
                                result.setVertices(result.getSimplex().getVertices());
                                result.setCoords(res);
                                return true;
                            }
                        }
                    }
                }
                if (result.getCells() != null) {
                    cLoop:
                    for (int i = 0; i < result.getCells().length; i++) {
                        int c = result.getCells()[i];
                        if (getCell(c) == result.getCell()) {
                            continue;
                        }
                        for (int j = 0; j < trueNSpace; j++) {
                            if (cellExtents[j][c] > p[j] || cellExtents[j + trueNSpace][c] < p[j]) {
                                continue cLoop;
                            }
                        }
                        Cell cell = getCell(c);
                        simplices = cell.triangulation();
                        for (Cell simplex : simplices) {
                            if (simplex.getType() == CellType.TRIANGLE) {
                                res = getBarycentricCoords((Triangle) simplex, p);
                                if (res != null) {
                                    result.setSimplex(simplex);
                                    result.setCell(cell);
                                    result.setVertices(result.getSimplex().getVertices());
                                    result.setCoords(res);
                                    return true;
                                }
                            }
                        }
                    }
                }
                cells = geoTree.getCells(p);
                cellsLoop:
                for (int i = 0; i < cells.length; i++) {
                    int c = cells[i];
                    for (int j = 0; j < trueNSpace; j++) {
                        if (cellExtents[j][c] > p[j]) {
                            continue cellsLoop;
                        }
                        if (cellExtents[j + trueNSpace][c] < p[j]) {
                            continue cellsLoop;
                        }
                    }
                    simplices = getCell(c).triangulation();
                    for (Cell simplex : simplices) {
                        if (simplex.getType() == CellType.TRIANGLE) {
                            res = getBarycentricCoords((Triangle) simplex, p);
                            if (res != null) {
                                result.setSimplex(simplex);
                                result.setCell(getCell(c));
                                result.setCells(cells);
                                result.setVertices(result.getSimplex().getVertices());
                                result.setCoords(res);
                                return true;
                            }
                        }
                    }
                }
                break;
        }
        return false;
    }

    @Override
    public IrregularField getTriangulated()
    {
        IrregularField outField = cloneDeep();
        for (int i = 0; i < cellSets.size(); i++) {
            CellSet cs = cellSets.get(i);
            CellSet trCs = cs.getTriangulated();
            trCs.generateDisplayData((FloatLargeArray) timeCoords.getValue(currentTime));
            outField.replaceCellSet(i, trCs);
        }
        outField.checkTrueNSpace();
        return outField;
    }

    @Override
    public LongLargeArray getIndices(int axis)
    {
        LongLargeArray out = new LongLargeArray(getNElements(), false);
        for (long i = 0; i < out.length(); i++) {
            out.setLong(i, i);
        }
        return out;
    }

    /**
     * Returns true if this field has cells of a given type, false otherwise.
     *
     * @param type cell type
     *
     * @return true if this field has cells of a given type, false otherwise
     */
    public boolean hasCellsType(CellType type)
    {
        if (type.getValue() < 0 || type.getValue() >= Cell.getNProperCellTypes()) {
            return false;
        }

        if (cellSets == null || cellSets.isEmpty()) {
            return false;
        }

        for (int i = 0; i < cellSets.size(); i++) {
            if (cellSets.get(i).getCellArray(type) != null) {
                return true;
            }

            CellArray[] bndrs = cellSets.get(i).getBoundaryCellArrays();
            if (bndrs != null) {
                for (CellArray bndr : bndrs) {
                    if (bndr != null && bndr.getType() == type) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Returns true if this field has point cells, false otherwise.
     *
     * @return true if this field has point cells, false otherwise
     */
    public boolean hasCellsPoint()
    {
        return hasCellsType(CellType.POINT);
    }

    /**
     * Returns true if this field has segment cells, false otherwise.
     *
     * @return true if this field has segment cells, false otherwise
     */
    public boolean hasCellsSegment()
    {
        return hasCellsType(CellType.SEGMENT);
    }

    /**
     * Returns true if this field has triangle cells, false otherwise.
     *
     * @return true if this field has triangle cells, false otherwise
     */
    public boolean hasCellsTriangle()
    {
        return hasCellsType(CellType.TRIANGLE);
    }

    /**
     * Returns true if this field has quadrilateral cells, false otherwise.
     *
     * @return true if this field has quadrilateral cells, false otherwise
     */
    public boolean hasCellsQuad()
    {
        return hasCellsType(CellType.QUAD);
    }

    /**
     * Returns true if this field has tetraheron cells, false otherwise.
     *
     * @return true if this field has tetraheron cells, false otherwise
     */
    public boolean hasCellsTetra()
    {
        return hasCellsType(CellType.TETRA);
    }

    /**
     * Returns true if this field has pyramid cells, false otherwise.
     *
     * @return true if this field has pyramid cells, false otherwise
     */
    public boolean hasCellsPyramid()
    {
        return hasCellsType(CellType.PYRAMID);
    }

    /**
     * Returns true if this field has prism cells, false otherwise.
     *
     * @return true if this field has prism cells, false otherwise
     */
    public boolean hasCellsPrism()
    {
        return hasCellsType(CellType.PRISM);
    }

    /**
     * Returns true if this field has hexahedron cells, false otherwise.
     *
     * @return true if this field has hexahedron cells, false otherwise
     */
    public boolean hasCellsHexahedron()
    {
        return hasCellsType(CellType.HEXAHEDRON);
    }

    /**
     * Returns true if this field has 1D cells, false otherwise.
     *
     * @return true if this field has 1D cells, false otherwise
     */
    public boolean hasCells1D()
    {
        return (hasCellsSegment());
    }

    /**
     * Returns true if this field has 2D cells, false otherwise.
     *
     * @return true if this field has 2D cells, false otherwise
     */
    public boolean hasCells2D()
    {
        return (hasCellsTriangle() || hasCellsQuad());
    }

    /**
     * Returns true if this field has 3D cells, false otherwise.
     *
     * @return true if this field has 3D cells, false otherwise
     */
    public boolean hasCells3D()
    {
        return (hasCellsTetra() || hasCellsPyramid() || hasCellsPrism() || hasCellsHexahedron());
    }

    /**
     * Returns the highest dimension of cells in this field.
     *
     * @return the highest dimension of cells in this field
     */
    public int getNCellDims()
    {
        if (hasCells3D()) {
            return 3;
        } else if (hasCells2D()) {
            return 2;
        } else if (hasCells1D()) {
            return 1;
        } else {
            return 0;
        }
    }

}
