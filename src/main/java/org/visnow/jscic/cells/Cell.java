/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.cells;

import java.util.Arrays;
import java.util.Objects;
import static org.apache.commons.math3.util.FastMath.*;
import static org.visnow.jscic.cells.CellType.*;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.utils.MatrixMath;
import org.visnow.jscic.utils.VectorMath;

/**
 * An abstract representation of a cell object.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
abstract public class Cell implements java.io.Serializable, Comparable
{
    private static final long serialVersionUID = -6771283305263607457L;

    protected CellType type;
    protected int nspace;
    protected int[] vertices;
    protected byte orientation;

    /**
     * Returns cell face corresponding to a given cell type.
     *
     * @param type cell type
     *
     * @return cell face
     */
    public static int[] cellTypeToFaceType(CellType type)
    {
        return type.getFaceTypes();
    }

    /**
     * Returns all valid cell types.
     *
     * @return valid cell types
     */
    public static CellType[] getProperCellTypes()
    {
        return new CellType[]{POINT, SEGMENT, TRIANGLE, QUAD, TETRA, PYRAMID, PRISM, HEXAHEDRON};
    }

    /**
     * Returns the number of valid cell types.
     *
     * @return the number of valid cell types
     */
    public static int getNProperCellTypes()
    {
        return getProperCellTypes().length;
    }

    /**
     * Returns all valid 1D cell types.
     *
     * @return all valid 1D cell types
     */
    public static CellType[] getProperCellTypesUpto1D()
    {
        return new CellType[]{POINT, SEGMENT};
    }

    /**
     * Returns the number of valid 1D cell types.
     *
     * @return the number of valid 1D cell types
     */
    public static int getNProperCellTypesUpto1D()
    {
        return getProperCellTypesUpto1D().length;
    }

    /**
     * Returns all valid 1D and 2D cell types.
     *
     * @return all valid 1D and 2D cell types
     */
    public static CellType[] getProperCellTypesUpto2D()
    {
        return new CellType[]{POINT, SEGMENT, TRIANGLE, QUAD};
    }

    /**
     * Returns the number of valid 1D and 2D cell types.
     *
     * @return the number of valid 1D and 2D cell types
     */
    public static int getNProperCellTypesUpto2D()
    {
        return getProperCellTypesUpto2D().length;
    }

    /**
     * Creates a new cell object.
     *
     * @param type        the type of cell
     * @param vertices    the coordinates of vertices
     * @param orientation the orientation of a cell
     *
     * @return a new cell object
     *
     */
    public static Cell createCell(CellType type, int[] vertices, byte orientation)
    {
        if (vertices.length != type.getNVertices())
            throw new IllegalArgumentException("vertices.length != type.getNVertices()");
        switch (type) {
            case POINT:
                return new Point(vertices[0], orientation);
            case SEGMENT:
                return new Segment(vertices[0], vertices[1], orientation);
            case TRIANGLE:
                return new Triangle(vertices[0], vertices[1], vertices[2], orientation);
            case QUAD:
                return new Quad(vertices[0], vertices[1], vertices[2], vertices[3], orientation);
            case TETRA:
                return new Tetra(vertices[0], vertices[1], vertices[2], vertices[3], orientation);
            case PYRAMID:
                return new Pyramid(vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], orientation);
            case PRISM:
                return new Prism(vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], vertices[5], orientation);
            case HEXAHEDRON:
                return new Hex(vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], vertices[5], vertices[6], vertices[7], orientation);
            default:
                throw new IllegalArgumentException("Invalid cell type.");
        }
    }

    /**
     * Creates a new cell object.
     *
     * @param type        the type of cell
     * @param n           a dummy param for backward compatibility with nspace
     * @param vertices    the coordinates of vertices
     * @param orientation the orientation of a cell
     *
     * @return a new cell object
     */
    public static Cell createCell(CellType type, int n, int[] vertices, byte orientation)
    {
        return createCell(type, vertices, orientation);
    }
    /**
     * Standard comparator required by the Comparable implementation
     * @param c Cell to be compared
     * @return -1 if type less than cell.type, 1 if type greater than cell.type; 
     * if types are equal, -1 when vertices are lexicographically earlier than cell.vertices,
     * 1  when vertices are lexicographically later than cell.vertices,
     * when vertices are equal to cell vertices, result of comparing orientations
     * 
     * throws ClassCastException when c is not a cell
     */
    @Override
    public int compareTo(Object c)
    {
        if (!(c instanceof Cell))
            throw new ClassCastException();
        Cell cell = (Cell)c;
        if (type.getValue() < cell.type.getValue())
            return -1;
        else if (type.getValue() > cell.type.getValue())
            return 1;
        for (int i = 0; i < vertices.length; i++)
            if (vertices[i] < cell.vertices[i])
                return -1;
            else if (vertices[i] > cell.vertices[i])
                return 1;
        if (orientation < cell.orientation)
            return -1;
        else if (orientation > cell.orientation)
            return 1;
        return 0;
    }    
    
    /**
     * compares cells ignoring orientation sign
     * @param c object to be compared
     * @return false if c is not a cell or is a cell of different type
     * otherwise, it returns true when nodes are identical in both compared cells, false otherwise
     */
    public boolean equalsIgnoreOrientation (Object c)
    {
        if (!(c instanceof Cell))
            return false;
        Cell cell = (Cell)c;
        if (type.getValue() != cell.type.getValue())
            return false;
        return Arrays.equals(vertices, cell.vertices);
    }    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.type);
        hash = 89 * hash + Arrays.hashCode(this.vertices);
        hash = 89 * hash + this.orientation;
        return hash;
    }
    
    /**
     * compares cells 
     * @param c object to be compared
     * @return false if c is not a cell or is a cell of different type
     * otherwise, it returns true when nodes and orientation are identical in both compared cells, false otherwise
     */
    @Override
    public boolean equals(Object c)
    {
        if (!(c instanceof Cell))
            return false;
        Cell cell = (Cell)c;
        if (type.getValue() != cell.type.getValue())
            return false;
        return orientation == cell.orientation && Arrays.equals(vertices, cell.vertices);
    }    
    
    /**
     * Returns a deep copy of this instance.
     *
     * @return a deep copy of this instance
     */
    public Cell cloneDeep()
    {
        return createCell(type, vertices, orientation);
    }

    /**
     * Returns cell type.
     *
     * @return cell type
     */
    public CellType getType()
    {
        return type;
    }

    /**
     * Returns true if the cell is simplex.
     *
     * @return true if the cell is simplex, false otherwise.
     */
    public boolean isSimplex()
    {
        return type.isSimplex();
    }

    /**
     * Returns the dimension of the cell.
     *
     * @return the dimension of the cell.
     */
    public int getDim()
    {
        return type.getDim();
    }

    /**
     * Returns the nspace of the cell.
     *
     * @return the nspace of the cell.
     */
    public int getNspace()
    {
        return nspace;
    }

    /**
     * Returns the number of vertices of the cell.
     *
     * @return the number of vertices of the cell
     */
    public int getNVertices()
    {
        return type.getNVertices();
    }

    /**
     * Returns the vertices of the cell.
     *
     * @return the vertices of the cell
     */
    public int[] getVertices()
    {
        return vertices;
    }

    /**
     * Returns the orientation of the cell.
     *
     * @return the orientation of the cell
     */
    public byte getOrientation()
    {
        return orientation;
    }

    /**
     * Sets the orientation of the cell.
     *
     * @param orientation the orientation
     */
    public void setOrientation(byte orientation)
    {
        this.orientation = orientation == 0 ? (byte) 0 : (byte) 1;
    }

    @Override
    public String toString()
    {
        StringBuffer b = new StringBuffer();
        b.append("[");
        for (int i = 0; i < vertices.length; i++)
            b.append(String.format("%5d ", vertices[i]));
        b.append("]");
        if (orientation == 1)
            b.append(" +");
        else
            b.append(" -");
        return new String(b);
    }

    /**
     * Creates complete list of subcells (faces, edges, vertices).
     *
     * @return array of length (dim+1) of arrays of cells
     *         (Cells[i] is array of subcells of dimension i)
     */
    abstract public Cell[][] subcells();

    /**
     * Creates array of cell faces.
     *
     * @return array faces
     */
    abstract public Cell[] faces();

    /**
     * Creates array of cell faces for a given set of node indices and orientation.
     *
     * @param nodes       nodes
     * @param orientation orientation
     *
     * @return array faces
     */
    abstract public Cell[] faces(int[] nodes, byte orientation);

    /**
     * Divides into array of simplices.
     *
     * @return array of simplices of triangulation
     */
    abstract public Cell[] triangulation();

    /**
     * Divides into array of simplices.
     *
     * @return array of simplices of triangulation
     */
    abstract public int[][] triangulationVertices();

    /**
     * Reorders vertices to increasing order (as long as possible) modifying orientation if necessary.
     *
     * @return array of reordered vertices
     */
    abstract protected int[] normalize();

    /**
     * Returns a byte describing compatibility with x.
     *
     * @param x compared cell
     *
     * @return
     *
     * 0 if x is of different dimension
     *
     * 0 if x has no common face
     *
     * 1 if x has the same type, vertices and orientation
     *
     * -1 if x has the same type and vertices but opposite orientation
     *
     * 2 if x has common face and compatible orientation
     *
     * -2 if x has common face but opposite orientation
     *
     * 3 if x is a compatibly oriented face of this
     *
     * -3 if x is a oppositely oriented face of this
     *
     * 4 if this is a compatibly oriented face of x
     *
     * -4 if this is a oppositely oriented face of x
     */
    public byte compare(Cell x)
    {
        Cell[] f;
        Cell[] xf;
        if (x.getDim() == this.getDim()) {
            if (x.getNVertices() == this.getNVertices()) {
                int[] v = x.getVertices();
                boolean eq = true;
                for (int i = 0; i < v.length; i++)
                    if (v[i] != vertices[i]) {
                        eq = false;
                        break;
                    }
                if (eq && (x.getOrientation() == orientation))
                    return 1;
                else if (eq)
                    return -1;
            }
            f = this.faces();
            xf = x.faces();
            if (f != null && xf != null)
                for (Cell f1 : f)
                    for (Cell xf1 : xf) {
                        if (f1.compare(xf1) == 1)
                            return -2;
                        if (f1.compare(xf1) == -1)
                            return 2;
                    }
            return 0;
        }
        if (x.getDim() == this.getDim() - 1) {
            f = this.faces();
            if (f != null)
                for (Cell f1 : f) {
                    if (f1.compare(x) == 1)
                        return 3;
                    if (f1.compare(x) == -1)
                        return -3;
                }
            return 0;
        }
        if (x.getDim() == this.getDim() + 1) {
            xf = x.faces();
            if (xf != null)
                for (Cell xf1 : xf) {
                    if (xf1.compare(this) == 1)
                        return 4;
                    if (xf1.compare(this) == -1)
                        return -4;
                }
            return 0;
        }
        return 0;
    }

    /**
     * Returns a byte describing compatibility with x of a cell of the same type with vertices v.
     *
     * @param v array of vertex numbers
     *
     * @return
     *
     * 0 if length of v is different from number of vertices of this
     *
     * 0 if x has no common face
     *
     * 1 if x has the same type, vertices and orientation
     *
     * -1 if x has the same type and vertices but opposite orientation
     *
     * 2 if x has common face and compatible orientation
     *
     * -2 if x has common face but opposite orientation
     */
    abstract public byte compare(int[] v);

    /**
     * Returns geometric orientation of the cell.
     *
     * @param coords vertex coordinates
     * @param nodes  list of nodes
     *
     * @return orientation of the cell
     *
     */
    public final int geomOrientation(FloatLargeArray coords, int[] nodes)
    {
        int[] orientingVerts = type.getOrientingVerts();
        if (orientingVerts.length < 4 || nodes.length < 4)
            return 0;
        int[] verts = new int[4];
        for (int i = 0; i < verts.length; i++)
            verts[i] = nodes[orientingVerts[i]];
        float[][] v = new float[nspace][nspace];
        float d = 0;
        for (int i = 0; i < nspace; i++)
            for (int j = 0; j < nspace; j++)
                v[i][j] = coords.getFloat(nspace * (long) verts[i + 1] + j) - coords.getFloat(nspace * (long) verts[0] + j);
        switch (nspace) {
            case 1:
                d = v[0][0];
                break;
            case 2:
                d = v[0][0] * v[1][1] - v[0][1] * v[1][0];
                break;
            case 3:
                d = MatrixMath.det3x3(v);
                break;
            default:
                break;
        }
        return (int) (signum(d));
    }

    /**
     * Returns geometric orientation of the cell.
     *
     * @param coords vertex coordinates
     *
     * @return orientation of the cell
     */
    public int geomOrientation(FloatLargeArray coords)
    {
        int[] orientingVerts = type.getOrientingVerts();
        if (orientingVerts.length < 4)
            return 0;
        int[] verts = new int[4];
        for (int i = 0; i < verts.length; i++)
            verts[i] = vertices[orientingVerts[i]];
        float[][] v = new float[3][3];
        float d;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                v[i][j] = coords.getFloat(3 * verts[i + 1] + j) - coords.getFloat(3 * verts[0] + j);
        return (int) (signum(MatrixMath.det3x3(v)));
    }

    /**
     * Returns generalized geometric orientation of the cell.
     *
     * @param coords vertex coordinates
     *
     * @return for a 3d cell, a one-length vector containing signum of orienting reper determinant (standard +-1 orientation cast to a float vector),
     *         for a 2d cell, a standard normal versor
     *         for a 1d cell, line versor
     */
    public float[] generalizedGeomOrientation(FloatLargeArray coords)
    {
        if (type.getDim() < 1)
            return new float[]{0, 0, 0};
        int[] orientingVerts = type.getOrientingVerts();
        int[] verts = new int[orientingVerts.length];
        for (int i = 0; i < verts.length; i++)
            verts[i] = vertices[orientingVerts[i]];
        float[][] v = new float[verts.length - 1][3];
        for (int i = 0; i < verts.length - 1; i++)
            for (int j = 0; j < 3; j++)
                v[i][j] = coords.getFloat(3 * verts[i + 1] + j) - coords.getFloat(3 * verts[0] + j);
        switch (type.getDim()) {
            case 3:
                float d = signum(MatrixMath.det3x3(v));
                return new float[]{d};
            case 2:
                float[] w = VectorMath.crossProduct(v[0], v[1]);
                d = (float) Math.sqrt(w[0] * w[0] + w[1] * w[1] + w[2] * w[2]);
                if (d == 0)
                    return new float[]{0, 0, 0};
                for (int i = 0; i < w.length; i++)
                    w[i] /= d;
                return w;
            case 1:
                w = v[0];
                d = (float) Math.sqrt(w[0] * w[0] + w[1] * w[1] + w[2] * w[2]);
                if (d == 0)
                    return new float[]{0, 0, 0};
                for (int i = 0; i < w.length; i++)
                    w[i] /= d;
                return w;
            default:
                return new float[]{};
        }
    }

    /**
     * Returns geometric orientation of the cell.
     *
     * @param coords vertex coordinates
     * @param normal vector normal to the surface
     *
     * @return orientation of the cell
     */
    public int geomOrientation(FloatLargeArray coords, float[] normal)
    {
        if (type != TRIANGLE && type != QUAD || normal == null || vertices[0] < 0 || vertices[1] < 0 || vertices[2] < 0)
            return 0;
        float[][] v = new float[3][3];
        float d = 0;
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 3; j++)
                v[i][j] = coords.getFloat(3 * vertices[i + 1] + j) - coords.getFloat(3 * vertices[0] + j);
        System.arraycopy(normal, 0, v[2], 0, 3);
        switch (nspace) {
            case 1:
                d = v[0][0];
                break;
            case 2:
                d = v[0][0] * v[1][1] - v[0][1] * v[1][0];
                break;
            case 3:
                d = MatrixMath.det3x3(v);
                break;
            default:
                break;
        }
        return (int) (signum(d));
    }

    /**
     * Utility subdividing a quad according to the convention of lowest vertex at the diagonal.
     *
     * @param quad numbers of four quad vertices (ordered as in Quad class)
     *
     * @return array describing two triangles of quad triangulation
     */
    protected static int[][] diagonalSubdiv(int[] quad)
    {
        if (quad == null || quad.length != 4)
            throw new IllegalArgumentException("quad == null || quad.length != 4");
        if ((quad[0] < quad[1] && quad[0] < quad[3]) ||
            (quad[2] < quad[1] && quad[2] < quad[3]))
            return new int[][]{{quad[0], quad[1], quad[2]}, {quad[0], quad[2], quad[3]}};
        else
            return new int[][]{{quad[1], quad[2], quad[3]}, {quad[1], quad[3], quad[0]}};
    }

    /**
     * Utility subdividing a quad according to the convention of lowest vertex at the diagonal.
     *
     * @param quad numbers of four quad vertices (ordered as in Quad class)
     *
     * @return array describing two triangles of quad triangulation
     */
    protected static int[][] diagonalSubdivIndices(int[] quad)
    {
        if (quad == null || quad.length != 4)
            throw new IllegalArgumentException("quad == null || quad.length != 4");
        if ((quad[0] < quad[1] && quad[0] < quad[3]) ||
            (quad[2] < quad[1] && quad[2] < quad[3]))
            return new int[][]{{0, 1, 2}, {0, 2, 3}};
        else
            return new int[][]{{1, 2, 3}, {1, 3, 0}};
    }

    /**
     * Returns the measure of the cell (length, area, volume).
     *
     * @param coords coordinates
     *
     * @return the measure of the cell
     */
    public float getMeasure(FloatLargeArray coords)
    {
        long i0, i1, i2, i3;
        float[] v1 = new float[3];
        float[] v2 = new float[3];
        float[] v3 = new float[3];
        float r;
        switch (type) {
            case POINT:
                return 1;
            case SEGMENT:
                r = 0;
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++)
                    r += (coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i)) * (coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i));
                return (float) (sqrt(r));
            case TRIANGLE:
                r = 0;
                i2 = vertices[2];
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++) {
                    v1[i] = coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i);
                    v2[i] = coords.getFloat(3 * i2 + i) - coords.getFloat(3 * i0 + i);
                }
                v3 = VectorMath.crossProduct(v1, v2);
                for (int i = 0; i < 3; i++)
                    r += v3[i] * v3[i];
                return (float) (sqrt(r)) / 2;
            case TETRA:
                r = 0;
                i3 = vertices[3];
                i2 = vertices[2];
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++) {
                    v1[i] = coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i);
                    v2[i] = coords.getFloat(3 * i2 + i) - coords.getFloat(3 * i0 + i);
                    v3[i] = coords.getFloat(3 * i3 + i) - coords.getFloat(3 * i0 + i);
                }
                r = MatrixMath.det3x3(new float[][] {v1, v2, v3});
                return abs(r) / 6;
            default:
                Cell[] triang = triangulation();
                r = 0;
                for (Cell triang1 : triang)
                    r += triang1.getMeasure(coords);
                return r;
        }
    }

    /**
     * Returns the signed measure of the cell (length, area, volume).
     * In case of 3D cells, oriented volume is returned.
     *
     * @param coords coordinates
     *
     * @return the signed measure of the cell
     */
    public float getSignedMeasure(FloatLargeArray coords)
    {
        long i0, i1, i2, i3;
        float[] v1 = new float[3];
        float[] v2 = new float[3];
        float[] v3 = new float[3];
        float r;
        switch (type) {
            case POINT:
                return 1;
            case SEGMENT:
                r = 0;
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++)
                    r += (coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i)) * (coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i));
                return (float) (sqrt(r));
            case TRIANGLE:
                r = 0;
                i2 = vertices[2];
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++) {
                    v1[i] = coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i);
                    v2[i] = coords.getFloat(3 * i2 + i) - coords.getFloat(3 * i0 + i);
                }
                v3[0] = v1[1] * v2[2] - v2[1] * v1[2];
                v3[1] = v1[2] * v2[0] - v2[2] * v1[0];
                v3[2] = v1[0] * v2[1] - v2[0] * v1[1];
                for (int i = 0; i < 3; i++)
                    r += v3[i] * v3[i];
                return (float) (sqrt(r)) / 2;
            case TETRA:
                r = 0;
                i3 = vertices[3];
                i2 = vertices[2];
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++) {
                    v1[i] = coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i);
                    v2[i] = coords.getFloat(3 * i2 + i) - coords.getFloat(3 * i0 + i);
                    v3[i] = coords.getFloat(3 * i3 + i) - coords.getFloat(3 * i0 + i);
                }
                r = v1[0] * v2[1] * v3[2] + v1[1] * v2[2] * v3[0] + v1[2] * v2[0] * v3[1] -
                   (v1[2] * v2[1] * v3[0] + v1[0] * v2[2] * v3[1] + v1[1] * v2[0] * v3[2]);
                float sign = this.orientation == 1 ? 1 : -1;
                return sign * r / 6;
            default:
                Cell[] triang = triangulation();
                r = 0;
                for (Cell triang1 : triang)
                    r += triang1.getSignedMeasure(coords);
                return r;
        }
    }
      /**
     * Returns the integral of the given data over the cell with the assumption of linear approximation
     *
     * @param coords coordinates
     * @param data   collection of scalar float data arrays
     *
     * @return a vector of integrals of the data over the cell
     */
    public float[] getIntegrals(FloatLargeArray coords, FloatLargeArray[] data)
    {
        long i0, i1, i2, i3;
        float[] v1 = new float[3];
        float[] v2 = new float[3];
        float[] v3 = new float[3];
        float r;
        float[] integrals = new float[data.length];
        switch (type) {
            case POINT:
                i0 = vertices[0];
                for (int i = 0; i < integrals.length; i++) 
                    integrals[i] = data[i].get(i0);
                break;
            case SEGMENT:
                r = 0;
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++)
                    r += (coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i)) * 
                         (coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i));
                r = (float)sqrt(r) / 2;
                for (int i = 0; i < integrals.length; i++) 
                    integrals[i] = r * (data[i].get(i0) + data[i].get(i1));
                break;
            case TRIANGLE:
                r = 0;
                i2 = vertices[2];
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++) {
                    v1[i] = coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i);
                    v2[i] = coords.getFloat(3 * i2 + i) - coords.getFloat(3 * i0 + i);
                }
                v3 = VectorMath.crossProduct(v1, v2);
                for (int i = 0; i < 3; i++)
                    r += v3[i] * v3[i];
                r = (float) sqrt(r) / 2;
                for (int i = 0; i < integrals.length; i++) 
                    integrals[i] = r * (data[i].get(i0) + data[i].get(i1) + data[i].get(i2));
                break;
            case TETRA:
                i3 = vertices[3];
                i2 = vertices[2];
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++) {
                    v1[i] = coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i);
                    v2[i] = coords.getFloat(3 * i2 + i) - coords.getFloat(3 * i0 + i);
                    v3[i] = coords.getFloat(3 * i3 + i) - coords.getFloat(3 * i0 + i);
                }
                r =  abs(MatrixMath.det3x3(new float[][] {v1, v2, v3})) / 6;
                for (int i = 0; i < integrals.length; i++) 
                    integrals[i] = r * (data[i].get(i0) + data[i].get(i1) + data[i].get(i2) + data[i].get(i3));
                break;
            default:
                Cell[] triang = triangulation();
                for (int i = 0; i < integrals.length; i++) 
                    integrals[i] = 0;
                for (Cell simplex : triang) {
                    float[] integr = simplex.getIntegrals(coords, data);
                    for (int i = 0; i < integr.length; i++) 
                        integrals[i] += integr[i];
                }
                break;
        }
        return integrals;
    }
  
    
}
