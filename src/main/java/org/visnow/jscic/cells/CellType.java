/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.cells;

/**
 * Cell type.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl), University of Warsaw, ICM
 *
 */
public enum CellType
{
//          value   nVertices dim simplex   name           UCDname     pluralName                  nameAnalogies                   faces                              orientingVertices    
    EMPTY     (-1,          0, -1,  false, "empty",       "empty",    "empties",     new String[]{"empty", "null"},                null,                              new int[]{}),
    POINT     ( 0,          1,  0,  true,  "point",       "pt",       "points",      new String[]{"point", "pt"},                  new int[]{1, 0, 0, 0, 0, 0, 0, 0}, new int[]{0}),
    SEGMENT   ( 1,          2,  1,  true,  "line",        "line",     "lines",       new String[]{"line", "segment", "seg"},       new int[]{2, 0, 0, 0, 0, 0, 0, 0}, new int[]{0, 1}),
    TRIANGLE  ( 2,          3,  2,  true,  "triangle",    "tri",      "triangles",   new String[]{"triangle", "tri"},              new int[]{0, 3, 0, 0, 0, 0, 0, 0}, new int[]{0, 1, 2}),  
    QUAD      ( 3,          4,  2,  false, "quadrangle",  "quad",     "quadrangles", new String[]{"quadrangle", "quad"},           new int[]{0, 4, 0, 0, 0, 0, 0, 0}, new int[]{0, 1, 3}),
    TETRA     ( 4,          4,  3,  true,  "tetrahedron", "tet",      "tetrahedra",  new String[]{"tetrahedron", "tetra", "tet"},  new int[]{0, 0, 4, 0, 0, 0, 0, 0}, new int[]{0, 1, 2, 3}),     
    PYRAMID   ( 5,          5,  3,  false, "pyramid",     "pyr",      "pyramids",    new String[]{"pyramid", "pyr"},               new int[]{0, 0, 4, 1, 0, 0, 0, 0}, new int[]{0, 1, 3, 4}), 
    PRISM     ( 6,          6,  3,  false, "prism",       "prism",    "prisms",      new String[]{"prism"},                        new int[]{0, 0, 2, 3, 0, 0, 0, 0}, new int[]{0, 1, 2, 3}), 
    HEXAHEDRON( 7,          8,  3,  false, "hexahedron",  "hex",      "hexahedra",   new String[]{"hexahedron", "hexa", "hex"},    new int[]{0, 0, 0, 6, 0, 0, 0, 0}, new int[]{0, 1, 3, 4}),
    EXPLICIT  ( 8,         -1, -1,  false, "explicit",    "explicit", "explicit",    new String[]{"explicit"},                     null,                              new int[]{}),    
    CUSTOM    ( 9,         -1, -1,  false, "custom",      "custom",   "custom",      new String[]{"custom"} ,                      null,                              new int[]{});    

    private final int value;
    private final int nVertices;
    private final int dim;
    private final boolean simplex;
    private final String name;
    private final String UCDName;
    private final String pluralName;
    private final String[] nameAnalogies;
    private final int[] faces;
    private final int[] orientingVertices;

    CellType(int value, int nVertices, int dim, boolean simplex,
             String name, String UCDName, String pluralName, String[] nameAnalogies,
             int[] faces, int[] orientingVertices)
    {
        this.value = value;
        this.nVertices = nVertices;
        this.dim = dim;
        this.simplex = simplex;
        this.name = name;
        this.UCDName = UCDName;
        this.pluralName = pluralName;
        this.nameAnalogies = nameAnalogies;
        this.faces = faces;
        this.orientingVertices = orientingVertices;
    }

    /**
     * Returns the value of the type.
     *
     * @return value of the type
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Returns number of vertices in a cell of this type
     *
     * @return number of cell vertices
     */
    public int getNVertices()
    {
        return nVertices;
    }

    /**
     * Returns combinatorial dimension a cell of this type
     *
     * @return combinatorial dimension
     */
    public int getDim()
    {
        return dim;
    }

    /**
     * Returns true if the cell corresponding to the cell type is simplex, false otherwise.
     *
     * @return true if the cell corresponding to the cell type is simplex, false otherwise
     */
    public boolean isSimplex()
    {
        return simplex;
    }

    /**
     * Returns the name of the cell corresponding to the cell type.
     *
     * @return the name of the cell corresponding to the cell type.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Returns the UCD name of the cell corresponding to the cell type.
     *
     * @return the UCD name of the cell corresponding to the cell type.
     */
    public String getUCDName()
    {
        return UCDName;
    }

    /**
     * Returns the name of the cell corresponding to the cell type.
     *
     * @return the name of the cell corresponding to the cell type.
     */
    public String getPluralName()
    {
        return pluralName;
    }

    /**
     * Returns the list of name analogies.
     *
     * @return list of name analogies
     */
    public final String[] getNameAnalogies()
    {
        return nameAnalogies;
    }

    /**
     * Returns vertices forming the orienting reper
     *
     * @return orienting vertices
     */
    public final int[] getOrientingVerts()
    {
        return orientingVertices;
    }

    /**
     * Returns types of faces
     *
     * @return face types
     */
    public int[] getFaceTypes()
    {
        return faces;
    }

    /**
     * Returns the cell type corresponding to the given value.
     *
     * @param value cell value
     *
     * @return cell type corresponding to the given value
     */
    public static CellType getType(int value)
    {
        switch (value) {
            case 0:
                return POINT;
            case 1:
                return SEGMENT;
            case 2:
                return TRIANGLE;
            case 3:
                return QUAD;
            case 4:
                return TETRA;
            case 5:
                return PYRAMID;
            case 6:
                return PRISM;
            case 7:
                return HEXAHEDRON;
            case 8:
                return EXPLICIT;
            case 9:
                return CUSTOM;
            default:
                return EMPTY;
        }
    }
}
