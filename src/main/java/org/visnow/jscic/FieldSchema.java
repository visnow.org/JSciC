/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.function.BiPredicate;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.FloatingPointUtils;
import static org.visnow.jscic.utils.FloatingPointUtils.*;
import org.visnow.jscic.utils.ScalarMath;

/**
 *
 * Holds general information about Field.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class FieldSchema extends DataContainerSchema
{

    public static final String[] COORD_NAMES = {"__coord x", "__coord y", "__coord z"};
    private static final long serialVersionUID = -6398543170859587900L;

    protected float[][] extents = {{MAX_NUMBER_FLOAT, MAX_NUMBER_FLOAT, MAX_NUMBER_FLOAT},
                                   {MIN_NUMBER_FLOAT, MIN_NUMBER_FLOAT, MIN_NUMBER_FLOAT}};
    protected float[][] preferredExtents = {{MAX_NUMBER_FLOAT, MAX_NUMBER_FLOAT, MAX_NUMBER_FLOAT},
                                            {MIN_NUMBER_FLOAT, MIN_NUMBER_FLOAT, MIN_NUMBER_FLOAT}};
    protected double[][] physMappingCoeffs = {{1, 0}, {1, 0}, {1, 0}};
    protected String[] coordsUnits = {"1", "1", "1"};
    protected String timeUnit = "1";
    protected boolean intializedPreferredExtents = false;

    /**
     * Creates a new instance of FieldSchema with default field name
     */
    public FieldSchema()
    {
        this("Field name");
    }

    /**
     * Creates a new instance of FieldSchema., automatically adding pseudocomponent descriptions for field coordinates
     *
     * @param name field name
     */
    public FieldSchema(String name)
    {
        super(name);
        for (String COORD_NAME : COORD_NAMES) {
            pseudoComponentSchemas.add(new DataArraySchema(COORD_NAME, "", new HashMap <>(), 
                                                           DataArrayType.FIELD_DATA_FLOAT, 1, 1, false, 
                                                           0, 1, 0, 1, 0, 1, 
                                                           MAX_NUMBER_FLOAT, MAX_NUMBER_FLOAT, MAX_NUMBER_FLOAT, 
                                                           null, false));
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || !(o instanceof FieldSchema))
            return false;
        FieldSchema fs = (FieldSchema) o;
        return super.equals(o) && Arrays.deepEquals(this.extents, fs.extents) && Arrays.deepEquals(this.preferredExtents, fs.preferredExtents) && Arrays.deepEquals(this.physMappingCoeffs, fs.physMappingCoeffs) &&
            Arrays.equals(this.coordsUnits, fs.coordsUnits);
    }
     /* Compares two FieldsSchemas. Returns true if names, and user defined attributes are compatible in this field and in the input field, false otherwise.
     *
     * @param	f field to be compared
     * @param   compat a lambda specifying data component equivalence, e.g. 
     * <code>(u, v) -&gt; (u.isNumeric == v.isNumeric) &amp;&amp; (u.getVectorLength() == v.getVectorLength())</code>
     *
     * @return	true if all components are compatible in both schemas, false otherwise.
     */
    public boolean isDataCompatibleWith(FieldSchema s, BiPredicate<DataArraySchema, DataArraySchema> compat)
    {
        return isCompatibleWith(s, compat);
    }
  
    @Override
    public int hashCode()
    {
        int hash = super.hashCode();
        hash = 47 * hash + Arrays.deepHashCode(this.extents);
        hash = 47 * hash + Arrays.deepHashCode(this.preferredExtents);
        hash = 47 * hash + Arrays.deepHashCode(this.physMappingCoeffs);
        hash = 47 * hash + Arrays.deepHashCode(this.coordsUnits);
        hash = 47 * hash + (this.intializedPreferredExtents == true ? 1 : 0);
        return hash;
    }

    @Override
    public FieldSchema cloneDeep()
    {
        FieldSchema clone = new FieldSchema(this.name);
        ArrayList<DataArraySchema> componentSchemasClone = new ArrayList<>();
        if (this.componentSchemas != null && this.componentSchemas.size() > 0) {
            for (DataArraySchema item : this.componentSchemas) componentSchemasClone.add(item.cloneDeep());
        }
        clone.componentSchemas = componentSchemasClone;
        if (coordsUnits != null) {
            System.arraycopy(coordsUnits, 0, clone.coordsUnits, 0, coordsUnits.length);
        }
        clone.timeUnit = timeUnit;

        ArrayList<DataArraySchema> pseudoComponentSchemasClone = new ArrayList<>();
        if (this.pseudoComponentSchemas != null && this.pseudoComponentSchemas.size() > 0) {
            for (DataArraySchema item : this.pseudoComponentSchemas) pseudoComponentSchemasClone.add(item.cloneDeep());
        }
        clone.pseudoComponentSchemas = pseudoComponentSchemasClone;
        clone.intializedPreferredExtents = intializedPreferredExtents;

        clone.extents = new float[2][3];
        for (int i = 0; i < extents.length; i++)
            System.arraycopy(this.extents[i], 0, clone.extents[i], 0, 3);
        clone.preferredExtents = new float[2][3];
        for (int i = 0; i < preferredExtents.length; i++)
            System.arraycopy(this.preferredExtents[i], 0, clone.preferredExtents[i], 0, 3);
        clone.physMappingCoeffs = new double[3][2];
        for (int i = 0; i < physMappingCoeffs.length; i++)
            System.arraycopy(this.physMappingCoeffs[i], 0, clone.physMappingCoeffs[i], 0, 2);
        return clone;
    }

    /**
     * Returns field extents. This method always returns a reference to the internal array.
     *
     * @return field extents
     */
    public float[][] getExtents()
    {
        return extents;
    }

    /**
     * Returns field preferred extents. This method always returns a reference to the internal array.
     *
     * @return field extents
     */
    public float[][] getPreferredExtents()
    {
        if (intializedPreferredExtents == false) {
            return extents;
        }
        return preferredExtents;
    }

    /**
     * Returns a diameter of this field.
     *
     * @return diameter of this field
     */
    public float getDiameter()
    {
        double d = 0;
        for (int i = 0; i < extents[0].length; i++)
            d += (extents[1][i] - extents[0][i]) * (extents[1][i] - extents[0][i]);
        return (float) Math.sqrt(d) / 2;
    }

    /**
     * Sets field extents.
     *
     * @param extents new value of the field extents.
     */
    public void setExtents(float[][] extents)
    {
        if (extents == null) {
            throw new IllegalArgumentException("extents == null");
        }

        for (int j = 0; j < extents[0].length; j++) {
            this.extents[0][j] = FloatingPointUtils.processNaNs(extents[0][j], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
            this.extents[1][j] = FloatingPointUtils.processNaNs(extents[1][j], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
            if (this.extents[1][j] < this.extents[0][j]) {
                throw new IllegalArgumentException("extents[1][j] < extents[0][j]");
            }
        }
    }

    /**
     * Sets field preferred extents. Linear mapping coefficients between preferred extents and preferred physical extents remain unchanged.
     *
     * @param preferredExtents new value of the field preferred extents.
     */
    public void setPreferredExtents(float[][] preferredExtents)
    {
        if (preferredExtents == null) {
            throw new IllegalArgumentException("extents == null");
        }

        intializedPreferredExtents = true;
        for (int j = 0; j < preferredExtents[0].length; j++) {
            this.preferredExtents[0][j] = FloatingPointUtils.processNaNs(preferredExtents[0][j], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
            this.preferredExtents[1][j] = FloatingPointUtils.processNaNs(preferredExtents[1][j], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
            if (this.preferredExtents[1][j] < this.preferredExtents[0][j]) {
                throw new IllegalArgumentException("preferredExtents[1][j] < preferredExtents[0][j]");
            }
        }
        float[][] physExtents = getPreferredPhysicalExtents();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < getNPseudoComponents(); j++) {
                if (getPseudoComponentSchema(i).getName().equals(FieldSchema.COORD_NAMES[i])) {
                    DataArraySchema sch = getPseudoComponentSchema(i);
                    double min = this.preferredExtents[0][i];
                    double max = this.preferredExtents[1][i];
                    if (min == max) {
                        if (min == 0.0) {
                            min = -0.1;
                            max = 0.1;
                        } else {
                            min -= 0.1 * abs(min);
                            max += 0.1 * abs(max);
                        }
                    }
                    sch.setPreferredRanges(min, max, FloatingPointUtils.processNaNs(physExtents[0][i], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), FloatingPointUtils.processNaNs(physExtents[1][i], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction));
                }
            }
        }
    }

    /**
     * Sets preferred extents. Linear mapping coefficients between preferred extents and physical preferred extents are computed and stored instead of
     * physicalExtents array.
     *
     * @param preferredExtents         new preferred extents.
     * @param preferredPhysicalExtents new preferred physical extents
     */
    public void setPreferredExtents(float[][] preferredExtents, float[][] preferredPhysicalExtents)
    {
        if (preferredExtents == null || preferredPhysicalExtents == null || preferredExtents.length != preferredPhysicalExtents.length || preferredExtents[0].length != preferredPhysicalExtents[0].length) {
            throw new IllegalArgumentException("extents == null || physExtents == null || extents.length != physExtents.length || extents[0].length != physExtents[0].length");
        }
        intializedPreferredExtents = true;
        for (int j = 0; j < preferredExtents[0].length; j++) {
            this.preferredExtents[0][j] = FloatingPointUtils.processNaNs(preferredExtents[0][j], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
            this.preferredExtents[1][j] = FloatingPointUtils.processNaNs(preferredExtents[1][j], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
            if (this.preferredExtents[1][j] < this.preferredExtents[0][j]) {
                throw new IllegalArgumentException("extents[1][j] < extents[0][j]");
            }
            this.physMappingCoeffs[j] = ScalarMath.linearMappingCoefficients((double)this.preferredExtents[0][j], (double)this.preferredExtents[1][j], (double)FloatingPointUtils.processNaNs(preferredPhysicalExtents[0][j], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), (double)FloatingPointUtils.processNaNs(preferredPhysicalExtents[1][j], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction));
            if (this.physMappingCoeffs[j][0] < 0) {
                throw new IllegalArgumentException("physMappingCoeffs[j][0] < 0");
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < getNPseudoComponents(); j++) {
                if (getPseudoComponentSchema(i).getName().equals(FieldSchema.COORD_NAMES[i])) {
                    DataArraySchema sch = getPseudoComponentSchema(i);
                    double min = this.preferredExtents[0][i];
                    double max = this.preferredExtents[1][i];
                    if (min == max) {
                        if (min == 0.0) {
                            min = -0.1;
                            max = 0.1;
                        } else {
                            min -= 0.1 * abs(min);
                            max += 0.1 * abs(max);
                        }
                    }
                    sch.setPreferredRanges(min, max, FloatingPointUtils.processNaNs(preferredPhysicalExtents[0][i], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), FloatingPointUtils.processNaNs(preferredPhysicalExtents[1][i], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction));
                }
            }
        }
    }

    /**
     * Returns physical extents.
     *
     * @return physical extents.
     */
    public float[][] getPhysicalExtents()
    {
        float[][] physExtents = new float[extents.length][extents[0].length];
        for (int i = 0; i < extents.length; i++) {
            for (int j = 0; j < extents[0].length; j++) {
                physExtents[i][j] = (float)(physMappingCoeffs[j][0] * extents[i][j] + physMappingCoeffs[j][1]);
            }
        }
        return physExtents;
    }
    
    /**
     * Returns a copy of coefficients for linear mapping of coordinates to physical values.
     *
     * @return a copy of coefficients for linear mapping of coordinates to physical values
     */
    public double[][] getPhysicalExtentsMappingCoefficients()
    {
        return physMappingCoeffs.clone();
    }

    /**
     * Returns preferred physical extents.
     *
     * @return preferred physical extents.
     */
    public float[][] getPreferredPhysicalExtents()
    {
        if (intializedPreferredExtents == false) {
            return getPhysicalExtents();
        }
        float[][] physExtents = new float[preferredExtents.length][preferredExtents[0].length];
        for (int i = 0; i < preferredExtents.length; i++) {
            for (int j = 0; j < preferredExtents[0].length; j++) {
                physExtents[i][j] = (float)(physMappingCoeffs[j][0] * preferredExtents[i][j] + physMappingCoeffs[j][1]);
            }
        }
        return physExtents;
    }

    /**
     * Returns units of coordinates. This method always returns a reference to the internal array.
     *
     * @return units of coordinates
     */
    public String[] getCoordsUnits()
    {
        return coordsUnits;
    }

    /**
     * Returns the unit of coordinates for a given dimension.
     *
     * @param dim dimension (0, 1, or 2)
     *
     * @return the unit of coordinates for a given dimension
     */
    public String getCoordsUnit(int dim)
    {
        if (dim < 0 || dim > 2) {
            throw new IllegalArgumentException("dim < 0 || dim > 2");
        }
        return coordsUnits[dim];
    }

    /**
     * Sets units of coordinates.
     *
     * @param coordsUnits new value of units of coordinates. This array is not copied.
     */
    public void setCoordsUnits(String[] coordsUnits)
    {
        if (coordsUnits != null && coordsUnits.length != 3) {
            throw new IllegalArgumentException("The length of  units of coordinates should be 3");
        }
        if (coordsUnits == null) {
            for (int i = 0; i < 3; i++) {
                this.coordsUnits[i] = "1";
            }
        } else {
            for (int i = 0; i < 3; i++) {
                if (coordsUnits[i].isEmpty()) {
                    this.coordsUnits[i] = "1";
                } else {
                    this.coordsUnits[i] = coordsUnits[i];
                }
            }
        }
    }

    /**
     * Sets unit of coordinates.
     *
     * @param coordsUnit new value of unit of coordinates.
     */
    public void setCoordsUnit(String coordsUnit)
    {
        if (coordsUnit == null || coordsUnit.isEmpty()) {
            for (int i = 0; i < 3; i++) {
                this.coordsUnits[i] = "1";
            }
        } else {
            for (int i = 0; i < 3; i++) {
                this.coordsUnits[i] = coordsUnit;
            }
        }
    }

    /**
     * Returns unit of time.
     *
     * @return unit of time
     */
    public String getTimeUnit()
    {
        return timeUnit;
    }

    /**
     * Sets unit of time.
     *
     * @param timeUnit new unit of time
     */
    public void setTimeUnit(String timeUnit)
    {
        if (timeUnit == null || timeUnit.isEmpty()) {
            this.timeUnit = "1";
        } else {
            this.timeUnit = timeUnit;
        }
    }
}
