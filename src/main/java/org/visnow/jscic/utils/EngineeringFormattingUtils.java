/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import static org.visnow.jscic.utils.NumberUtils.*;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * Methods for formatting numbers. Plain formatting (without exponent field) and engineering notation formatting are provided.
 * Plain formatting can be forced by using Integer.MIN_VALUE and MAX_VALUE as engineering low and high exponent.
 * Engineering notation uses exponent of form 3*k.
 *
 * Main assumption here is:<b>not to change value in formatting process!</b>
 * So if one wants to format:
 * 12345.34 using 5 digits but with atLeast1FractionDigit flag turned on then he gets 12345.3 (and not 12345.0)
 * 12345.34 using 3 digits he gets 12345 (and not 12300)
 *
 * Engineering notation always starts from non 0 digit.
 *
 * Formatting methods here assume unlimited precision, so if "0.1" is passed it's considered as "0.1000000..." and not as rounded "0.089" or "0.12345" or
 * something similar. This gives us proper value for floating point numbers like 0.1f, which don't have real representation in 32bit float
 * number, but they are formatted like that in Float.toString, which is also the reason of using Float.toString as input to BigDecimal
 * (read BigDecimal(String val) documentation).
 *
 * Possible formats are:
 * - integer like: 123
 * - small float: 123.4
 * - smaller float: 0.123
 * - float with precision zeroes: 0.12300
 * - large float: 12.345E9
 * - large float shorter: 12E9
 * - very small float: 12.345E-9
 * - additional HTML version 12.34 * 10^-9
 *
 * Rules are:
 * 1. To not change value in formatting process!
 * 2. Engineering notation always starts from non 0 digit (unless it comparesTo ZERO).
 * 3. Formatting methods here assume unlimited argument precision in plain form. Such number is then rounded to digits / exponent.
 *
 * Note:
 * 1. This library assumes that input values are "good enough" to do Java provided arithmetics, that's why formatInContext may not work properly
 * if Double.MIN_VALUE and Double.MAX_VALUE are passed in one set.
 * 2. Infinity and NaN values are not supported (exception is thrown)
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class EngineeringFormattingUtils
{

    /**
     * Trims trailing zeroes from number in plain format.
     *
     * @param numberString          number string in plain format
     * @param atLeast1FractionDigit {@code true} if fraction digit should be always present (like in 123.0)
     *
     * @return numberString with trailing zeroes trimmed
     */
    private static String trimZeroes(String numberString, boolean atLeast1FractionDigit)
    {
        if (numberString.contains(".")) {
            numberString = numberString.replaceAll("([^\\.])0+$", "$1");
            if (!atLeast1FractionDigit) {
                numberString = numberString.replaceAll("\\.0$", "");
            }
        }
        return numberString;
    }

    /**
     * Converts number string in scientific notation (including E sign) into form "a * 10^b" using html entities like middot and superscript.
     *
     * @param numberString number string in scientific notation (including E sign)
     * @param addSpaces    if {@code true} then additional nbsp are inserted in between all components
     *
     * @return number converted to html form
     */
    private static String convertToHtml(String numberString, boolean addSpaces)
    {
        return numberString.replaceAll("E(.+$)", addSpaces ? "&nbsp;&middot;&nbsp;10<sup>$1</sup>" : "&middot;10<sup>$1</sup>");
    }

    /**
     * Returns closest number divisible by 3 less then or equal to the argument.
     *
     * @param number input number
     *
     * @return closest number divisible by 3 less then or equal to the argument
     */
    private static int floorTo3(int number)
    {
        return (int) floor(number / 3.0) * 3;
    }

    /**
     * Returns closest number divisible by 3 greater then or equal to the argument.
     *
     * @param number input number
     *
     * @return closest number divisible by 3 greater then or equal to the argument
     */
    private static int ceilTo3(int number)
    {
        return floorTo3(number + 2);
    }

    /**
     * Returns engineering exponent less then or equal to this exponent.
     *
     * @param exponent input exponent
     *
     * @return engineering exponent less then or equal to this exponent
     */
    private static int engineeringExponent(int exponent)
    {
        return floorTo3(exponent);
    }

    /**
     * Returns engineering exponent. For 0.01 returns -3.
     *
     * @param number input number
     *
     * @return engineering exponent
     */
    private static int engineeringExponent(BigDecimal number)
    {
        return floorTo3(mostSignificantExponent(number));
    }

    /**
     * Returns length of integer part of mantissa of number string in engineering notation.
     * Returns:
     * 3 for 123.456E6
     * 2 for 23.456E6
     * 1 for 9.999E6
     *
     * @param number input number
     *
     * @return length of integer part of mantissa
     */
    private static int engineeringDigitCount(BigDecimal number)
    {
        int mostExp = mostSignificantExponent(number);
        return mostExp - floorTo3(mostExp) + 1;
    }

    /**
     * Converts number to engineering form by adjusting internal state of BigDecimal and rounding if necessary to obtain expected precision.
     *
     * Conversion works by properly setting up internal state of BigDecimal (unscaledValue, scale).
     * After that, number is prepared to conversion into string.
     *
     * Parameter number is considered as with unlimited precision, and such number is going to be rounded if necessary.
     * So 9.9 is considered as 9.9000000.... and 123.4 is 123.400000...
     *
     * @param number                 input number
     * @param minimumPrecisionDigits expected minimum number of significant digits
     * @param atLeast1FractionDigit  {@code true} if fraction digit should be always present (like in 12000.0 or 12.0E3 or 12345.678)
     *
     * @return converted number prepared to further conversion into string with respect to minimumPrecisionDigits and atLeast1FractionDigit
     */
    private static BigDecimal engineeringFormDigitPrecision(BigDecimal number, int minimumPrecisionDigits, boolean atLeast1FractionDigit)
    {
        if (minimumPrecisionDigits < 1) throw new IllegalArgumentException(String.format("Wrong argument: %s", minimumPrecisionDigits));

        int fractionDigit = (atLeast1FractionDigit ? 1 : 0);

        if (number.compareTo(BigDecimal.ZERO) == 0)
            return BigDecimal.ZERO.setScale(max(fractionDigit, minimumPrecisionDigits - 1));
        else {
            int engExp = engineeringExponent(number);
            BigDecimal roundedNumber = number.setScale(number.scale() - number.precision() + minimumPrecisionDigits, RoundingMode.HALF_UP);
            //test if not went to higher exponent
            if (engExp != engineeringExponent(roundedNumber))
                return engineeringFormDigitPrecision(roundedNumber, minimumPrecisionDigits, atLeast1FractionDigit);
            else
                return number.setScale(max(-engExp + fractionDigit, number.scale() - number.precision() + minimumPrecisionDigits), RoundingMode.HALF_UP);
        }
    }

    /**
     * Converts number to plain form by adjusting internal state of BigDecimal and rounding if necessary to obtain expected precision.
     *
     * Conversion works by properly setting up internal state of BigDecimal (unscaledValue, scale).
     * After that, number is prepared to conversion into string.
     *
     * Resulting internal scale of BigDecimal is at least 0. It's at least 1 if atLeast1FractionDigit is true.
     *
     * Parameter number is considered as with unlimited precision, and such number is going to be rounded if necessary.
     * So 9.9 is considered as 9.9000000.... and 123.4 is 123.400000...
     *
     * @param number                 input number
     * @param minimumPrecisionDigits expected minimum number of significant digits
     * @param atLeast1FractionDigit  {@code true} if fraction digit should be always present (like in 12000.0 or 12345.678)
     *
     * @return converted number prepared to further conversion into string with respect to minimumPrecisionDigits and atLeast1FractionDigit
     */
    private static BigDecimal plainFormDigitPrecision(BigDecimal number, int minimumPrecisionDigits, boolean atLeast1FractionDigit)
    {
        if (minimumPrecisionDigits < 1) throw new IllegalArgumentException(String.format("Wrong argument: %s", minimumPrecisionDigits));

        int fractionDigit = (atLeast1FractionDigit ? 1 : 0);

        if (number.compareTo(BigDecimal.ZERO) == 0)
            return BigDecimal.ZERO.setScale(max(fractionDigit, minimumPrecisionDigits - 1));
        else {
            number = number.setScale(max(fractionDigit, number.scale() - number.precision() + minimumPrecisionDigits), RoundingMode.HALF_UP);
            //precision too large (rounded 999 to 1000)
            if (number.scale() > fractionDigit && number.precision() > minimumPrecisionDigits)
                number = number.setScale(number.scale() - 1);
            return number;
        }
    }

    /**
     * Converts number to engineering form by adjusting internal state of BigDecimal and rounding if necessary to obtain expected precision.
     *
     * Conversion works by properly setting up internal state of BigDecimal (unscaledValue, scale).
     * After that, number is prepared to conversion into string.
     *
     * Parameter number is considered as with unlimited precision, and such number is going to be rounded if necessary.
     * So 9.9 is considered as 9.9000000.... and 123.4 is 123.400000...
     *
     * When converting ZERO we assume precision digits == 1.
     *
     * @param number                   input number
     * @param leastSignificantExponent expected least significant exponent
     * @param atLeast1FractionDigit    {@code true} if fraction digit should be always present (like in 12.0E3 or 12.345E6)
     *
     * @return converted number prepared to further conversion into string with respect to leastSignificantExponent and atLeast1FractionDigit
     */
    private static BigDecimal engineeringFormExponentPrecision(BigDecimal number, int leastSignificantExponent, boolean atLeast1FractionDigit)
    {
//     This one is pretty difficult, so one needs to consider following cases:
//     %0.9E6
//     %9E6
//     %90E6
//     %900E6
//     %0.1E6
//     %1E6
//     %10E6
//     %100E6
//     where % is least significant exponent position (with or without fraction digit).

        int fractionDigit = (atLeast1FractionDigit ? 1 : 0);

        if (number.compareTo(BigDecimal.ZERO) == 0)
            return BigDecimal.ZERO.setScale(atLeast1FractionDigit && leastSignificantExponent == engineeringExponent(leastSignificantExponent) ? 1 - leastSignificantExponent : -leastSignificantExponent);
        else {
            int mse = mostSignificantExponent(number);
            int engLse = engineeringExponent(leastSignificantExponent);
            //special case: not to round 900E6 into 1.0E9 (nor 0.9E9) (lse: 9)
            if (atLeast1FractionDigit && engLse == mse + 1)
                return engineeringFormExponentPrecision(number, leastSignificantExponent - 1, atLeast1FractionDigit);
            else {

                int engNum = engineeringExponent(number);
                if (engLse > engNum) {
                    //special case: not to round  90E6 into 0.1E9 (nor 100E6) (lse: 9)
                    BigDecimal roundedNumber = number.setScale(-engLse + fractionDigit, RoundingMode.HALF_UP);
                    if (atLeast1FractionDigit && mse == engLse - 2 && roundedNumber.compareTo(BigDecimal.ZERO) != 0)// mostSignificantExponent(roundedNumber) == engLse - 1)
                        return number.setScale(-engNum + fractionDigit, RoundingMode.HALF_UP);
                    else return roundedNumber;
                } else {
                    BigDecimal roundedNumber = number.setScale(-leastSignificantExponent, RoundingMode.HALF_UP);
                    //test if went to higher exponent
                    if (engineeringExponent(roundedNumber) > engNum && roundedNumber.compareTo(BigDecimal.ZERO) != 0)
                        return roundedNumber;
                    else
                        return number.setScale(max(-leastSignificantExponent, -engNum + fractionDigit), RoundingMode.HALF_UP);
                }
            }
        }
    }

    /**
     * Converts number to plain form by adjusting internal state of BigDecimal and rounding if necessary to obtain expected precision.
     *
     * @param number                   input number
     * @param leastSignificantExponent expected least significant exponent
     * @param atLeast1FractionDigit    {@code true} if fraction digit should be always present (like in 12.0E3 or 12.345E6)
     *
     * Parameter number is considered as with unlimited precision, and such number is going to be rounded if necessary.
     * So 9.9 is considered as 9.9000000.... and 123.4 is 123.400000...
     *
     * @return converted number prepared to further conversion into string with respect to leastSignificantExponent and atLeast1FractionDigit
     */
    private static BigDecimal plainFormExponentPrecision(BigDecimal number, int leastSignificantExponent, boolean atLeast1FractionDigit)
    {
        return number.setScale(max(atLeast1FractionDigit ? 1 : 0, -leastSignificantExponent), RoundingMode.HALF_UP);
    }

    /**
     * Converts number to engineering string.
     * Number needs to have precision large enough to show in engineering format, otherwise IllegalArgumentException is thrown.
     * So 1.2E5 is incorrect. It should be at least 1.20E5 -> 120E3 or 1.200E5 -> 120.0E3
     *
     * Non-zero numbers are formatted in one of format:
     * D[.DDDD...]Eexponent
     * DD[.DDDD...]Eexponent
     * DDD[.DDDD...]Eexponent
     * while zero has always precision == 1 so it is formatted like:
     * 0Eexponent
     * 0.0Eexponent
     * 0.00Eexponent
     *
     * @param number                input number
     * @param atLeast1FractionDigit {@code true} if fraction digit should be always present (like in 12.0E3 or 12.345E6)
     * @param preserveZeroes        {@code true} if trailing zeroes should be present (like in 12.00E3)
     *
     * @return number converted to string in engineering format with respect to atLeast1FractionDigit and preserveZeroes
     *
     * @throws IllegalArgumentException if number doesn't have enough precision digits (scale) to show in engineering notation.
     */
    private static String formatEngineering(BigDecimal number, boolean atLeast1FractionDigit, boolean preserveZeroes)
    {
        int fractionDigit = (atLeast1FractionDigit ? 1 : 0);
        int engNum;

        if (number.compareTo(BigDecimal.ZERO) == 0) {
            int zeroMSE = mostSignificantExponent(number);
            engNum = ceilTo3(zeroMSE);
            if (zeroMSE == engNum && atLeast1FractionDigit)
                throw new IllegalArgumentException(String.format("%s has too small precision to show in engineering format", number));
        } else {
            if (engineeringDigitCount(number) + fractionDigit > number.precision())
                throw new IllegalArgumentException(String.format("%s has too small precision to show in engineering format", number));
            engNum = engineeringExponent(number);
        }

        String numberString = number.divide(new BigDecimal("1e" + engNum)).toPlainString();
        if (preserveZeroes) return numberString + "E" + engNum;
        //this could be done in BigDecimal, but it's way easier in string
        else return trimZeroes(numberString, atLeast1FractionDigit) + "E" + engNum;
    }

    /**
     * Converts number to plain string.
     * Number needs to have precision large enough to show in plain format, otherwise IllegalArgumentException is thrown.
     * So 1.2E5 is incorrect. It should be at least 1.20000E5 -> 120000 or 1.200000E5 -> 120000.0
     *
     * @param number                input number
     * @param atLeast1FractionDigit {@code true} if fraction digit should be always present (like in 120000.0)
     * @param preserveZeroes        {@code true} if trailing zeroes should be present (like in 34.000)
     *
     * @return number converted to string in plain format with respect to atLeast1FractionDigit and preserveZeroes
     *
     * @throws IllegalArgumentException if number doesn't have enough precision digits (scale) to show in plain notation.
     */
    private static String formatPlain(BigDecimal number, boolean atLeast1FractionDigit, boolean preserveZeroes)
    {
        int fractionDigit = (atLeast1FractionDigit ? 1 : 0);

        if (number.precision() < mostSignificantExponent(number) + 1 + fractionDigit)
            throw new IllegalArgumentException(String.format("%s has too small precision to show in plain format", number));

        String numberString = number.toPlainString();
        if (preserveZeroes) return numberString;
        //this could be done in BigDecimal, but it's way easier in string
        else return trimZeroes(numberString, atLeast1FractionDigit);
    }

    /**
     * Convert number into string with respect to minimumPrecisionDigits.
     * Results in engineering format if output number is out of (10^engineeringExponentLow..10^engineeringExponentHigh) range,
     * otherwise plain format is used.
     *
     * One of main assumption is: <b>if you can round to precisionDigits - do it!</b>
     * So: 0.0009992 should be formatted in 2 digits to 0.0010 and not to 999E-6
     *
     * Larger precision than expected appears when integer part of number (or integer part of exponential form) is longer then minimumPrecisionDigits.
     * Larger precision than expected may appear also in engineering notation, when minimumPrecisionDigits is small (1,2,3).
     *
     * @param number                  input number
     * @param minimumPrecisionDigits  expected minimum number of significant digits
     * @param atLeast1FractionDigit   {@code true} if fraction digit should be always present (like in 12.0)
     * @param preserveZeroes          {@code true} if trailing zeroes should be present (like in 34.000)
     * @param engineeringExponentLow  low border between plain and engineering format
     * @param engineeringExponentHigh high border between plain and engineering format
     *
     * @return number converted to string in plain or engineering format
     */
    public static String formatToPrecision(BigDecimal number, int minimumPrecisionDigits, boolean atLeast1FractionDigit, boolean preserveZeroes, int engineeringExponentLow, int engineeringExponentHigh)
    {
        if (number.compareTo(BigDecimal.ZERO) == 0) { //XXX: added later - double check it!
            if (atLeast1FractionDigit) return new BigDecimal("0.0").toPlainString();
            else return new BigDecimal("0").toPlainString();
        } else {
            if (minimumPrecisionDigits < 1 || engineeringExponentLow >= 0 || engineeringExponentHigh < 0 || engineeringExponentLow >= engineeringExponentHigh)
                throw new IllegalArgumentException(String.format("Wrong arguments: %s %s %s", minimumPrecisionDigits, engineeringExponentLow, engineeringExponentHigh));

            BigDecimal numberEngineering = engineeringFormDigitPrecision(number, minimumPrecisionDigits, atLeast1FractionDigit);
            int mostExp = mostSignificantExponent(numberEngineering);
            if (mostExp < engineeringExponentLow || mostExp >= engineeringExponentHigh)
                return formatEngineering(numberEngineering, atLeast1FractionDigit, preserveZeroes);
            //can jump out from engineering notation when using non-standard engineeringLow/High exponents
            //even if it's correct: 0.0099 (digit:1, 1f, engLow:-3) correct as 9.9E-3 but should be formatted to 0.01
            else if (mostExp == engineeringExponentLow) {
                BigDecimal numberPlain = plainFormDigitPrecision(number, minimumPrecisionDigits, atLeast1FractionDigit);
                int mostExpPlain = mostSignificantExponent(numberPlain);
                if (mostExpPlain > engineeringExponentLow && mostExpPlain < engineeringExponentHigh)
                    return formatPlain(numberPlain, atLeast1FractionDigit, preserveZeroes);
                else return formatEngineering(numberEngineering, atLeast1FractionDigit, preserveZeroes);
            } else
                return formatPlain(plainFormDigitPrecision(number, minimumPrecisionDigits, atLeast1FractionDigit), atLeast1FractionDigit, preserveZeroes);
        }
    }

    /**
     * Convert number into string with respect to leastSignificantExponent.
     * Results in engineering format if output number is out of (10^engineeringExponentLow..10^engineeringExponentHigh) range,
     * otherwise plain format is used.
     *
     * Larger precision than expected appears when leastSignificantExponent is somewhere in the middle of two subsequent engineering exponents
     * or is greater than 0 but output format is plain.
     *
     * @param number                   input number
     * @param leastSignificantExponent expected least significant exponent
     * @param atLeast1FractionDigit    {@code true} if fraction digit should be always present (like in 12.0E3 or 12.345E6)
     * @param preserveZeroes           {@code true} if trailing zeroes should be present (like in 12.00E3)
     * @param engineeringExponentLow   low border between plain and engineering format
     * @param engineeringExponentHigh  high border between plain and engineering format
     * @param digitZEROReplacement     if &gt; 0 then replaces ZERO with formatToPrecision, otherwise formats to exponent
     *
     * @return number converted to string in plain or engineering format
     */
    public static String formatToExponent(BigDecimal number, int leastSignificantExponent, boolean atLeast1FractionDigit, boolean preserveZeroes, int engineeringExponentLow, int engineeringExponentHigh, int digitZEROReplacement)
    {
        if (engineeringExponentLow >= 0 || engineeringExponentHigh < 0 || engineeringExponentLow >= engineeringExponentHigh)
            throw new IllegalArgumentException(String.format("Wrong arguments: %s %s", engineeringExponentLow, engineeringExponentHigh));

        BigDecimal numberEngineering = engineeringFormExponentPrecision(number, leastSignificantExponent, atLeast1FractionDigit);
        int mostExp = mostSignificantExponent(numberEngineering);
        //special case for ZERO big decimal - it's formatted in other way - see formatToEngineering
        if (numberEngineering.compareTo(BigDecimal.ZERO) == 0) {
            if (digitZEROReplacement > 0)
                return formatToPrecision(number, digitZEROReplacement, atLeast1FractionDigit, preserveZeroes, engineeringExponentLow, engineeringExponentHigh);
            int engZERO = ceilTo3(mostExp);
            mostExp = max(mostExp, engZERO);
        }
        if (mostExp <= engineeringExponentLow || mostExp >= engineeringExponentHigh)
            return formatEngineering(numberEngineering, atLeast1FractionDigit, preserveZeroes);
        else
            return formatPlain(plainFormExponentPrecision(number, leastSignificantExponent, atLeast1FractionDigit), atLeast1FractionDigit, preserveZeroes);
    }

    /**
     * Format numbers to precision high enough to obtain:
     * difference steps between two different numbers equal at least singleDifference,
     * difference steps between boundary numbers equal at least rangeDifference.
     * This is achieved by finding small enough leastSignificantExponent used in formatToExponent.
     *
     * Difference steps between A and B is number of different numbers +1 that are between A and B (in output precision),
     * so between 0.99 and 1.02 there are 3 steps, but there are 30 steps between 0.990 and 1.020.
     *
     * @param numbers               input numbers
     * @param rangeDifference       minimum number of steps between boundary numbers
     * @param singleDifference      minimum number of steps between subsequent numbers
     * @param atLeast1FractionDigit {@code true} if fraction digit should be always present (like in 12.0E3 or 12.345E6)
     *
     * @return numbers converted to string in plain or engineering format
     */
    public static String[] formatInContext(double[] numbers, int rangeDifference, int singleDifference, boolean atLeast1FractionDigit)
    {
        if (singleDifference < 1 || rangeDifference < 1)
            throw new IllegalArgumentException(String.format("Differences have to be positive: %s %s ", rangeDifference, singleDifference));

        if (numbers.length == 0) return new String[0];

        Map<Double, String> formattedNumbers = new HashMap<>();
        for (int i = 0; i < numbers.length; i++)
            formattedNumbers.put(numbers[i], null);

        Double[] uniqueNumbers = formattedNumbers.keySet().toArray(new Double[formattedNumbers.size()]);
        Arrays.sort(uniqueNumbers);

        String[] numberStrings = new String[numbers.length];

        if (uniqueNumbers.length == 1)
            formattedNumbers.put(numbers[0], formatToPrecision(new BigDecimal(Double.toString(numbers[0])), 3, atLeast1FractionDigit, false, -4, 6));
        else {
            double rangePrecision = uniqueNumbers[uniqueNumbers.length - 1] - uniqueNumbers[0];
            double singlePrecision = Double.MAX_VALUE;
            for (int i = 1; i < uniqueNumbers.length; i++)
                singlePrecision = min(singlePrecision, uniqueNumbers[i] - uniqueNumbers[i - 1]);

            rangePrecision /= rangeDifference;
            singlePrecision /= singleDifference;

            int precisionExponent = min(mostSignificantExponent(new BigDecimal(Double.toString(rangePrecision))),
                                        mostSignificantExponent(new BigDecimal(Double.toString(singlePrecision))));

            for (Double number : uniqueNumbers)
                formattedNumbers.put(number, formatToExponent(new BigDecimal(Double.toString(number)), precisionExponent, atLeast1FractionDigit, false, -4, 6, 1));
        }

        for (int i = 0; i < numberStrings.length; i++)
            numberStrings[i] = formattedNumbers.get(numbers[i]);

        return numberStrings;
    }

    /**
     * Format numbers to obtain:
     * difference steps between two different subsequent numbers equal at least singleDifference.
     * Numbers need to be in monotonic order.
     *
     * Difference steps between A and B is number of different numbers +1 that are between A and B (in output precision),
     * so between 0.99 and 1.02 there are 3 steps, but there are 30 steps between 0.990 and 1.020.
     *
     * @param numbers               input numbers in monotonic order
     * @param singleDifference      minimum number of steps between subsequent numbers
     * @param atLeast1FractionDigit {@code true} if fraction digit should be always present (like in 12.0E3 or 12.345E6)
     *
     * @return numbers converted to string in plain or engineering format
     */
    public static String[] formatInPairs(double[] numbers, int singleDifference, boolean atLeast1FractionDigit)
    {
        if (numbers.length == 0) return new String[]{};
        else if (numbers.length == 1)
            return new String[]{format(numbers[0])};
        else {
            String[] formattedNumbers = new String[numbers.length];

            formattedNumbers[0] = formatInContext(new double[]{numbers[0], numbers[1]}, singleDifference, singleDifference, atLeast1FractionDigit)[0];
            for (int i = 1; i < numbers.length; i++)
                formattedNumbers[i] = formatInContext(new double[]{numbers[i - 1], numbers[i]}, singleDifference, singleDifference, atLeast1FractionDigit)[1];
            return formattedNumbers;
        }
    }

    /**
     * Utility method, a shortcut to {@link #formatInContext(double[], int, int, boolean) }.
     *
     * @param numbers input numbers
     *
     * @return numbers converted to string in plain or engineering format
     */
    public static String[] formatInContext(double[] numbers)
    {
        return formatInContext(numbers, 300, 1, true);
    }

    /**
     * Utility method, a shortcut to {@link #formatInContext(double[], int, int, boolean) }.
     *
     * @param numbers input numbers
     *
     * @return numbers converted to string in plain or engineering format
     */
    public static String[] formatInContext(float[] numbers)
    {
        return formatInContext(NumberUtils.unbox(NumberUtils.convertToDouble(NumberUtils.box(numbers))), 300, 1, true);
    }

    /**
     * Converts numbers into strings using {@link #formatInContext(double[], int, int, boolean) }
     * and than applying html filter such that if output number is in scientific notation (includes E sign)
     * than its output form is "a * 10^b" using html entities like middot and superscript.
     *
     * @param numbers input numbers
     *
     * @return numbers converted to html form
     */
    public static String[] formatInContextHtml(double[] numbers)
    {
        String[] formattedNumbers = formatInContext(numbers);
        for (int i = 0; i < formattedNumbers.length; i++)
            formattedNumbers[i] = convertToHtml(formattedNumbers[i], true);

        return formattedNumbers;
    }

    /**
     * Converts numbers into strings using {@link #formatInContext(double[], int, int, boolean) }
     * and than applying html filter such that if output number is in scientific notation (includes E sign)
     * than its output form is "a * 10^b" using html entities like middot and superscript.
     *
     * @param numbers input numbers
     *
     * @return numbers converted to html form
     */
    public static String[] formatInContextHtml(float[] numbers)
    {
        String[] formattedNumbers = formatInContext(numbers);
        for (int i = 0; i < formattedNumbers.length; i++)
            formattedNumbers[i] = convertToHtml(formattedNumbers[i], true);

        return formattedNumbers;
    }

    /**
     * Utility method, a shortcut to {@link #formatToPrecision(java.math.BigDecimal, int, boolean, boolean, int, int)}
     *
     *
     * @param number                 input number
     * @param minimumPrecisionDigits expected minimum number of significant digits
     *
     * @return number converted to string
     */
    public static String format(BigDecimal number, int minimumPrecisionDigits)
    {
        //engineering low from 10^-3 could be also pretty good solution
        return formatToPrecision(number, minimumPrecisionDigits, true, false, -4, 6);
    }

    /**
     * Utility method, a shortcut to {@link #formatToPrecision(java.math.BigDecimal, int, boolean, boolean, int, int)}
     *
     * @param number input number
     *
     * @return number converted to string
     */
    public static String format(BigDecimal number)
    {
        return format(number, 3);
    }

    /**
     * Utility method, a shortcut to {@link #formatToPrecision(java.math.BigDecimal, int, boolean, boolean, int, int)}
     *
     * @param number input number
     *
     * @return number converted to string
     */
    public static String format(float number)
    {
        return format(toBigDecimal(number));
    }

    /**
     * Utility method, a shortcut to {@link #formatToPrecision(java.math.BigDecimal, int, boolean, boolean, int, int)}
     *
     * @param number input number
     *
     * @return number converted to string
     */
    public static String format(double number)
    {
        return format(toBigDecimal(number));
    }

    /**
     * Formats array of numbers at once. This is utility method, a shortcut to {@link #formatToPrecision(java.math.BigDecimal, int, boolean, boolean, int, int)}
     *
     * @param numbers                input numbers
     * @param minimumPrecisionDigits expected minimum number of significant digits
     * @param atLeast1FractionDigit  {@code true} if fraction digit should be always present (like in 12.0E3 or 12.345E6)
     *
     * @return numbers converted to string
     */
    public static String[] format(BigDecimal[] numbers, int minimumPrecisionDigits, boolean atLeast1FractionDigit)
    {
        String[] numbersFormatted = new String[numbers.length];
        for (int i = 0; i < numbersFormatted.length; i++)
            numbersFormatted[i] = formatToPrecision(numbers[i], minimumPrecisionDigits, atLeast1FractionDigit, false, -4, 6);
        return numbersFormatted;
    }

    /**
     * Formats array of numbers at once. This is utility method, a shortcut to {@link #formatToPrecision(java.math.BigDecimal, int, boolean, boolean, int, int)}
     *
     * @param numbers input numbers
     *
     * @return numbers converted to string
     */
    public static String[] format(BigDecimal[] numbers)
    {
        return format(numbers, 3, true);
    }

    /**
     * Formats array of numbers at once. This is utility method, a shortcut to {@link #formatToPrecision(java.math.BigDecimal, int, boolean, boolean, int, int)}
     *
     * @param number input number
     *
     * @return numbers converted to string
     */
    public static String[] format(float[] number)
    {
        return format(toBigDecimal(number));
    }

    /**
     * Formats array of numbers at once. This is utility method, a shortcut to {@link #formatToPrecision(java.math.BigDecimal, int, boolean, boolean, int, int)}
     *
     * @param number input number
     *
     * @return numbers converted to string
     */
    public static String[] format(double[] number)
    {
        return format(toBigDecimal(number));
    }

    /**
     * Formats array of numbers at once. This is utility method, a shortcut to {@link #formatToPrecision(java.math.BigDecimal, int, boolean, boolean, int, int)}
     *
     * @param numbers                input numbers
     * @param minimumPrecisionDigits expected minimum number of significant digits
     * @param atLeast1FractionDigit  {@code true} if fraction digit should be always present (like in 12.0E3 or 12.345E6)
     *
     *
     * @return numbers converted to string
     */
    public static String[] format(double[] numbers, int minimumPrecisionDigits, boolean atLeast1FractionDigit)
    {
        return format(toBigDecimal(numbers), minimumPrecisionDigits, atLeast1FractionDigit);
    }

    /**
     * Converts number into string using {@link #formatToPrecision(java.math.BigDecimal, int, boolean, boolean, int, int)}
     * and than applying html filter such that if output number is in scientific notation (includes E sign)
     * than its output form is "a * 10^b" using html entities like middot and superscript.
     *
     * @param number input number
     *
     * @return number converted to html form
     */
    public static String formatHtml(BigDecimal number)
    {
        return convertToHtml(format(number), true);
    }

    /**
     * Utility method, a shortcut to {@link #formatHtml(java.math.BigDecimal)}.
     *
     * @param number input number
     *
     * @return number converted to html form
     */
    public static String formatHtml(float number)
    {
        return formatHtml(toBigDecimal(number));
    }

    /**
     * Utility method, a shortcut to {@link #formatHtml(java.math.BigDecimal)}.
     *
     * @param number input number
     *
     * @return number converted to html form
     */
    public static String formatHtml(double number)
    {
        return formatHtml(toBigDecimal(number));
    }

    /**
     * Formats array of numbers at once. This is utility method, a shortcut to {@link #formatHtml(java.math.BigDecimal) }
     *
     * @param numbers input numbers
     *
     * @return numbers converted to html form
     */
    public static String[] formatHtml(BigDecimal[] numbers)
    {
        String[] numbersFormatted = new String[numbers.length];
        for (int i = 0; i < numbersFormatted.length; i++)
            numbersFormatted[i] = formatHtml(numbers[i]);
        return numbersFormatted;
    }

    /**
     * Formats array of numbers at once. This is utility method, a shortcut to {@link #formatHtml(java.math.BigDecimal) }
     *
     * @param numbers input numbers
     *
     * @return numbers converted to html form
     */
    public static String[] formatHtml(float[] numbers)
    {
        return formatHtml(toBigDecimal(numbers));
    }

    /**
     * Formats array of numbers at once. This is utility method, a shortcut to {@link #formatHtml(java.math.BigDecimal) }
     *
     * @param numbers input numbers
     *
     * @return numbers converted to html form
     */
    public static String[] formatHtml(double[] numbers)
    {
        return formatHtml(toBigDecimal(numbers));
    }

    public static void main(String[] args)
    {
        Random r = new Random(123);

        for (int i = 0; i < 20000; i++) {
            double num = 0;
            int lse = 0;
            try {
                num = pow(10, r.nextDouble() * 200 - 100);
                lse = r.nextInt() % 40 - 20;
                formatToExponent(new BigDecimal(Double.toString(num)), lse, true, false, -4, 6, 0);
            } catch (Exception e) {
                System.out.println(i + " " + lse + " " + num);
                System.out.println(e);
            }
        }

        for (int lse = 0; lse > -20; lse--) {
            System.out.println(String.format("%s %s", lse, formatToExponent(new BigDecimal("3E-7"), lse, true, false, -4, 6, 1)));
        }
        System.out.println(formatToExponent(new BigDecimal("3E-7"), -5, true, false, -4, 6, 1));
        System.out.println(formatToExponent(new BigDecimal("0.3E-6"), -3, true, false, -4, 6, 0));
        System.out.println(formatToExponent(new BigDecimal("378E-9"), -4, true, false, -4, 6, 0));
        System.out.println(formatToExponent(new BigDecimal("0.330E-6"), -4, true, false, -4, 6, 0));
        System.out.println(formatToExponent(new BigDecimal("0.0000003"), -4, true, false, -4, 6, 0));
        System.out.println(formatToExponent(new BigDecimal("0.00000033"), -4, true, false, -4, 6, 0));
        System.out.println(formatToExponent(new BigDecimal("0.3E-6"), -3, true, false, -4, 6, 1));

        System.out.println(Arrays.toString(formatInContext(new double[]{123456.12, 33.567})));
        System.out.println(Arrays.toString(formatInContext(new double[]{13.12, 33.567})));
        System.out.println(Arrays.toString(formatInContext(new double[]{3.12, 3.567})));
        System.out.println(Arrays.toString(formatInContext(new double[]{0.00312, 35675656000.0})));
        System.out.println(Arrays.toString(formatInContext(new double[]{35675659001.0, 35675656000.0})));
    }
}
