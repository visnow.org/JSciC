/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.FloatLargeArray;

/**
 * Various operations on matrices.
 *
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class MatrixMath
{

    /**
     * Returns the transpose of the input matrix.
     *
     * @param A input matrix
     *
     * @return transpose of the input matrix
     */
    public static float[][] matrixTranspose(float[][] A)
    {
        if (A == null || A.length < 1 || A[0].length < 1)
            throw new IllegalArgumentException("A == null || A.length < 1 || A[0].length < 1");
        if (A.length == 1 && A[0].length == 1)
            return A;

        float[][] out = new float[A[0].length][A.length];
        for (int i = 0; i < out.length; i++) {
            for (int j = 0; j < out[0].length; j++) {
                out[i][j] = A[j][i];
            }
        }
        return out;
    }

    /**
     * Returns the transpose of the input matrix.
     *
     * @param A input matrix
     *
     * @return transpose of the input matrix
     */
    public static double[][] matrixTranspose(double[][] A)
    {
        if (A == null || A.length < 1 || A[0].length < 1)
            throw new IllegalArgumentException("A == null || A.length < 1 || A[0].length < 1");
        if (A.length == 1 && A[0].length == 1)
            return A;

        double[][] out = new double[A[0].length][A.length];
        for (int i = 0; i < out.length; i++) {
            for (int j = 0; j < out[0].length; j++) {
                out[i][j] = A[j][i];
            }
        }
        return out;
    }

    /**
     * Returns the matrix product.
     *
     * @param A input matrix
     * @param B input matrix
     *
     * @return matrix product
     */
    public static double[][] matrixMult(double[][] A, double[][] B)
    {
        if (!(A.length > 1 && B.length > 1 && A[0].length > 1 && B[0].length > 1 && A[0].length == B.length))
            throw new IllegalArgumentException("!(A.length > 1 && B.length > 1 && A[0].length > 1 && B[0].length > 1 && A[0].length == B.length)");

        int N = A.length;
        int M = A[0].length;
        int K = B[0].length;
        double[][] out = new double[N][K];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < K; j++) {
                out[i][j] = 0;
                for (int l = 0; l < M; l++) {
                    out[i][j] += A[i][l] * B[l][j];
                }
            }
        }
        return out;
    }

    /**
     * Returns the matrix product.
     *
     * @param A input matrix
     * @param B input matrix
     *
     * @return matrix product
     */
    public static float[][] matrixMult(float[][] A, float[][] B)
    {
        if (!(A.length > 1 && B.length > 1 && A[0].length > 1 && B[0].length > 1 && A[0].length == B.length))
            throw new IllegalArgumentException("!(A.length > 1 && B.length > 1 && A[0].length > 1 && B[0].length > 1 && A[0].length == B.length)");

        int N = A.length;
        int M = A[0].length;
        int K = B[0].length;
        float[][] out = new float[N][K];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < K; j++) {
                out[i][j] = 0;
                for (int l = 0; l < M; l++) {
                    out[i][j] += A[i][l] * B[l][j];
                }
            }
        }
        return out;
    }

    /**
     * Returns the matrix sum.
     *
     * @param A input matrix
     * @param B input matrix
     *
     * @return matrix sum
     */
    public static double[][] matrixAdd(double[][] A, double[][] B)
    {
        if (!(A.length > 1 && B.length > 1 && A[0].length > 1 && B[0].length > 1 && A.length == B.length && A[0].length == B[0].length))
            throw new IllegalArgumentException("!(A.length > 1 && B.length > 1 && A[0].length > 1 && B[0].length > 1 && A.length == B.length && A[0].length == B[0].length)");
        int N = A.length;
        int M = A[0].length;
        double[][] out = new double[N][M];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                out[i][j] = A[i][j] + B[i][j];
            }
        }
        return out;
    }

    /**
     * Returns the matrix-vector product.
     *
     * @param A input matrix
     * @param b input vector
     *
     * @return matrix-vector product
     */
    public static float[] matrixVectorMult(float[][] A, float[] b)
    {
        if (!(A.length > 1 && b.length > 1 && A[0].length > 1 && A.length == b.length))
            throw new IllegalArgumentException("!(A.length > 1 && b.length > 1 && A[0].length > 1 && A.length == b.length)");
        int N = A.length;
        int M = A[0].length;
        float[] out = new float[N];
        for (int i = 0; i < N; i++) {
            out[i] = 0;
            for (int j = 0; j < M; j++) {
                out[i] += A[i][j] * b[j];
            }
        }
        return out;
    }

    /**
     * Returns the matrix-vector product.
     *
     * @param A input matrix
     * @param b input vector
     *
     * @return matrix-vector product
     */
    public static double[] matrixVectorMult(double[][] A, double[] b)
    {
        if (!(A.length > 1 && b.length > 1 && A[0].length > 1 && A.length == b.length))
            throw new IllegalArgumentException("!(A.length > 1 && b.length > 1 && A[0].length > 1 && A.length == b.length)");
        int N = A.length;
        int M = A[0].length;
        double[] out = new double[N];
        for (int i = 0; i < N; i++) {
            out[i] = 0;
            for (int j = 0; j < M; j++) {
                out[i] += A[i][j] * b[j];
            }
        }
        return out;
    }

    /**
     * Return the determinant of a 2 by 2 matrix.
     *
     * @param A 2 by 2 matrix
     *
     * @return the determinant
     */
    public static final float det2x2(float[][] A)
    {
        if (A == null || A.length != 2 || A[0] == null || A[0].length != 2 || A[1] == null || A[1].length != 2) {
            throw new IllegalArgumentException("A is not a 2 by 2 matrix");
        }
        return A[0][0] * A[1][1] - (A[0][1] * A[1][0]);
    }

    /**
     * Return the determinant of a 2 by 2 matrix.
     *
     * @param A 2 by 2 matrix
     *
     * @return the determinant
     */
    public static final double det2x2(double[][] A)
    {
        if (A == null || A.length != 2 || A[0] == null || A[0].length != 2 || A[1] == null || A[1].length != 2) {
            throw new IllegalArgumentException("A is not a 2 by 2 matrix");
        }
        return A[0][0] * A[1][1] - (A[0][1] * A[1][0]);
    }

    /**
     * Return the determinant of a 3 by 3 matrix.
     *
     * @param A 3 by 3 matrix
     *
     * @return the determinant
     */
    public static final float det3x3(float[][] A)
    {
        if (A == null || A.length != 3 || A[0] == null || A[0].length != 3 || A[1] == null || A[1].length != 3 || A[2] == null || A[2].length != 3) {
            throw new IllegalArgumentException("A is not a 3 by 3 matrix");
        }
        return A[0][0] * A[1][1] * A[2][2] + A[0][1] * A[1][2] * A[2][0] + A[0][2] * A[1][0] * A[2][1] -
            (A[0][2] * A[1][1] * A[2][0] + A[0][0] * A[1][2] * A[2][1] + A[0][1] * A[1][0] * A[2][2]);
    }

    /**
     * Return the determinant of a 3 by 3 matrix.
     *
     * @param A 3 by 3 matrix
     *
     * @return the determinant
     */
    public static final double det3x3(double[][] A)
    {
        if (A == null || A.length != 3 || A[0] == null || A[0].length != 3 || A[1] == null || A[1].length != 3 || A[2] == null || A[2].length != 3) {
            throw new IllegalArgumentException("A is not a 3 by 3 matrix");
        }
        return A[0][0] * A[1][1] * A[2][2] + A[0][1] * A[1][2] * A[2][0] + A[0][2] * A[1][0] * A[2][1] -
            (A[0][2] * A[1][1] * A[2][0] + A[0][0] * A[1][2] * A[2][1] + A[0][1] * A[1][0] * A[2][2]);
    }

    /**
     * Solves the eigenproblem of a square matrix using Jacobi algorithm.
     *
     * @param A       original matrix to solve
     * @param D       pre-allocated vector for result eigenvalues
     * @param V       pre-allocated array for result eigenvectors (v[i] is i-th eigenvector respective to i-th eigenvalue)
     * @param maxIter maximum number of iterations
     *
     * @return algorithm status (-1 - error, 1 - result reached in maximum number of iterations, 2 - result precision reached)
     */
    public static final int jacobi(float A[][], float D[], float V[][], int maxIter)
    {
        if (A == null)
            return -1;
        int n = A.length;
        int i, j, p, q, kr;
        float dm, bm, diff, g, t, th, c, s, h;
        if (D == null || D.length != n || V == null || V.length != n)
            return -1;
        for (i = 0; i < n; i++)
            if (A[i] == null || A[i].length != n || V[i] == null || V[i].length != n)
                return -1;

        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++)
                V[i][j] = 0;
            V[i][i] = 1;
            D[i] = A[i][i];
        }
        if (n == 1)
            return 0;
        for (kr = 0; kr < maxIter; kr++) {
            dm = D[0];
            bm = 0;
            p = q = 0;
            for (i = 1; i < n; i++) {
                for (j = 0; j < i; j++)
                    if (abs(A[j][i]) > bm) {
                        p = j;
                        q = i;
                        bm = abs(A[j][i]);
                    }
                if (abs(D[i]) <= dm)
                    dm = abs(D[i]);
            }
            if (dm + bm == dm || bm == 0.) {
                for (i = 0; i < n; i++)
                    for (j = i + 1; j < n; j++) {
                        A[i][j] = A[j][i];
                        h = V[i][j];
                        V[i][j] = V[j][i];
                        V[j][i] = h;
                    }
                return 2;
            }
            diff = D[q] - D[p];
            g = 100 * bm;
            if (g + abs(diff) == abs(diff))
                t = A[p][q] / diff;
            else {
                th = diff / (2 * A[p][q]);
                t = 1 / (float) (abs(th) + sqrt(1. + th * th));
                if (th < 0.)
                    t = -t;
            }
            c = 1 / (float) sqrt(1 + t * t);
            s = t * c;
            h = t * A[p][q];
            D[p] -= h;
            D[q] += h;
            A[p][q] = 0;
            if (p > 0)
                for (i = 0; i < p; i++) {
                    g = A[i][p];
                    h = A[i][q];
                    A[i][p] = c * g - s * h;
                    A[i][q] = s * g + c * h;
                }
            if (p < q - 1)
                for (i = p + 1; i < q; i++) {
                    g = A[p][i];
                    h = A[i][q];
                    A[p][i] = c * g - s * h;
                    A[i][q] = s * g + c * h;
                }
            if (q < n - 1)
                for (i = q + 1; i < n; i++) {
                    g = A[p][i];
                    h = A[q][i];
                    A[p][i] = c * g - s * h;
                    A[q][i] = s * g + c * h;
                }
            for (i = 0; i < n; i++) {
                g = V[i][p];
                h = V[i][q];
                V[i][p] = c * g - s * h;
                V[i][q] = s * g + c * h;
            }
        }
        for (i = 0; i < n; i++)
            for (j = i + 1; j < n; j++) {
                A[i][j] = A[j][i];
                h = V[i][j];
                V[i][j] = V[j][i];
                V[j][i] = h;
            }
        return kr;
    }

    /**
     * Solves the eigenproblem of a square matrix using Jacobi algorithm.
     *
     * @param A       original matrix to solve
     * @param D       pre-allocated vector for result eigenvalues
     * @param V       pre-allocated array for result eigenvectors (v[i] is i-th eigenvector respective to i-th eigenvalue)
     * @param maxIter maximum number of iterations
     *
     * @return algorithm status (-1 - error, 1 - result reached in maximum number of iterations, 2 - result precision reached)
     */
    public static final int jacobi(double A[][], double D[], double V[][], int maxIter)
    {
        if (A == null)
            return -1;
        int n = A.length;
        int i, j, p, q, kr;
        double dm, bm, diff, g, t, th, c, s, h;
        if (D == null || D.length != n || V == null || V.length != n)
            return -1;
        for (i = 0; i < n; i++)
            if (A[i] == null || A[i].length != n || V[i] == null || V[i].length != n)
                return -1;

        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++)
                V[i][j] = 0;
            V[i][i] = 1;
            D[i] = A[i][i];
        }
        if (n == 1)
            return 0;
        for (kr = 0; kr < maxIter; kr++) {
            dm = D[0];
            bm = 0;
            p = q = 0;
            for (i = 1; i < n; i++) {
                for (j = 0; j < i; j++)
                    if (abs(A[j][i]) > bm) {
                        p = j;
                        q = i;
                        bm = abs(A[j][i]);
                    }
                if (abs(D[i]) <= dm)
                    dm = abs(D[i]);
            }
            if (dm + bm == dm || bm == 0.) {
                for (i = 0; i < n; i++)
                    for (j = i + 1; j < n; j++) {
                        A[i][j] = A[j][i];
                        h = V[i][j];
                        V[i][j] = V[j][i];
                        V[j][i] = h;
                    }
                return 2;
            }
            diff = D[q] - D[p];
            g = 100 * bm;
            if (g + abs(diff) == abs(diff))
                t = A[p][q] / diff;
            else {
                th = diff / (2 * A[p][q]);
                t = 1 / (abs(th) + sqrt(1. + th * th));
                if (th < 0.)
                    t = -t;
            }
            c = 1 / sqrt(1 + t * t);
            s = t * c;
            h = t * A[p][q];
            D[p] -= h;
            D[q] += h;
            A[p][q] = 0;
            if (p > 0)
                for (i = 0; i < p; i++) {
                    g = A[i][p];
                    h = A[i][q];
                    A[i][p] = c * g - s * h;
                    A[i][q] = s * g + c * h;
                }
            if (p < q - 1)
                for (i = p + 1; i < q; i++) {
                    g = A[p][i];
                    h = A[i][q];
                    A[p][i] = c * g - s * h;
                    A[i][q] = s * g + c * h;
                }
            if (q < n - 1)
                for (i = q + 1; i < n; i++) {
                    g = A[p][i];
                    h = A[q][i];
                    A[p][i] = c * g - s * h;
                    A[q][i] = s * g + c * h;
                }
            for (i = 0; i < n; i++) {
                g = V[i][p];
                h = V[i][q];
                V[i][p] = c * g - s * h;
                V[i][q] = s * g + c * h;
            }
        }
        for (i = 0; i < n; i++)
            for (j = i + 1; j < n; j++) {
                A[i][j] = A[j][i];
                h = V[i][j];
                V[i][j] = V[j][i];
                V[j][i] = h;
            }
        return kr;
    }

    /**
     * Applies an affine transformation defined by trMatrix to the affine coordinates inAffine.
     *
     * @param inAffine affine coordinates
     * @param trMatrix transformation matrix
     *
     * @return affine coordinates after transformation
     */
    public static float[][] transformAffine(float[][] inAffine, float[][] trMatrix)
    {
        float[][] outAffine = new float[4][3];
        for (int i = 0; i < 3; i++) {
            outAffine[3][i] = trMatrix[i][3];
            for (int j = 0; j < 3; j++) {
                outAffine[i][j] = 0;
                for (int k = 0; k < 3; k++)
                    outAffine[i][j] += trMatrix[k][i] * inAffine[j][k];
            }
        }
        return outAffine;
    }

    public static float[][] computeAffineFromExtents(int dims[], float[][] extents)
    {
        if (dims == null || extents == null || extents.length != 2 || extents[0].length != extents[1].length) {
            throw new IllegalArgumentException("dims == null || extents == null || extents.length != 2 || extents[0].length != extents[1].length");
        }
        float[][] affine = new float[4][3];
        for (int i = 0; i < extents[0].length; i++) {
            affine[3][i] = extents[0][i];
            for (int j = 0; j < 3; j++) {
                affine[j][i] = 0;
            }
            if (i < dims.length && dims[i] > 1) {
                affine[i][i] = (extents[1][i] - extents[0][i]) / (dims[i] - 1);
            }
        }
        for (int i = extents[0].length; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                affine[j][i] = 0;
            }
            affine[i][i] = 1;
        }
        return affine;
    }

    public static float[][] computeAffineFromExtents(long dims[], float[][] extents)
    {
        if (dims == null || extents == null || extents.length != 2 || extents[0].length != extents[1].length) {
            throw new IllegalArgumentException("dims == null || extents == null || extents.length != 2 || extents[0].length != extents[1].length");
        }
        float[][] affine = new float[4][3];
        for (int i = 0; i < extents[0].length; i++) {
            affine[3][i] = extents[0][i];
            for (int j = 0; j < 3; j++) {
                affine[j][i] = 0;
            }
            if (i < dims.length && dims[i] > 1) {
                affine[i][i] = (extents[1][i] - extents[0][i]) / (dims[i] - 1);
            }
        }
        for (int i = extents[0].length; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                affine[j][i] = 0;
            }
            affine[i][i] = 1;
        }
        return affine;
    }

    /**
     * Solves the linear system Ax=b using Gaussian elimination with partial pivoting. Matrix A needs to be square and nonsingular.
     *
     * @param A nonsingular, square matrix
     * @param b right-hand-side.
     *
     * @return x, the solution of Ax=b
     */
    public static float[] lsolve(float[][] A, float[] b)
    {
        int N = b.length;

        for (int p = 0; p < N; p++) {
            // find pivot row and swap
            int max = p;
            for (int i = p + 1; i < N; i++)
                if (abs(A[i][p]) > abs(A[max][p]))
                    max = i;
            float[] temp = A[p];
            A[p] = A[max];
            A[max] = temp;
            float t = b[p];
            b[p] = b[max];
            b[max] = t;

            // singular
            if (A[p][p] == 0.0f)
                throw new IllegalArgumentException("The input matrix is singular");

            // pivot within A and b
            for (int i = p + 1; i < N; i++) {
                float alpha = A[i][p] / A[p][p];
                b[i] -= alpha * b[p];
                for (int j = p; j < N; j++)
                    A[i][j] -= alpha * A[p][j];
            }
        }
        // back substitution
        float[] x = new float[N];
        for (int i = N - 1; i >= 0; i--) {
            float sum = 0.0f;
            for (int j = i + 1; j < N; j++)
                sum += A[i][j] * x[j];
            x[i] = (b[i] - sum) / A[i][i];
        }
        return x;
    }

    /**
     * Solves the linear system Ax=b using Gaussian elimination with partial pivoting. Matrix A needs to be square and nonsingular.
     *
     * @param A nonsingular, square matrix
     * @param b right-hand-side.
     *
     * @return x, the solution of Ax=b
     */
    public static double[] lsolve(double[][] A, double[] b)
    {
        int N = b.length;

        for (int p = 0; p < N; p++) {
            // find pivot row and swap
            int max = p;
            for (int i = p + 1; i < N; i++)
                if (abs(A[i][p]) > abs(A[max][p]))
                    max = i;
            double[] temp = A[p];
            A[p] = A[max];
            A[max] = temp;
            double t = b[p];
            b[p] = b[max];
            b[max] = t;

            // singular
            if (A[p][p] == 0.0)
                throw new IllegalArgumentException("The input matrix is singular");

            // pivot within A and b
            for (int i = p + 1; i < N; i++) {
                double alpha = A[i][p] / A[p][p];
                b[i] -= alpha * b[p];
                for (int j = p; j < N; j++)
                    A[i][j] -= alpha * A[p][j];
            }
        }
        // back substitution
        double[] x = new double[N];
        for (int i = N - 1; i >= 0; i--) {
            double sum = 0.0f;
            for (int j = i + 1; j < N; j++)
                sum += A[i][j] * x[j];
            x[i] = (b[i] - sum) / A[i][i];
        }
        return x;
    }

    /**
     * Solves the linear system Ax=b, where A is a 3 by 3 nonsingular matrix and b is a vector of length 3.
     *
     * @param A nonsingular, 3 by 3 matrix
     * @param b ight-hand-side.
     *
     * @return x, the solution of Ax=b
     */
    public static float[] lsolve3x3(float[][] A, float[] b)
    {
        float[] out = new float[3];
        float det, detx, dety, detz;
        det = A[0][0] * A[1][1] * A[2][2] - A[0][0] * A[1][2] * A[2][1] + A[0][1] * A[1][2] * A[2][0] - A[0][1] * A[1][0] * A[2][2] + A[0][2] * A[1][0] * A[2][1] - A[0][2] * A[1][1] * A[2][0];
        if (det == 0.0f)
            throw new IllegalArgumentException("The input matrix is singular");

        detx = b[0] * A[1][1] * A[2][2] - b[0] * A[1][2] * A[2][1] + A[0][1] * A[1][2] * b[2] - A[0][1] * b[1] * A[2][2] + A[0][2] * b[1] * A[2][1] - A[0][2] * A[1][1] * b[2];
        dety = A[0][0] * b[1] * A[2][2] - A[0][0] * A[1][2] * b[2] + b[0] * A[1][2] * A[2][0] - b[0] * A[1][0] * A[2][2] + A[0][2] * A[1][0] * b[2] - A[0][2] * b[1] * A[2][0];
        detz = A[0][0] * A[1][1] * b[2] - A[0][0] * b[1] * A[2][1] + A[0][1] * b[1] * A[2][0] - A[0][1] * A[1][0] * b[2] + b[0] * A[1][0] * A[2][1] - b[0] * A[1][1] * A[2][0];
        out[0] = detx / det;
        out[1] = dety / det;
        out[2] = detz / det;
        return out;
    }

    /**
     * Solves the linear system Ax=b, where A is a 3 by 3 nonsingular matrix and b is a vector of length 3.
     *
     * @param A nonsingular, 3 by 3 matrix
     * @param b right-hand-side.
     *
     * @return x, the solution of Ax=b
     */
    public static double[] lsolve3x3(double[][] A, double[] b)
    {
        double[] out = new double[3];
        double det, detx, dety, detz;
        det = A[0][0] * A[1][1] * A[2][2] - A[0][0] * A[1][2] * A[2][1] + A[0][1] * A[1][2] * A[2][0] - A[0][1] * A[1][0] * A[2][2] + A[0][2] * A[1][0] * A[2][1] - A[0][2] * A[1][1] * A[2][0];
        if (det == 0.0)
            throw new IllegalArgumentException("The input matrix is singular");

        detx = b[0] * A[1][1] * A[2][2] - b[0] * A[1][2] * A[2][1] + A[0][1] * A[1][2] * b[2] - A[0][1] * b[1] * A[2][2] + A[0][2] * b[1] * A[2][1] - A[0][2] * A[1][1] * b[2];
        dety = A[0][0] * b[1] * A[2][2] - A[0][0] * A[1][2] * b[2] + b[0] * A[1][2] * A[2][0] - b[0] * A[1][0] * A[2][2] + A[0][2] * A[1][0] * b[2] - A[0][2] * b[1] * A[2][0];
        detz = A[0][0] * A[1][1] * b[2] - A[0][0] * b[1] * A[2][1] + A[0][1] * b[1] * A[2][0] - A[0][1] * A[1][0] * b[2] + b[0] * A[1][0] * A[2][1] - b[0] * A[1][1] * A[2][0];
        out[0] = detx / det;
        out[1] = dety / det;
        out[2] = detz / det;
        return out;
    }

    /**
     * Solves the linear system Ax=b, where A is a 3 by 3 nonsingular matrix and b is a vector of length 3.
     *
     * @param A nonsingular, 3 by 3 matrix
     * @param b right-hand-side.
     *
     * @return x, the solution of Ax=b
     */
    public static float[] lsolve2x2(float[][] A, float[] b)
    {
        float[] out = new float[3];
        float det, detx, dety;
        det = A[0][0] * A[1][1] - A[0][1] * A[1][0];
        if (det == 0.0f)
            throw new IllegalArgumentException("The input matrix is singular");

        detx = b[0] * A[1][1] - A[0][1] * b[1];
        dety = A[0][0] * b[1] - b[0] * A[1][0];
        out[0] = detx / det;
        out[1] = dety / det;
        return out;
    }

    /**
     * Solves the linear system Ax=b, where A is a 3 by 3 nonsingular matrix and b is a vector of length 3.
     *
     * @param A nonsingular, 3 by 3 matrix
     * @param b right-hand-side.
     *
     * @return x, the solution of Ax=b
     */
    public static double[] lsolve2x2(double[][] A, double[] b)
    {
        double[] out = new double[3];
        double det, detx, dety;
        det = A[0][0] * A[1][1] - A[0][1] * A[1][0];
        if (det == 0.0f)
            throw new IllegalArgumentException("The input matrix is singular");

        detx = b[0] * A[1][1] - A[0][1] * b[1];
        dety = A[0][0] * b[1] - b[0] * A[1][0];
        out[0] = detx / det;
        out[1] = dety / det;
        return out;
    }

    /**
     * Computes the inverse of the matrix A using Gaussian elimination with partial pivoting. Matrix A needs to be square and nonsingular.
     *
     * @param A    nonsingular, square matrix
     * @param invA output, the inverse of matrix A
     *
     * @return false if error occurred, true otherwise
     */
    public static boolean invert(float[][] A, float[][] invA)
    {
        if (A == null || invA == null)
            return false;
        int n = A.length;
        if (invA.length != n)
            return false;
        for (int i = 0; i < A.length; i++)
            if (A[i] == null || A[i].length != n || invA[i] == null || invA[i].length != n)
                return false;
        float[][] a = new float[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                invA[i][j] = 0;
                a[i][j] = A[i][j];
            }
            invA[i][i] = 1;
        }
        int[] pivot = new int[n];
        boolean[] row = new boolean[n];
        boolean[] col = new boolean[n];
        for (int i = 0; i < pivot.length; i++) {
            pivot[i] = -1;
            row[i] = col[i] = false;
        }
        for (int p = 0; p < n; p++) {
            // find pivot row
            int k = -1, l = -1;
            float v = 0;
            for (int i = 0; i < n; i++) {
                if (row[i])
                    continue;
                for (int j = 0; j < n; j++)
                    if (!col[j] && abs(a[i][j]) > v) {
                        k = i;
                        l = j;
                        v = abs(a[i][j]);
                    }
            }
            if (k == -1)
                return false;
            pivot[k] = l;
            row[k] = col[l] = true;
            v = a[k][l];
            // normalize pivot row and subtract from other rows
            for (int j = 0; j < n; j++) {
                a[k][j] /= v;
                invA[k][j] /= v;
            }
            for (int i = 0; i < n; i++)
                if (i != k) {
                    v = a[i][l];
                    for (int j = 0; j < n; j++) {
                        a[i][j] -= a[k][j] * v;
                        invA[i][j] -= invA[k][j] * v;
                    }
                }
        }
        for (int i = 0; i < n; i++)
            System.arraycopy(invA[i], 0, a[i], 0, n);
        for (int i = 0; i < n; i++) {
            int k = pivot[i];
            System.arraycopy(a[i], 0, invA[k], 0, n);
        }
        // back substitution
        return true;
    }

    /**
     * Computes the inverse of the matrix A using Gaussian elimination with partial pivoting. Matrix A needs to be square and nonsingular.
     *
     * @param A    nonsingular, square matrix
     * @param invA output, the inverse of matrix A
     *
     * @return false if error occurred, true otherwise
     */
    public static boolean invert(double[][] A, double[][] invA)
    {
        if (A == null || invA == null)
            return false;
        int n = A.length;
        if (invA.length != n)
            return false;
        for (int i = 0; i < A.length; i++)
            if (A[i] == null || A[i].length != n || invA[i] == null || invA[i].length != n)
                return false;
        double[][] a = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                invA[i][j] = 0;
                a[i][j] = A[i][j];
            }
            invA[i][i] = 1;
        }
        int[] pivot = new int[n];
        boolean[] row = new boolean[n];
        boolean[] col = new boolean[n];
        for (int i = 0; i < pivot.length; i++) {
            pivot[i] = -1;
            row[i] = col[i] = false;
        }
        for (int p = 0; p < n; p++) {
            // find pivot row
            int k = -1, l = -1;
            double v = 0;
            for (int i = 0; i < n; i++) {
                if (row[i])
                    continue;
                for (int j = 0; j < n; j++)
                    if (!col[j] && abs(a[i][j]) > v) {
                        k = i;
                        l = j;
                        v = abs(a[i][j]);
                    }
            }
            if (k == -1)
                return false;
            pivot[k] = l;
            row[k] = col[l] = true;
            v = a[k][l];
            // normalize pivot row and subtract from other rows
            for (int j = 0; j < n; j++) {
                a[k][j] /= v;
                invA[k][j] /= v;
            }
            for (int i = 0; i < n; i++)
                if (i != k) {
                    v = a[i][l];
                    for (int j = 0; j < n; j++) {
                        a[i][j] -= a[k][j] * v;
                        invA[i][j] -= invA[k][j] * v;
                    }
                }
        }
        for (int i = 0; i < n; i++)
            System.arraycopy(invA[i], 0, a[i], 0, n);
        for (int i = 0; i < n; i++) {
            int k = pivot[i];
            System.arraycopy(a[i], 0, invA[k], 0, n);
        }
        // back substitution
        return true;
    }

    /**
     * Computes the square root of a square matrix A.
     *
     * @param A square matrix
     *
     * @return A^(1/2)
     */
    public static double[][] matrixSqrt(double[][] A)
    {
        int n = A.length;
        for (int i = 0; i < n; i++)
            if (A[i].length != n)
                throw new IllegalArgumentException("A must be a square matrix");

        for (int i = 0; i < n - 1; i++)
            for (int j = i + 1; j < n; j++)
                if (Math.abs(A[i][j] - A[j][i]) >
                    .0001 * (Math.abs(A[i][j]) + Math.abs(A[j][i])))
                    throw new IllegalArgumentException("A must be symmetric");
        double[][] m = new double[n][n];
        double[] d = new double[n];
        double[][] v = new double[n][n];
        double[][] res = new double[n][n];
        for (int i = 0; i < n; i++)
            System.arraycopy(A[i], 0, m[i], 0, n);
        jacobi(m, d, v, 1000);
        for (int i = 0; i < n; i++) {
            if (d[i] < 0)
                throw new IllegalArgumentException("A must be positively semi-definite");
            d[i] = sqrt(abs(d[i]));
        }
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
                res[i][j] = 0;
                for (int k = 0; k < n; k++)
                    res[i][j] += v[k][i] * d[k] * v[k][j];
            }
        return res;
    }

    /**
     * Computes the inverse of the square root of a square matrix A.
     *
     * @param A square matrix
     *
     * @return A^(-1/2)
     */
    public static double[][] matrixSqrtInverse(double[][] A)
    {
        int n = A.length;
        for (int i = 0; i < n; i++)
            if (A[i].length != n)
                throw new IllegalArgumentException("A must be a square matrix");
        for (int i = 0; i < n - 1; i++)
            for (int j = i + 1; j < n; j++)
                if (Math.abs(A[i][j] - A[j][i]) >
                    .0001 * (Math.abs(A[i][j]) + Math.abs(A[j][i])))
                    throw new IllegalArgumentException("A must be symmetric");
        double[][] m = new double[n][n];
        double[] d = new double[n];
        double[][] v = new double[n][n];
        double[][] res = new double[n][n];
        for (int i = 0; i < n; i++)
            System.arraycopy(A[i], 0, m[i], 0, n);
        jacobi(m, d, v, 1000);
        for (int i = 0; i < n; i++) {
            if (d[i] <= 0)
                throw new IllegalArgumentException("A must be positively definite");
            d[i] = 1. / sqrt(abs(d[i]));
        }
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
                res[i][j] = 0;
                for (int k = 0; k < n; k++)
                    res[i][j] += v[k][i] * d[k] * v[k][j];
            }
        return res;
    }

    private static class PullVectorField implements Runnable
    {

        int iThread;
        int nThreads;
        int[] dims;
        float[] coords;
        float[] vec;
        float[] pullVec;

        public PullVectorField(int iThread, int nThreads,
                               int[] dims, float[] coords,
                               float[] vec, float[] pullVec)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.dims = dims;
            this.coords = coords;
            this.vec = vec;
            this.pullVec = pullVec;
        }

        @Override
        public void run()
        {
            if (dims.length == 3) {
                float[][] jacobi = new float[3][3];
                int len = dims[0];
                int slc = dims[0] * dims[1];
                int m = (iThread * dims[2]) / nThreads;
                m *= slc;
                for (int i = (iThread * dims[2]) / nThreads;
                    i < ((iThread + 1) * dims[2]) / nThreads;
                    i++) {
                    for (int j = 0; j < dims[1]; j++)
                        for (int k = 0; k < dims[0]; k++, m++) {
                            for (int l = 0; l < 3; l++) {
                                if (k == 0)
                                    jacobi[l][0] = coords[3 * (m + 1) + l] - coords[3 * m + l];
                                else if (k == dims[0] - 1)
                                    jacobi[l][0] = coords[3 * m + l] - coords[3 * (m - 1) + l];
                                else
                                    jacobi[l][0] = .5f * (coords[3 * (m + 1) + l] - coords[3 * (m - 1) + l]);
                                if (j == 0)
                                    jacobi[l][1] = coords[3 * (m + len) + l] - coords[3 * m + l];
                                else if (j == dims[1] - 1)
                                    jacobi[l][1] = coords[3 * m + l] - coords[3 * (m - len) + l];
                                else
                                    jacobi[l][1] = .5f * (coords[3 * (m + len) + l] - coords[3 * (m - len) + l]);
                                if (i == 0)
                                    jacobi[l][2] = coords[3 * (m + slc) + l] - coords[3 * m + l];
                                else if (i == dims[2] - 1)
                                    jacobi[l][2] = coords[3 * m + l] - coords[3 * (m - slc) + l];
                                else
                                    jacobi[l][2] = .5f * (coords[3 * (m + slc) + l] - coords[3 * (m - slc) + l]);
                            }
                            float[] v = new float[3];
                            System.arraycopy(vec, 3 * m, v, 0, 3);
                            float[] w = null;
                            try {
                                w = lsolve(jacobi, v);
                            } catch (IllegalArgumentException e) {

                            }
                            if (w != null)
                                System.arraycopy(w, 0, pullVec, 3 * m, 3);
                        }

                }
            } else if (dims.length == 2) {
                float[][] jacobi = new float[2][2];
                int len = dims[0];
                int m = (iThread * dims[1]) / nThreads;
                m *= len;
                for (int j = (iThread * dims[1]) / nThreads;
                    j < ((iThread + 1) * dims[1]) / nThreads; j++)
                    for (int k = 0; k < dims[0]; k++, m++) {
                        for (int l = 0; l < 2; l++) {
                            if (k == 0)
                                jacobi[l][0] = coords[3 * (m + 1) + l] - coords[3 * m + l];
                            else if (k == dims[0] - 1)
                                jacobi[l][0] = coords[3 * m + l] - coords[3 * (m - 1) + l];
                            else
                                jacobi[l][0] = .5f * (coords[3 * (m + 1) + l] - coords[3 * (m - 1) + l]);
                            if (j == 0)
                                jacobi[l][1] = coords[3 * (m + len) + l] - coords[3 * m + l];
                            else if (j == dims[1] - 1)
                                jacobi[l][1] = coords[3 * m + l] - coords[3 * (m - len) + l];
                            else
                                jacobi[l][1] = .5f * (coords[3 * (m + len) + l] - coords[3 * (m - len) + l]);
                        }
                        float[] v = new float[2];
                        System.arraycopy(vec, 2 * m, v, 0, 2);
                        float[] w = null;
                        try {
                            w = MatrixMath.lsolve(jacobi, v);
                        } catch (IllegalArgumentException e) {

                        }
                        if (w != null)
                            System.arraycopy(w, 0, pullVec, 2 * m, 2);
                    }
            }
        }

    }

    public static float[] pullVectorField(int[] dims, float[] coords, float[] vec)
    {
        float[] pullVec = new float[vec.length];
        int nThreads = ConcurrencyUtils.getNumberOfThreads();
        Thread[] workThreads = new Thread[nThreads];
        for (int n = 0; n < nThreads; n++) {
            workThreads[n] = new Thread(new PullVectorField(n, nThreads, dims, coords, vec, pullVec));
            workThreads[n].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return pullVec;
    }

    private static class PullLargeVectorField implements Runnable
    {

        int iThread;
        int nThreads;
        int[] dims;
        FloatLargeArray coords;
        FloatLargeArray vec;
        FloatLargeArray pullVec;

        public PullLargeVectorField(int iThread, int nThreads,
                                    int[] dims, FloatLargeArray coords,
                                    FloatLargeArray vec, FloatLargeArray pullVec)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.dims = dims;
            this.coords = coords;
            this.vec = vec;
            this.pullVec = pullVec;
        }

        @Override
        public void run()
        {
            if (dims.length == 3) {
                float[][] jacobi = new float[3][3];
                int len = dims[0];
                int slc = dims[0] * dims[1];
                long m = (iThread * dims[2]) / nThreads;
                m *= slc;
                for (int i = (iThread * dims[2]) / nThreads;
                    i < ((iThread + 1) * dims[2]) / nThreads;
                    i++) {
                    for (int j = 0; j < dims[1]; j++)
                        for (int k = 0; k < dims[0]; k++, m++) {
                            for (int l = 0; l < 3; l++) {
                                if (k == 0)
                                    jacobi[l][0] = coords.get(3 * (m + 1) + l) - coords.get(3 * m + l);
                                else if (k == dims[0] - 1)
                                    jacobi[l][0] = coords.get(3 * m + l) - coords.get(3 * (m - 1) + l);
                                else
                                    jacobi[l][0] = .5f * (coords.get(3 * (m + 1) + l) - coords.get(3 * (m - 1) + l));
                                if (j == 0)
                                    jacobi[l][1] = coords.get(3 * (m + len) + l) - coords.get(3 * m + l);
                                else if (j == dims[1] - 1)
                                    jacobi[l][1] = coords.get(3 * m + l) - coords.get(3 * (m - len) + l);
                                else
                                    jacobi[l][1] = .5f * (coords.get(3 * (m + len) + l) - coords.get(3 * (m - len) + l));
                                if (i == 0)
                                    jacobi[l][2] = coords.get(3 * (m + slc) + l) - coords.get(3 * m + l);
                                else if (i == dims[2] - 1)
                                    jacobi[l][2] = coords.get(3 * m + l) - coords.get(3 * (m - slc) + l);
                                else
                                    jacobi[l][2] = .5f * (coords.get(3 * (m + slc) + l) - coords.get(3 * (m - slc) + l));
                            }
                            float[] v = new float[3];
                            for (int l = 0; l < 3; l++)
                                v[l] = vec.get(3 * m + l);
                            float[] w = null;
                            try {
                                w = lsolve(jacobi, v);
                            } catch (IllegalArgumentException e) {

                            }
                            if (w != null)
                                for (int l = 0; l < 3; l++)
                                    pullVec.set(3 * m + l, w[l]);
                        }

                }
            } else if (dims.length == 2) {
                float[][] jacobi = new float[2][2];
                int len = dims[0];
                int m = (iThread * dims[1]) / nThreads;
                m *= len;
                for (int j = (iThread * dims[1]) / nThreads;
                    j < ((iThread + 1) * dims[1]) / nThreads; j++)
                    for (int k = 0; k < dims[0]; k++, m++) {
                        for (int l = 0; l < 2; l++) {
                            if (k == 0)
                                jacobi[l][0] = coords.get(3 * (m + 1) + l) - coords.get(3 * m + l);
                            else if (k == dims[0] - 1)
                                jacobi[l][0] = coords.get(3 * m + l) - coords.get(3 * (m - 1) + l);
                            else
                                jacobi[l][0] = .5f * (coords.get(3 * (m + 1) + l) - coords.get(3 * (m - 1) + l));
                            if (j == 0)
                                jacobi[l][1] = coords.get(3 * (m + len) + l) - coords.get(3 * m + l);
                            else if (j == dims[1] - 1)
                                jacobi[l][1] = coords.get(3 * m + l) - coords.get(3 * (m - len) + l);
                            else
                                jacobi[l][1] = .5f * (coords.get(3 * (m + len) + l) - coords.get(3 * (m - len) + l));
                        }
                        float[] v = new float[2];
                        for (int l = 0; l < 2; l++)
                            v[l] = vec.get(2 * m + l);
                        float[] w = null;
                        try {
                            w = MatrixMath.lsolve(jacobi, v);
                        } catch (IllegalArgumentException e) {

                        }
                        if (w != null)
                            for (int l = 0; l < 2; l++)
                                pullVec.set(2 * m + l, w[l]);
                    }
            }
        }

    }

    public static FloatLargeArray pullVectorField(int[] dims, FloatLargeArray coords, FloatLargeArray vec)
    {
        FloatLargeArray pullVec = new FloatLargeArray(vec.length());
        int nThreads = ConcurrencyUtils.getNumberOfThreads();
        Thread[] workThreads = new Thread[nThreads];
        for (int n = 0; n < nThreads; n++) {
            workThreads[n] = new Thread(new PullLargeVectorField(n, nThreads, dims, coords, vec, pullVec));
            workThreads[n].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return pullVec;
    }

    public static float[] pushVectorField(int[] dims, float[] coords, float[] vec)
    {
        float[] pushVec = new float[vec.length];
        if (dims.length == 3) {
            float[][] jacobi = new float[3][3];
            float[] v = new float[3];
            int len = dims[0];
            int slc = dims[0] * dims[1];
            for (int i = 0, m = 0; i < dims[2]; i++)
                for (int j = 0; j < dims[1]; j++)
                    for (int k = 0; k < dims[0]; k++, m++) {
                        for (int l = 0; l < 3; l++) {
                            if (k == 0)
                                jacobi[l][0] = coords[3 * (m + 1) + l] - coords[3 * m + l];
                            else if (k == dims[0] - 1)
                                jacobi[l][0] = coords[3 * m + l] - coords[3 * (m - 1) + l];
                            else
                                jacobi[l][0] = .5f * (coords[3 * (m + 1) + l] - coords[3 * (m - 1) + l]);
                            if (j == 0)
                                jacobi[l][1] = coords[3 * (m + len) + l] - coords[3 * m + l];
                            else if (j == dims[1] - 1)
                                jacobi[l][1] = coords[3 * m + l] - coords[3 * (m - len) + l];
                            else
                                jacobi[1][l] = .5f * (coords[3 * (m + len) + l] - coords[3 * (m - len) + l]);
                            if (i == 0)
                                jacobi[l][2] = coords[3 * (m + slc) + l] - coords[3 * m + l];
                            else if (i == dims[2] - 1)
                                jacobi[l][2] = coords[3 * m + l] - coords[3 * (m - slc) + l];
                            else
                                jacobi[l][2] = .5f * (coords[3 * (m + slc) + l] - coords[3 * (m - slc) + l]);
                        }
                        for (int l = 0; l < 3; l++) {
                            v[l] = 0;
                            for (int n = 0; n < 3; n++)
                                v[l] += jacobi[l][n] * vec[3 * m + n];
                        }
                        System.arraycopy(v, 0, pushVec, 3 * m, 3);
                    }
        } else if (dims.length == 2) {
            float[][] jacobi = new float[2][2];
            float[] v = new float[2];
            int len = dims[0];
            for (int j = 0, m = 0; j < dims[1]; j++)
                for (int k = 0; k < dims[0]; k++, m++) {
                    for (int l = 0; l < 2; l++) {
                        if (k == 0)
                            jacobi[l][0] = coords[2 * (m + 1) + l] - coords[2 * m + l];
                        else if (k == dims[0] - 1)
                            jacobi[l][0] = coords[2 * m + l] - coords[2 * (m - 1) + l];
                        else
                            jacobi[l][0] = .5f * (coords[2 * (m + 1) + l] - coords[2 * (m - 1) + l]);
                        if (j == 0)
                            jacobi[l][1] = coords[2 * (m + len) + l] - coords[2 * m + l];
                        else if (j == dims[1] - 1)
                            jacobi[l][1] = coords[2 * m + l] - coords[2 * (m - len) + l];
                        else
                            jacobi[l][1] = .5f * (coords[2 * (m + len) + l] - coords[2 * (m - len) + l]);
                    }
                    for (int l = 0; l < 2; l++) {
                        v[l] = 0;
                        for (int n = 0; n < 2; n++)
                            v[l] += jacobi[l][n] * vec[2 * m + n];
                    }
                    System.arraycopy(v, 0, pushVec, 2 * m, 2);
                }
        }
        return pushVec;
    }

    public static void main(String[] args)
    {
        int n = 6;
        float[][] a = new float[n][n];
        float[][] A = new float[n][n];
        float[] d = new float[n];
        float[][] v = new float[n][n];
        for (int i = 0; i < n; i++)
            for (int j = i; j < n; j++)
                A[i][j] = a[i][j] = A[j][i] = a[j][i] = (float) Math.random();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                System.out.printf("%10.4f ", a[i][j]);
            System.out.println("");
        }
        System.out.println("" + jacobi(A, d, v, 1000));
        for (int i = 0; i < n; i++) {
            System.out.printf("%10.4f   ", d[i]);
            for (int j = 0; j < n; j++)
                System.out.printf("%10.4f ", v[i][j]);
            System.out.println("");
        }
        for (float[] v1 : v) {
            for (float[] v2 : v) {
                float p = 0;
                for (int i = 0; i < n; i++)
                    p += v1[i] * v2[i];
                System.out.printf("%10.4f", p);
            }
            System.out.println("");
        }
        float[] w = new float[n];
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v.length; j++) {
                w[j] = 0;
                for (int k = 0; k < w.length; k++)
                    w[j] += a[j][k] * v[i][k];
                System.out.printf("%10.4f ", w[j]);
            }
            for (int j = 0; j < v.length; j++)
                System.out.printf("%10.4f ", d[i] * v[i][j]);
            System.out.println("");
        }
    }

    private MatrixMath()
    {
    }

}
