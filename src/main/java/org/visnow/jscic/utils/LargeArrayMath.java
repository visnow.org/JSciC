/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.dataarrays.DataObjectInterface;
import org.visnow.jlargearrays.ComplexDoubleLargeArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.ObjectLargeArray;

/**
 * Various numerical operations on large arrays.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class LargeArrayMath
{

    /**
     * Computes norms of each vector element of the input array.
     *
     * @param in     input array
     * @param veclen vector length
     *
     * @return norms of each vector element of the input array
     */
    public static FloatLargeArray vectorNorms(final LargeArray in, final int veclen)
    {
        switch (in.getType()) {
            case LOGIC:
            case BYTE:
            case UNSIGNED_BYTE:
            case SHORT:
            case INT:
            case LONG:
            case FLOAT:
            case DOUBLE:
            case STRING:
                final long n = in.length() / veclen;
                final FloatLargeArray out = new FloatLargeArray(n);
                int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
                long k = n / nthreads;
                Future<?>[] futures = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = j * k;
                    final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
                    futures[j] = ConcurrencyUtils.submit(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if (veclen == 1) {
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    out.setDouble(i, abs(in.getDouble(i)));
                                }
                            } else {
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    double d = 0;
                                    for (long j = 0; j < veclen; j++) {
                                        long l = i * veclen + j;
                                        double tmp = in.getDouble(l);
                                        d += tmp * tmp;
                                    }
                                    out.setFloat(i, (float) sqrt(d));
                                }
                            }
                        }
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(futures);
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException(ex);
                }
                return out;
            case COMPLEX_FLOAT:
                return vectorNorms((ComplexFloatLargeArray) in, veclen);
            case COMPLEX_DOUBLE:
                return vectorNorms((ComplexDoubleLargeArray) in, veclen);
            case OBJECT:
                return vectorNorms((ObjectLargeArray) in, veclen);
            default:
                throw new IllegalArgumentException("Unsuported array type.");
        }

    }

    /**
     * Computes norms of each vector element of the input array.
     *
     * @param in     input array
     * @param veclen vector length
     *
     * @return norms of each vector element of the input array
     */
    private static FloatLargeArray vectorNorms(final ComplexFloatLargeArray in, final int veclen)
    {
        final long n = in.length() / veclen;
        final FloatLargeArray out = new FloatLargeArray(n);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (veclen == 1) {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            float[] elem = in.get(i);
                            out.setDouble(i, sqrt(elem[0] * elem[0] + elem[1] * elem[1]));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double d = 0;
                            for (long j = 0; j < veclen; j++) {
                                long l = i * veclen + j;
                                float[] elem = in.get(l);
                                d += (elem[0] * elem[0] + elem[1] * elem[1]);
                            }
                            out.setFloat(i, (float) sqrt(d));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Computes norms of each vector element of the input array.
     *
     * @param in     input array
     * @param veclen vector length
     *
     * @return norms of each vector element of the input array
     */
    private static FloatLargeArray vectorNorms(final ComplexDoubleLargeArray in, final int veclen)
    {
        final long n = in.length() / veclen;
        final FloatLargeArray out = new FloatLargeArray(n);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (veclen == 1) {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double[] elem = in.get(i);
                            out.setDouble(i, sqrt(elem[0] * elem[0] + elem[1] * elem[1]));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double d = 0;
                            for (long j = 0; j < veclen; j++) {
                                long l = i * veclen + j;
                                double[] elem = in.get(l);
                                d += (elem[0] * elem[0] + elem[1] * elem[1]);
                            }
                            out.setDouble(i, sqrt(d));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Computes norms of each vector element of the input array.
     *
     * @param in     input array
     * @param veclen vector length
     *
     * @return norms of each vector element of the input array
     */
    private static FloatLargeArray vectorNorms(final ObjectLargeArray in, final int veclen)
    {
        final long n = in.length() / veclen;
        final FloatLargeArray out = new FloatLargeArray(n);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (veclen == 1) {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double tmp = ((DataObjectInterface) in.get(i)).toFloat();
                            out.setDouble(i, abs(tmp));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double d = 0;
                            for (long j = 0; j < veclen; j++) {
                                long l = i * veclen + j;
                                double tmp = ((DataObjectInterface) in.get(l)).toFloat();
                                d += tmp * tmp;
                            }
                            out.setFloat(i, (float) sqrt(d));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }
}
