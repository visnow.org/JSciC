/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.ArrayList;
import java.util.Arrays;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 * Field utilities.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FieldUtils
{

    /**
     * Converts RegularField to IrregularField.
     *
     * @param inField regular field
     *
     * @return irregular field
     */
    public static final IrregularField convertToIrregular(RegularField inField)
    {
        int[] dims = inField.getDims();
        int[] offsets = null;
        switch (dims.length) {
            case 1:
                offsets = new int[]{0, 1};
                break;
            case 2:
                offsets = new int[]{0, 1, dims[0] + 1, dims[0]};
                break;
            case 3:

                offsets = new int[]{0, 1, dims[0] + 1, dims[0],
                                    dims[0] * dims[1], dims[0] * dims[1] + 1, dims[0] * dims[1] + dims[0] + 1, dims[0] * dims[1] + dims[0]};
                break;
        }

        int nNodes = (int) inField.getNNodes();
        int nOutNodes;
        FloatLargeArray coords;
        boolean isMask = inField.hasMask();
        int[] indices = null;

        CellSet cs = new CellSet("cells");
        if (isMask) {
            LogicLargeArray mask = inField.getCurrentMask();
            indices = new int[nNodes];
            nOutNodes = 0;
            Arrays.fill(indices, -1);
            for (int i = 0; i < nNodes; i++)
                if (mask.getBoolean(i)) {
                    indices[i] = nOutNodes;
                    nOutNodes += 1;
                }
            coords = new FloatLargeArray(3 * (long) nOutNodes, false);
            if (inField.getCurrentCoords() != null) {
                float[] inCoords = inField.getCurrentCoords().getData();
                for (int i = 0, j = 0; i < nNodes; i++)
                    if (mask.getBoolean(i)) {
                        LargeArrayUtils.arraycopy(inCoords, 3 * i, coords, 3 * j, 3);
                        j += 1;
                    }
            } else {
                float[][] affine = inField.getAffine();
                switch (dims.length) {
                    case 1:
                        for (long k = 0, l = 0, ii = 0; k < dims[0]; k++, ii++)
                            if (mask.getBoolean(ii))
                                for (int m = 0; m < 3; m++, l++)
                                    coords.setFloat(l, affine[3][m] + k * affine[0][m]);
                        break;
                    case 2:
                        for (long j = 0, l = 0, ii = 0; j < dims[1]; j++)
                            for (int k = 0; k < dims[0]; k++, ii++)
                                if (mask.getBoolean(ii))
                                    for (int m = 0; m < 3; m++, l++)
                                        coords.setFloat(l, affine[3][m] + j * affine[1][m] + k * affine[0][m]);
                        break;
                    case 3:
                        for (long i = 0, l = 0, ii = 0; i < dims[2]; i++)
                            for (int j = 0; j < dims[1]; j++)
                                for (int k = 0; k < dims[0]; k++, ii++)
                                    if (mask.getBoolean(ii))
                                        for (int m = 0; m < 3; m++, l++)
                                            coords.setFloat(l, affine[3][m] + i * affine[2][m] + j * affine[1][m] + k * affine[0][m]);
                        break;
                }
            }

            int[] outNodes;
            int[] tmpOutNodes;
            int nOutCells = 0;
            switch (dims.length) {
                case 1:
                    tmpOutNodes = new int[2 * (dims[0] - 1)];
                    for (int k = 0, l = 0; k < dims[0] - 1; k++)
                        if (indices[k] >= 0 && indices[k + 1] >= 0) {
                            tmpOutNodes[l] = indices[k];
                            tmpOutNodes[l + 1] = indices[k + 1];
                            l += 2;
                            nOutCells += 1;
                        }
                    outNodes = new int[2 * nOutCells];
                    System.arraycopy(tmpOutNodes, 0, outNodes, 0, outNodes.length);
                    cs.addCells(new CellArray(CellType.SEGMENT, outNodes, null, null));
                    break;
                case 2:
                    tmpOutNodes = new int[4 * (dims[1] - 1) * (dims[0] - 1)];
                    for (int j = 0, l = 0; j < dims[1] - 1; j++)
                        for (int k = 0, m = j * dims[0]; k < dims[0] - 1; k++, m++)
                            if (indices[m] >= 0 && indices[m + 1] >= 0 &&
                                indices[m + dims[0]] >= 0 && indices[m + dims[0] + 1] >= 0) {
                                tmpOutNodes[l] = indices[m];
                                tmpOutNodes[l + 1] = indices[m + 1];
                                tmpOutNodes[l + 2] = indices[m + 1 + dims[0]];
                                tmpOutNodes[l + 3] = indices[m + dims[0]];
                                l += 4;
                                nOutCells += 1;
                            }
                    outNodes = new int[4 * nOutCells];
                    System.arraycopy(tmpOutNodes, 0, outNodes, 0, outNodes.length);
                    cs.addCells(new CellArray(CellType.QUAD, outNodes, null, null));
                    break;
                case 3:
                    tmpOutNodes = new int[8 * (dims[2] - 1) * (dims[1] - 1) * (dims[0] - 1)];
                    for (int i = 0, l = 0; i < dims[2] - 1; i++)
                        for (int j = 0; j < dims[1] - 1; j++)
                            for (int k = 0, m = (i * dims[1] + j) * dims[0]; k < dims[0] - 1; k++, m++)
                                if (indices[m] >= 0 && indices[m + 1] >= 0 &&
                                    indices[m + dims[0]] >= 0 && indices[m + dims[0] + 1] >= 0 &&
                                    indices[m + dims[0] * dims[1]] >= 0 &&
                                    indices[m + dims[0] * dims[1] + 1] >= 0 &&
                                    indices[m + dims[0] * dims[1] + dims[0]] >= 0 &&
                                    indices[m + dims[0] * dims[1] + dims[0] + 1] >= 0) {
                                    tmpOutNodes[l] = indices[m];
                                    tmpOutNodes[l + 1] = indices[m + 1];
                                    tmpOutNodes[l + 2] = indices[m + 1 + dims[0]];
                                    tmpOutNodes[l + 3] = indices[m + dims[0]];
                                    tmpOutNodes[l + 4] = indices[m + dims[0] * dims[1]];
                                    tmpOutNodes[l + 5] = indices[m + 1 + dims[0] * dims[1]];
                                    tmpOutNodes[l + 6] = indices[m + 1 + dims[0] + dims[0] * dims[1]];
                                    tmpOutNodes[l + 7] = indices[m + dims[0] + dims[0] * dims[1]];
                                    l += 8;
                                    nOutCells += 1;
                                }
                    outNodes = new int[8 * nOutCells];
                    System.arraycopy(tmpOutNodes, 0, outNodes, 0, outNodes.length);
                    cs.addCells(new CellArray(CellType.HEXAHEDRON, outNodes, null, null));
                    break;
            }
        } else {
            nOutNodes = nNodes;
            if (inField.getCurrentCoords() != null)
                coords = inField.getCurrentCoords();
            else {
                coords = new FloatLargeArray(3 * (long) nOutNodes, false);
                float[][] affine = inField.getAffine();
                switch (dims.length) {
                    case 1:
                        for (long k = 0, l = 0; k < dims[0]; k++)
                            for (int m = 0; m < 3; m++, l++)
                                coords.setFloat(l, affine[3][m] + k * affine[0][m]);
                        break;
                    case 2:
                        for (long j = 0, l = 0; j < dims[1]; j++)
                            for (int k = 0; k < dims[0]; k++)
                                for (int m = 0; m < 3; m++, l++)
                                    coords.setFloat(l, affine[3][m] + j * affine[1][m] + k * affine[0][m]);
                        break;
                    case 3:
                        for (long i = 0, l = 0; i < dims[2]; i++)
                            for (int j = 0; j < dims[1]; j++)
                                for (int k = 0; k < dims[0]; k++)
                                    for (int m = 0; m < 3; m++, l++)
                                        coords.setFloat(l, affine[3][m] + i * affine[2][m] + j * affine[1][m] + k * affine[0][m]);
                        break;
                }
            }

            int[] outNodes;
            byte[] orientations;
            switch (dims.length) {
                case 1:
                    outNodes = new int[2 * (dims[0] - 1)];
                    orientations = new byte[dims[0] - 1];
                    for (int k = 0, l = 0, m = 0; k < dims[0] - 1; k++, m++) {
                        outNodes[l] = k;
                        outNodes[l + 1] = outNodes[l] + 1;
                        l += 2;
                        orientations[m] = 1;
                    }
                    cs.addCells(new CellArray(CellType.SEGMENT, outNodes, orientations, null));
                    break;
                case 2:
                    outNodes = new int[4 * (dims[1] - 1) * (dims[0] - 1)];
                    orientations = new byte[(dims[1] - 1) * (dims[0] - 1)];
                    for (int j = 0, l = 0, m = 0; j < dims[1] - 1; j++)
                        for (int k = 0; k < dims[0] - 1; k++, m++) {
                            for (int p = 0, q = j * dims[0] + k; p < 4; p++, l++)
                                outNodes[l] = q + offsets[p];
                            orientations[m] = 1;
                        }
                    cs.addCells(new CellArray(CellType.QUAD, outNodes, orientations, null));
                    break;
                case 3:
                    outNodes = new int[8 * (dims[2] - 1) * (dims[1] - 1) * (dims[0] - 1)];
                    orientations = new byte[(dims[2] - 1) * (dims[1] - 1) * (dims[0] - 1)];
                    for (int i = 0, l = 0, m = 0; i < dims[2] - 1; i++)
                        for (int j = 0; j < dims[1] - 1; j++)
                            for (int k = 0; k < dims[0] - 1; k++, m++) {
                                for (int p = 0, q = (i * dims[1] + j) * dims[0] + k; p < 8; p++, l++)
                                    outNodes[l] = q + offsets[p];
                                orientations[m] = 1;
                            }
                    cs.addCells(new CellArray(CellType.HEXAHEDRON, outNodes, orientations, null));
                    break;
            }
        }
        IrregularField outIrregularField = new IrregularField(nOutNodes);
        outIrregularField.setCurrentCoords(coords);
        cs.generateDisplayData(coords);
        outIrregularField.addCellSet(cs);
        if (isMask) {
            for (DataArray inDa : inField.getComponents()) {
                int vlen = inDa.getVectorLength();
                LargeArray outB = LargeArrayUtils.create(inDa.getRawArray().getType(), vlen * nOutNodes);
                for (int i = 0, j = 0; i < nNodes; i++)
                    if (indices[i] >= 0) {
                        LargeArrayUtils.arraycopy(inDa.getRawArray(), vlen * i, outB, vlen * j, vlen);
                        j += 1;
                    }
                DataArray da = DataArray.create(outB, vlen, inDa.getName()).unit(inDa.getUnit()).userData(inDa.getUserData());
                da.setPreferredRanges(inDa.getPreferredMinValue(), inDa.getPreferredMaxValue(), inDa.getPreferredPhysMinValue(), inDa.getPreferredPhysMaxValue());
                outIrregularField.addComponent(da);
            }
        } else
            for (DataArray inDa : inField.getComponents())
                outIrregularField.addComponent(inDa.cloneShallow());
        return outIrregularField;
    }

    /**
     * Converts the cell data stored in a given IrregularField to a node data.
     *
     * @param field field to be modified
     * @param drop  if true, then the cell data are removed
     */
    public static void convertCellDataToNodeData(IrregularField field, boolean drop)
    {
        int nNodes = (int) field.getNNodes();
        int[] counts = new int[nNodes];
        int csId = 0;
        for (CellSet cs : field.getCellSets()) {
            ArrayList<DataArray> cellData = cs.getComponents();
            for (DataArray inDA : cellData) {
                float[] inD = inDA.getRawFloatArray().getData();
                int vlen = inDA.getVectorLength();
                float[] outD = new float[nNodes * vlen];
                java.util.Arrays.fill(counts, 0);
                java.util.Arrays.fill(outD, 0.f);
                for (int i = 0; i < Cell.getNProperCellTypes(); i++) {
                    int nv = CellType.getType(i).getNVertices();
                    CellArray ca = cs.getCellArray(CellType.getType(i));
                    if (ca == null)
                        continue;
                    int[] nodes = ca.getNodes();
                    int[] indices = ca.getDataIndices();
                    for (int j = 0; j < ca.getNCells(); j++) {
                        int iin = vlen * indices[j];
                        for (int l = nv * j; l < nv * (j + 1); l++) {
                            int iout = vlen * nodes[l];
                            counts[nodes[l]] += 1;
                            for (int k = 0; k < vlen; k++) {
                                outD[iout + k] += inD[iin + k];
                            }
                        }
                    }
                }
                for (int i = 0; i < nNodes; i++) {
                    if (counts[i] == 0)
                        continue;
                    float f = 1.f / counts[i];
                    for (int j = i * vlen; j < (i + 1) * vlen; j++)
                        outD[j] *= f;
                }
                String name = inDA.getName();
                if (field.getComponent(name) != null)
                    name += csId;
                field.addComponent(DataArray.create(outD, vlen, name).unit(inDA.getUnit()).userData(inDA.getUserData()));
            }
            if (drop) {
                cs.removeComponents();
                CellSet outCS = new CellSet(cs.getName());
                outCS.setCellArrays(cs.getCellArrays());
                outCS.setBoundaryCellArrays(cs.getBoundaryCellArrays());
                field.addCellSet(outCS);
            }
            csId += 1;
        }
    }

    /**
     * Converts the node data stored in a given IrregularField to a cell data.
     *
     * @param field field to be modified
     */
    public static void convertNodeDataToCellData(IrregularField field)
    {
        int nComp = field.getNComponents();
        DataArray[] interpolatedComponents = new DataArray[nComp];
        FloatLargeArray[] inData = new FloatLargeArray[nComp];
        int[] vLens = new int[nComp];
        int nInterpolated = 0;
        for (int i = 0; i < field.getNComponents(); i++) {
            DataArray component = field.getComponent(i);
            if (component.isNumeric()) {
                interpolatedComponents[nInterpolated] = component;
                vLens[nInterpolated] = component.getVectorLength();
                if (component.getType() == DataArrayType.FIELD_DATA_FLOAT)
                    inData[nInterpolated] = (FloatLargeArray) component.getRawArray();
                else
                    inData[nInterpolated] = component.getRawFloatArray();
                nInterpolated += 1;
            }
        }
        for (CellSet cs : field.getCellSets()) {

            int totalCells = 0;
            for (CellArray ca : cs.getCellArrays())
                if (ca != null)
                    totalCells += ca.getNCells();
            if (totalCells > 0) {
                float[][] outData = new float[nInterpolated][];
                for (int i = 0; i < nInterpolated; i++) {
                    outData[i] = new float[vLens[i] * totalCells];
                    Arrays.fill(outData[i], 0);
                }
                int k = 0, tc = 0;
                for (CellArray ca : cs.getCellArrays()) {
                    if (ca == null)
                        continue;
                    int[] indices = new int[ca.getNCells()];
                    for (int i = 0; i < indices.length; i++, tc++)
                        indices[i] = tc;
                    int[] nodes = ca.getNodes();
                    int nCells = ca.getNCells();
                    int nCellNodes = ca.getNCellNodes();
                    for (int i = 0; i < nCells; i++, k++)
                        for (int j = 0; j < nInterpolated; j++) {
                            int vlen = vLens[j];
                            for (int l = 0; l < vlen; l++) {
                                for (int m = 0; m < nCellNodes; m++) {
                                    outData[j][vlen * k + l] += inData[j].get(vlen * nodes[nCellNodes * i + m] + l);
                                }
                                outData[j][vlen * k + l] /= nCellNodes;
                            }
                        }
                    ca.setDataIndices(indices);
                }
                for (int i = 0; i < nInterpolated; i++) {
                    DataArray inDA = interpolatedComponents[i];
                    cs.addComponent(DataArray.create(outData[i], vLens[i], inDA.getName()).unit(inDA.getUnit()).preferredRanges(inDA.getPreferredMinValue(), inDA.getPreferredMaxValue(), inDA.getPreferredPhysMinValue(), inDA.getPreferredPhysMaxValue()).userData(inDA.getUserData()));
                }
            }
        }
    }

    /**
     * Checks if specified array holds valid dimensions of RegularField.
     *
     * @param dims dimensions
     *
     * @throws IllegalArgumentException if specified array does not hold valid dimensions
     */
    public static void checkDimensions(long[] dims)
    {
        if (dims == null) {
            throw new IllegalArgumentException("dims cannot be null");
        }
        if (dims.length < 1 || dims.length > 3) {
            throw new IllegalArgumentException("Only 1D, 2D and 3D regular fields are supported");
        }

        for (int i = 0; i < dims.length; i++) {
            if (dims[i] < 1) {
                throw new IllegalArgumentException("All values in array dims must be greater than 0");
            }
        }
    }
    
     /**
     * Returns true if at least one dimension is equal to 1, false otherwise.
     *
     * @param dims dimensions
     * @return true if at least one dimension is equal to 1, false otherwise
     *
     * @throws IllegalArgumentException if specified array does not hold valid dimensions
     */
    public static boolean atLeastOneDimensionIsOne(long[] dims)
    {
        checkDimensions(dims);
        for (int i = 0; i < dims.length; i++) {
            if (dims[i] == 1) {
                return true;
            }
        }
        return false;
    }

    private FieldUtils()
    {
    }

}
