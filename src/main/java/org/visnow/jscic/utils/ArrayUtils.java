/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.Arrays;
import java.util.List;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * Array utilities.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class ArrayUtils
{

    private ArrayUtils()
    {

    }

    /**
     * Returns the deep copy of a 2D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static byte[][] cloneDeep(byte[][] a)
    {
        if (a == null || a[0] == null) return null;
        byte[][] clone = new byte[a.length][a[0].length];
        for (int i = 0; i < a.length; i++) {
            clone[i] = a[i] != null ? a[i].clone() : null;
        }
        return clone;
    }

    /**
     * Returns the deep copy of a 2D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static short[][] cloneDeep(short[][] a)
    {
        if (a == null || a[0] == null) return null;
        short[][] clone = new short[a.length][a[0].length];
        for (int i = 0; i < a.length; i++) {
            clone[i] = a[i] != null ? a[i].clone() : null;
        }
        return clone;
    }

    /**
     * Returns the deep copy of a 2D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static int[][] cloneDeep(int[][] a)
    {
        if (a == null || a[0] == null) return null;
        int[][] clone = new int[a.length][a[0].length];
        for (int i = 0; i < a.length; i++) {
            clone[i] = a[i] != null ? a[i].clone() : null;
        }
        return clone;
    }

    /**
     * Returns the deep copy of a 2D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static long[][] cloneDeep(long[][] a)
    {
        if (a == null || a[0] == null) return null;
        long[][] clone = new long[a.length][a[0].length];
        for (int i = 0; i < a.length; i++) {
            clone[i] = a[i] != null ? a[i].clone() : null;
        }
        return clone;
    }

    /**
     * Returns the deep copy of a 2D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static float[][] cloneDeep(float[][] a)
    {
        if (a == null || a[0] == null) return null;
        float[][] clone = new float[a.length][a[0].length];
        for (int i = 0; i < a.length; i++) {
            clone[i] = a[i] != null ? a[i].clone() : null;
        }
        return clone;
    }

    /**
     * Returns the deep copy of a 2D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static double[][] cloneDeep(double[][] a)
    {
        if (a == null || a[0] == null) return null;
        double[][] clone = new double[a.length][a[0].length];
        for (int i = 0; i < a.length; i++) {
            clone[i] = a[i] != null ? a[i].clone() : null;
        }
        return clone;
    }

    /**
     * Returns the deep copy of a 3D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static byte[][][] cloneDeep(byte[][][] a)
    {
        if (a == null || a[0] == null || a[0][0] == null) return null;
        byte[][][] clone = new byte[a.length][a[0].length][a[0][0].length];
        for (int i = 0; i < a.length; i++) {
            if (a[i] == null) {
                clone[i] = null;
            } else {
                for (int j = 0; j < a[0].length; j++) {
                    clone[i][j] = a[i][j] != null ? a[i][j].clone() : null;
                }
            }
        }
        return clone;
    }

    /**
     * Returns the deep copy of a 3D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static short[][][] cloneDeep(short[][][] a)
    {
        if (a == null || a[0] == null || a[0][0] == null) return null;
        short[][][] clone = new short[a.length][a[0].length][a[0][0].length];
        for (int i = 0; i < a.length; i++) {
            if (a[i] == null) {
                clone[i] = null;
            } else {
                for (int j = 0; j < a[0].length; j++) {
                    clone[i][j] = a[i][j] != null ? a[i][j].clone() : null;
                }
            }
        }
        return clone;
    }

    /**
     * Returns a deep copy of a 3D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static int[][][] cloneDeep(int[][][] a)
    {
        if (a == null || a[0] == null || a[0][0] == null) return null;
        int[][][] clone = new int[a.length][a[0].length][a[0][0].length];
        for (int i = 0; i < a.length; i++) {
            if (a[i] == null) {
                clone[i] = null;
            } else {
                for (int j = 0; j < a[0].length; j++) {
                    clone[i][j] = a[i][j] != null ? a[i][j].clone() : null;
                }
            }
        }
        return clone;
    }

    /**
     * Returns the deep copy of a 3D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static long[][][] cloneDeep(long[][][] a)
    {
        if (a == null || a[0] == null || a[0][0] == null) return null;
        long[][][] clone = new long[a.length][a[0].length][a[0][0].length];
        for (int i = 0; i < a.length; i++) {
            if (a[i] == null) {
                clone[i] = null;
            } else {
                for (int j = 0; j < a[0].length; j++) {
                    clone[i][j] = a[i][j] != null ? a[i][j].clone() : null;
                }
            }
        }
        return clone;
    }

    /**
     * Returns the deep copy of a 3D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static float[][][] cloneDeep(float[][][] a)
    {
        if (a == null || a[0] == null || a[0][0] == null) return null;
        float[][][] clone = new float[a.length][a[0].length][a[0][0].length];
        for (int i = 0; i < a.length; i++) {
            if (a[i] == null) {
                clone[i] = null;
            } else {
                for (int j = 0; j < a[0].length; j++) {
                    clone[i][j] = a[i][j] != null ? a[i][j].clone() : null;
                }
            }
        }
        return clone;
    }

    /**
     * Returns the deep copy of a 3D array.
     *
     * @param a array to be cloned
     *
     * @return deep copy of an array
     */
    public static double[][][] cloneDeep(double[][][] a)
    {
        if (a == null || a[0] == null || a[0][0] == null) return null;
        double[][][] clone = new double[a.length][a[0].length][a[0][0].length];
        for (int i = 0; i < a.length; i++) {
            if (a[i] == null) {
                clone[i] = null;
            } else {
                for (int j = 0; j < a[0].length; j++) {
                    clone[i][j] = a[i][j] != null ? a[i][j].clone() : null;
                }
            }
        }
        return clone;
    }

    /**
     * Reverses byte array in tuples (order within tuple stays unchanged).
     *
     * @param table       array to be reversed
     * @param tupleLength length of the tuple
     *
     * @return array with reversed elements
     */
    public static byte[] reverse(byte[] table, int tupleLength)
    {
        byte[] reversed = new byte[table.length];
        int pos = table.length - tupleLength;
        for (int i = 0; i < table.length; i += tupleLength) {
            for (int j = 0; j < tupleLength; j++)
                reversed[i + j] = table[pos + j];
            pos -= tupleLength;
        }
        return reversed;
    }

    /**
     * Finds first occurrence of objectToFind and returns its index or -1 if not found.
     *
     * @param array        input array
     * @param objectToFind object to be found
     *
     * @return index of the first occurrence of objectToFind in array, -1 if the object is not found
     */
    public static int indexOf(Object[] array, Object objectToFind)
    {
        if (objectToFind == null) {
            for (int i = 0; i < array.length; i++) if (array[i] == null) return i;
        } else
            for (int i = 0; i < array.length; i++) if (objectToFind.equals(array[i])) return i;
        return -1;
    }

    /**
     * Return the nearest index of the valueToFind in orderedValues list.
     *
     * @param orderedValues sorted array
     * @param valueToFind   value to be found
     *
     * @return nearest index of the valueToFind in orderedValues list
     */
    public static int findNearestIndex(int[] orderedValues, int valueToFind)
    {
        if (orderedValues.length == 0) throw new IllegalArgumentException("Value list may not be empty");

        int position = Arrays.binarySearch(orderedValues, valueToFind);
        if (position < 0) {
            position = -1 - position; //insertion position
            if (position > 0)
                if (position == orderedValues.length) position--;
                else {
                    int distanceToLower = abs(orderedValues[position - 1] - valueToFind);
                    int distanceToHigher = abs(orderedValues[position] - valueToFind);
                    if (distanceToLower <= distanceToHigher) position--;
                }
        }
        return position;
    }

    /**
     * Returns the string representation of each object in an array.
     *
     * @param object input array
     *
     * @return string representation of each object in the input array
     */
    public static String[] toString(Object[] object)
    {
        String[] strings = new String[object.length];
        for (int i = 0; i < object.length; i++) strings[i] = object[i].toString();
        return strings;
    }

    /**
     * Return true if the list is sorted, false otherwise.
     *
     * @param <T>  type
     * @param list list
     *
     * @return true if the list is sorted, false otherwise
     */
    public static <T extends Comparable<? super T>> boolean isSorted(List<T> list)
    {
        for (int i = 0; i < list.size() - 1; i++) {
            if (list.get(i).compareTo(list.get(i + 1)) > 0) {
                return false;
            }
        }
        return true;
    }
}
