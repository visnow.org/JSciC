/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import static org.visnow.jscic.utils.InfinityAction.INF_AS_0;
import static org.visnow.jscic.utils.NaNAction.NAN_AS_0;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.ConcurrencyUtils;

/**
 * Utilities for processing NaNs and Infinities.
 *
 * @author Piotr Wendykier (piotrw@icm.edu.pl)
 */
public class FloatingPointUtils
{

    /**
     * Maximal floating point number of a single precision.
     */
    public static final float MAX_NUMBER_FLOAT = Float.MAX_VALUE / 10;

    /**
     * Minimal floating point number of a single precision.
     */
    public static final float MIN_NUMBER_FLOAT = -MAX_NUMBER_FLOAT;

    /**
     * Maximal floating point number of a double precision.
     */
    public static final double MAX_NUMBER_DOUBLE = Float.MAX_VALUE / 10;

    /**
     * Minimal floating point number of a double precision.
     */
    public static final double MIN_NUMBER_DOUBLE = -MAX_NUMBER_DOUBLE;

    /**
     * The default action on NaN.
     */
    public static NaNAction defaultNanAction = NAN_AS_0;

    /**
     * The default action on Infinity.
     */
    public static InfinityAction defaultInfinityAction = INF_AS_0;

    /**
     * Replaces Float.NEGATIVE_INFINITY by minVal and Float.POSITIVE_INFINITY by maxVal.
     *
     * @param data   data to be processed
     * @param minVal minimal value
     * @param maxVal maximal value
     */
    private static void processInfAsExtremeDataValue(final FloatLargeArray data, final float minVal, final float maxVal)
    {
        long n = data.length();
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {

                    for (long i = firstIdx; i < lastIdx; i++) {
                        float e = data.getFloat(i);
                        if (e == Float.POSITIVE_INFINITY) {
                            data.setFloat(i, maxVal);
                        }
                        if (e == Float.NEGATIVE_INFINITY) {
                            data.setFloat(i, minVal);
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * Replaces Double.NEGATIVE_INFINITY by minVal and Double.POSITIVE_INFINITY by maxVal.
     *
     * @param data   data to be processed
     * @param minVal minimal value
     * @param maxVal maximal value
     */
    private static void processInfAsExtremeDataValue(final DoubleLargeArray data, final double minVal, final double maxVal)
    {
        long n = data.length();
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {

                    for (long i = firstIdx; i < lastIdx; i++) {
                        Double e = data.getDouble(i);
                        if (e == Double.POSITIVE_INFINITY) {
                            data.setDouble(i, maxVal);
                        }
                        if (e == Double.NEGATIVE_INFINITY) {
                            data.setDouble(i, minVal);
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * Replaces Float.NAN by val.
     *
     * @param data data to be processed
     * @param val  value
     */
    private static void processNaNAsExtremeDataValue(final FloatLargeArray data, final float val)
    {
        long n = data.length();
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {

                    for (long i = firstIdx; i < lastIdx; i++) {
                        float e = data.getFloat(i);
                        if (Float.isNaN(e)) {
                            data.setFloat(i, val);
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * Replaces Double.NAN by val.
     *
     * @param data data to be processed
     * @param val  value
     */
    private static void processNaNAsExtremeDataValue(final DoubleLargeArray data, final double val)
    {
        long n = data.length();
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {

                    for (long i = firstIdx; i < lastIdx; i++) {
                        double e = data.getDouble(i);
                        if (Double.isNaN(e)) {
                            data.setDouble(i, val);
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * Replaces values greater than MAX_NUMBER_FLOAT by MAX_NUMBER_FLOAT and values smaller than MIN_NUMBER_FLOAT by MIN_NUMBER_FLOAT.
     *
     * @param data        data to be processed
     * @param minNumFloat minimal value
     * @param maxNumFloat maximal value
     */
    private static void processLargeValues(final FloatLargeArray data, final float minNumFloat, final float maxNumFloat)
    {
        long n = data.length();
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {

                    for (long i = firstIdx; i < lastIdx; i++) {
                        float e = data.getFloat(i);
                        if (e < minNumFloat) {
                            data.setFloat(i, minNumFloat);
                        } else if (e > maxNumFloat) {
                            data.setFloat(i, maxNumFloat);
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * Replaces values greater than MAX_NUMBER_DOUBLE by MAX_NUMBER_DOUBLE and values smaller than MIN_NUMBER_DOUBLE by MIN_NUMBER_DOUBLE.
     *
     * @param data         data to be processed
     * @param minNumDouble minimal value
     * @param maxNumDouble maximal value
     */
    private static void processLargeValues(final DoubleLargeArray data, final double minNumDouble, final double maxNumDouble)
    {
        long n = data.length();
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {

                    for (long i = firstIdx; i < lastIdx; i++) {
                        double e = data.getDouble(i);
                        if (e < minNumDouble) {
                            data.setDouble(i, minNumDouble);
                        } else if (e > maxNumDouble) {
                            data.setDouble(i, maxNumDouble);
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * Tests whether data is NaN, Float.NEGATIVE_INFINITY or
     * Float.POSITIVE_INFINITY and returns a valid value according to actions nanAction and infAction.
     *
     * @param data      data to be processed
     * @param nanAction an action performed if data is NaN
     * @param infAction an action performed if data is Float.NEGATIVE_INFINITY or Float.POSITIVE_INFINITY
     *
     * @return a valid value according to actions nanAction and infAction
     *
     * @throws IllegalArgumentException if nanAction == EXCEPTION_AT_NAN or infAction == EXCEPTION_AT_INF
     */
    public static float processNaNs(float data, final NaNAction nanAction, final InfinityAction infAction) throws IllegalArgumentException
    {
        boolean containsNaN = false;
        boolean containsInf = false;
        boolean containsLargeValue = false;
        float res = data;
        float maxVal = -Float.MAX_VALUE;
        float minVal = Float.MAX_VALUE;
        if (Float.isNaN(data)) {
            switch (nanAction) {
                case NAN_AS_0:
                    res = 0f;
                    break;
                case NAN_AS_MAX_NUMBER_VAL:
                    res = MAX_NUMBER_FLOAT;
                    break;
                case NAN_AS_MIN_NUMBER_VAL:
                    res = MIN_NUMBER_FLOAT;
                    break;
                case EXCEPTION_AT_NAN:
                    throw new IllegalArgumentException("data cannot be equal to NaN");
                default:
                    break;
            }
            containsNaN = true;
        } else if (data == Float.POSITIVE_INFINITY || data == Float.NEGATIVE_INFINITY) {
            switch (infAction) {
                case INF_AS_0:
                    res = 0;
                    break;
                case INF_AS_EXTREME_NUMBER_VAL:
                    if (data == Float.POSITIVE_INFINITY) {
                        res = MAX_NUMBER_FLOAT;
                    } else {
                        res = MIN_NUMBER_FLOAT;
                    }
                    break;
                case EXCEPTION_AT_INF:
                    throw new IllegalArgumentException("data cannot be equal to Infinity");
                default:
                    break;
            }
            containsInf = true;
        } else {
            if (data < MIN_NUMBER_FLOAT || data > MAX_NUMBER_FLOAT) {
                containsLargeValue = true;
            }
            if (data < minVal) {
                minVal = data;
            }
            if (data > maxVal) {
                maxVal = data;
            }
        }

        if (containsLargeValue) {
            if (data < MIN_NUMBER_FLOAT) {
                res = MIN_NUMBER_FLOAT;
            } else if (data > MAX_NUMBER_FLOAT) {
                res = MAX_NUMBER_FLOAT;
            } else {
                res = data;
            }
            if (minVal < MIN_NUMBER_FLOAT) {
                minVal = MIN_NUMBER_FLOAT;
            }
            if (maxVal > MAX_NUMBER_FLOAT) {
                maxVal = MAX_NUMBER_FLOAT;
            }
        }

        if (!containsNaN && !containsInf) {
            return res;
        }

        if (containsInf && infAction == InfinityAction.INF_AS_EXTREME_DATA_VAL) {
            if (data == Float.POSITIVE_INFINITY) {
                res = maxVal;
            } else if (data == Float.NEGATIVE_INFINITY) {
                res = minVal;
            }
        }
        if (containsNaN && nanAction == NaNAction.NAN_AS_MAX_DATA_VAL) {
            if (Float.isNaN(data)) {
                res = maxVal;
            }
        } else if (containsNaN && nanAction == NaNAction.NAN_AS_MIN_DATA_VAL) {
            if (Float.isNaN(data)) {
                res = minVal;
            }
        }
        return res;
    }

    /**
     * Tests whether data array contains NaN, Float.NEGATIVE_INFINITY or
     * Float.POSITIVE_INFINITY elements and replaces them according to actions nanAction and infAction.
     *
     * @param data      data to be processed
     * @param nanAction an action performed on NaN elements
     * @param infAction an action performed on Float.NEGATIVE_INFINITY and Float.POSITIVE_INFINITY elements
     *
     * @return true if data array does not contain NaN, Float.NEGATIVE_INFINITY or Float.POSITIVE_INFINITY elements, false otherwise
     *
     * @throws IllegalArgumentException if data == null or nanAction == EXCEPTION_AT_NAN or infAction == EXCEPTION_AT_INF
     */
    public static boolean processNaNs(final FloatLargeArray data, final NaNAction nanAction, final InfinityAction infAction) throws IllegalArgumentException
    {
        if (data == null) {
            throw new IllegalArgumentException("data cannot be null.");
        }
        long n = data.length();
        boolean containsNaN = false;
        boolean containsInf = false;
        boolean containsLargeValue = false;
        float maxVal = -Float.MAX_VALUE;
        float minVal = Float.MAX_VALUE;
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Callable<float[]>()
            {
                @Override
                public float[] call() throws Exception
                {
                    float containsNaN = 0;
                    float containsInf = 0;
                    float containsLargeValue = 0;
                    float maxVal = -Float.MAX_VALUE;
                    float minVal = Float.MAX_VALUE;
                    for (long i = firstIdx; i < lastIdx; i++) {
                        float e = data.getFloat(i);
                        if (Float.isNaN(e)) {
                            switch (nanAction) {
                                case NAN_AS_0:
                                    data.setFloat(i, 0);
                                    break;
                                case NAN_AS_MAX_NUMBER_VAL:
                                    data.setFloat(i, MAX_NUMBER_FLOAT);
                                    break;
                                case NAN_AS_MIN_NUMBER_VAL:
                                    data.setFloat(i, MIN_NUMBER_FLOAT);
                                    break;
                                case EXCEPTION_AT_NAN:
                                    throw new IllegalArgumentException("data cannot contain NaN");
                                default:
                                    break;
                            }
                            containsNaN = 1;
                        } else if (e == Float.POSITIVE_INFINITY || e == Float.NEGATIVE_INFINITY) {
                            switch (infAction) {
                                case INF_AS_0:
                                    data.setFloat(i, 0);
                                    break;
                                case INF_AS_EXTREME_NUMBER_VAL:
                                    if (e == Float.POSITIVE_INFINITY) {
                                        data.setFloat(i, MAX_NUMBER_FLOAT);
                                    } else {
                                        data.setFloat(i, MIN_NUMBER_FLOAT);
                                    }
                                    break;
                                case EXCEPTION_AT_INF:
                                    throw new IllegalArgumentException("data cannot contain Infinity");
                                default:
                                    break;
                            }
                            containsInf = 1;
                        } else {
                            if (e < MIN_NUMBER_FLOAT || e > MAX_NUMBER_FLOAT) {
                                containsLargeValue = 1;
                            }
                            if (e < minVal) {
                                minVal = e;
                            }
                            if (e > maxVal) {
                                maxVal = e;
                            }
                        }
                    }
                    return new float[]{minVal, maxVal, containsNaN, containsInf, containsLargeValue};
                }
            });
        }
        try {
            for (int j = 0; j < nthreads; j++) {
                float[] res = (float[]) futures[j].get();
                if (res[0] < minVal) {
                    minVal = res[0];
                }
                if (res[1] > maxVal) {
                    maxVal = res[1];
                }
                if (res[2] == 1) {
                    containsNaN = true;
                }
                if (res[3] == 1) {
                    containsInf = true;
                }
                if (res[4] == 1) {
                    containsLargeValue = true;
                }
            }
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }

        if (containsLargeValue) {
            processLargeValues(data, MIN_NUMBER_FLOAT, MAX_NUMBER_FLOAT);
            if (minVal < MIN_NUMBER_FLOAT) {
                minVal = MIN_NUMBER_FLOAT;
            }
            if (maxVal > MAX_NUMBER_FLOAT) {
                maxVal = MAX_NUMBER_FLOAT;
            }
        }

        if (!containsNaN && !containsInf) {
            return true;
        }

        if (containsInf && infAction == InfinityAction.INF_AS_EXTREME_DATA_VAL) {
            processInfAsExtremeDataValue(data, minVal, maxVal);
        }
        if (containsNaN && nanAction == NaNAction.NAN_AS_MAX_DATA_VAL) {
            processNaNAsExtremeDataValue(data, maxVal);
        } else if (containsNaN && nanAction == NaNAction.NAN_AS_MIN_DATA_VAL) {
            processNaNAsExtremeDataValue(data, minVal);
        }
        return false;
    }

    /**
     * Tests whether data array contains NaN, Float.NEGATIVE_INFINITY or
     * Float.POSITIVE_INFINITY elements and replaces them according to actions nanAction and infAction.
     *
     * @param data      data to be processed
     * @param nanAction an action performed on NaN elements
     * @param infAction an action performed on Float.NEGATIVE_INFINITY and Float.POSITIVE_INFINITY elements
     *
     * @return true if data array does not contain NaN, Float.NEGATIVE_INFINITY or Float.POSITIVE_INFINITY elements, false otherwise
     *
     * @throws IllegalArgumentException if data == null or nanAction == EXCEPTION_AT_NAN or infAction == EXCEPTION_AT_INF
     */
    public static boolean processNaNs(ComplexFloatLargeArray data, NaNAction nanAction, InfinityAction infAction) throws IllegalArgumentException
    {
        if (data == null) {
            throw new IllegalArgumentException("data cannot be null.");
        }
        boolean res = processNaNs(data.getRealArray(), nanAction, infAction);
        return res || processNaNs(data.getImaginaryArray(), nanAction, infAction);
    }

    /**
     * Tests whether data is NaN, Double.NEGATIVE_INFINITY or
     * Double.POSITIVE_INFINITY and returns a valid value according to actions nanAction and infAction.
     *
     * @param data      data to be processed
     * @param nanAction an action performed if data is NaN
     * @param infAction an action performed if data is Double.NEGATIVE_INFINITY or Double.POSITIVE_INFINITY
     *
     * @return a valid value according to actions nanAction and infAction
     *
     * @throws IllegalArgumentException if nanAction == EXCEPTION_AT_NAN or infAction == EXCEPTION_AT_INF
     */
    public static double processNaNs(double data, final NaNAction nanAction, final InfinityAction infAction) throws IllegalArgumentException
    {
        boolean containsNaN = false;
        boolean containsInf = false;
        boolean containsLargeValue = false;
        double res = data;
        double maxVal = -Double.MAX_VALUE;
        double minVal = Double.MAX_VALUE;
        if (Double.isNaN(data)) {
            switch (nanAction) {
                case NAN_AS_0:
                    res = 0f;
                    break;
                case NAN_AS_MAX_NUMBER_VAL:
                    res = MAX_NUMBER_DOUBLE;
                    break;
                case NAN_AS_MIN_NUMBER_VAL:
                    res = MIN_NUMBER_DOUBLE;
                    break;
                case EXCEPTION_AT_NAN:
                    throw new IllegalArgumentException("data cannot be equal to NaN");
                default:
                    break;
            }
            containsNaN = true;
        } else if (data == Double.POSITIVE_INFINITY || data == Double.NEGATIVE_INFINITY) {
            switch (infAction) {
                case INF_AS_0:
                    res = 0;
                    break;
                case INF_AS_EXTREME_NUMBER_VAL:
                    if (data == Double.POSITIVE_INFINITY) {
                        res = MAX_NUMBER_DOUBLE;
                    } else {
                        res = MIN_NUMBER_DOUBLE;
                    }
                    break;
                case EXCEPTION_AT_INF:
                    throw new IllegalArgumentException("data cannot be equal to Infinity");
                default:
                    break;
            }
            containsInf = true;
        } else {
            if (data < MIN_NUMBER_DOUBLE || data > MAX_NUMBER_DOUBLE) {
                containsLargeValue = true;
            }
            if (data < minVal) {
                minVal = data;
            }
            if (data > maxVal) {
                maxVal = data;
            }
        }

        if (containsLargeValue) {
            if (data < MIN_NUMBER_DOUBLE) {
                res = MIN_NUMBER_DOUBLE;
            } else if (data > MAX_NUMBER_DOUBLE) {
                res = MAX_NUMBER_DOUBLE;
            } else {
                res = data;
            }
            if (minVal < MIN_NUMBER_DOUBLE) {
                minVal = MIN_NUMBER_DOUBLE;
            }
            if (maxVal > MAX_NUMBER_DOUBLE) {
                maxVal = MAX_NUMBER_DOUBLE;
            }
        }

        if (!containsNaN && !containsInf) {
            return res;
        }

        if (containsInf && infAction == InfinityAction.INF_AS_EXTREME_DATA_VAL) {
            if (data == Double.POSITIVE_INFINITY) {
                res = maxVal;
            } else if (data == Double.NEGATIVE_INFINITY) {
                res = minVal;
            }
        }
        if (containsNaN && nanAction == NaNAction.NAN_AS_MAX_DATA_VAL) {
            if (Double.isNaN(data)) {
                res = maxVal;
            }
        } else if (containsNaN && nanAction == NaNAction.NAN_AS_MIN_DATA_VAL) {
            if (Double.isNaN(data)) {
                res = minVal;
            }
        }
        return res;
    }

    /**
     * Tests whether data array contains NaN, Double.NEGATIVE_INFINITY or
     * Double.POSITIVE_INFINITY elements and replaces them according to actions nanAction and infAction.
     *
     * @param data      data to be processed
     * @param nanAction an action performed on NaN elements
     * @param infAction an action performed on Double.NEGATIVE_INFINITY and Double.POSITIVE_INFINITY elements
     *
     * @return true if data array does not contain NaN, Double.NEGATIVE_INFINITY or Double.POSITIVE_INFINITY elements, false otherwise
     *
     * @throws IllegalArgumentException if data == null or nanAction == EXCEPTION_AT_NAN or infAction == EXCEPTION_AT_INF
     */
    public static boolean processNaNs(final DoubleLargeArray data, final NaNAction nanAction, final InfinityAction infAction) throws IllegalArgumentException
    {
        if (data == null) {
            throw new IllegalArgumentException("data cannot be null.");
        }
        long n = data.length();
        boolean containsNaN = false;
        boolean containsInf = false;
        boolean containsLargeValue = false;
        double maxVal = -Double.MAX_VALUE;
        double minVal = Double.MAX_VALUE;
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
            {
                @Override
                public double[] call() throws Exception
                {
                    double containsNaN = 0;
                    double containsInf = 0;
                    double containsLargeValue = 0;
                    double maxVal = -Double.MAX_VALUE;
                    double minVal = Double.MAX_VALUE;
                    for (long i = firstIdx; i < lastIdx; i++) {
                        double e = data.getDouble(i);
                        if (Double.isNaN(e)) {
                            switch (nanAction) {
                                case NAN_AS_0:
                                    data.setDouble(i, 0);
                                    break;
                                case NAN_AS_MAX_NUMBER_VAL:
                                    data.setDouble(i, MAX_NUMBER_DOUBLE);
                                    break;
                                case NAN_AS_MIN_NUMBER_VAL:
                                    data.setDouble(i, MIN_NUMBER_DOUBLE);
                                    break;
                                case EXCEPTION_AT_NAN:
                                    throw new IllegalArgumentException("data cannot contain NaN");
                                default:
                                    break;
                            }
                            containsNaN = 1;
                        } else if (e == Double.POSITIVE_INFINITY || e == Double.NEGATIVE_INFINITY) {
                            switch (infAction) {
                                case INF_AS_0:
                                    data.setDouble(i, 0);
                                    break;
                                case INF_AS_EXTREME_NUMBER_VAL:
                                    if (e == Double.POSITIVE_INFINITY) {
                                        data.setDouble(i, MAX_NUMBER_DOUBLE);
                                    } else {
                                        data.setDouble(i, MIN_NUMBER_DOUBLE);
                                    }
                                    break;
                                case EXCEPTION_AT_INF:
                                    throw new IllegalArgumentException("data cannot contain Infinity");
                                default:
                                    break;
                            }
                            containsInf = 1;
                        } else {
                            if (e < MIN_NUMBER_DOUBLE || e > MAX_NUMBER_DOUBLE) {
                                containsLargeValue = 1;
                            }
                            if (e < minVal) {
                                minVal = e;
                            }
                            if (e > maxVal) {
                                maxVal = e;
                            }
                        }
                    }
                    return new double[]{minVal, maxVal, containsNaN, containsInf, containsLargeValue};
                }
            });
        }
        try {
            for (int j = 0; j < nthreads; j++) {
                double[] res = (double[]) futures[j].get();
                if (res[0] < minVal) {
                    minVal = res[0];
                }
                if (res[1] > maxVal) {
                    maxVal = res[1];
                }
                if (res[2] == 1) {
                    containsNaN = true;
                }
                if (res[3] == 1) {
                    containsInf = true;
                }
                if (res[4] == 1) {
                    containsLargeValue = true;
                }
            }
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }

        if (containsLargeValue) {
            processLargeValues(data, MIN_NUMBER_DOUBLE, MAX_NUMBER_DOUBLE);
            if (minVal < MIN_NUMBER_DOUBLE) {
                minVal = MIN_NUMBER_DOUBLE;
            }
            if (maxVal > MAX_NUMBER_DOUBLE) {
                maxVal = MAX_NUMBER_DOUBLE;
            }
        }

        if (!containsNaN && !containsInf) {
            return true;
        }

        if (containsInf && infAction == InfinityAction.INF_AS_EXTREME_DATA_VAL) {
            processInfAsExtremeDataValue(data, minVal, maxVal);
        }
        if (containsNaN && nanAction == NaNAction.NAN_AS_MAX_DATA_VAL) {
            processNaNAsExtremeDataValue(data, maxVal);
        } else if (containsNaN && nanAction == NaNAction.NAN_AS_MIN_DATA_VAL) {
            processNaNAsExtremeDataValue(data, minVal);
        }
        return false;
    }
}
