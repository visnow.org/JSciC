/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.apache.commons.math3.util.FastMath;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.cells.Quad;
import org.visnow.jscic.cells.Triangle;

/**
 * Utility for parallel generation of (boundary) edges of a cell set
 * and estimation of feature angles along edges
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 */
public class CellSetGeometryUtilities
{

    private static final double CNV = 180 / Math.PI;

    static class Segment
    {

        private final int p0;
        private final int p1;
        private float[] normal;
        private float angle = -999.f;

        public Segment(int p0, int p1, float[] normal)
        {
            if (p0 < p1) {
                this.p0 = p0;
                this.p1 = p1;
            } else {
                this.p0 = p1;
                this.p1 = p0;
            }
            this.normal = normal;
        }

        @Override
        public int hashCode()
        {
            return ((5 * 41) + p0) * 41 + p1;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            final Segment other = (Segment) obj;
            return p0 == other.p0 && p1 == other.p1;
        }

        public float getAngle()
        {
            return angle;
        }

        public void setAngle(float angle)
        {
            this.angle = angle;
            normal = null;
        }

        public float[] getNormal()
        {
            return normal;
        }

    }

    static class TriangularFace
    {

        private final int p0;
        private final int p1;
        private final int p2;
        private final byte orientation;

        public TriangularFace(Triangle q)
        {
            p0 = q.getVertices()[0];
            p1 = q.getVertices()[1];
            p2 = q.getVertices()[2];
            orientation = q.getOrientation();
        }

        @Override
        public int hashCode()
        {
            return (((5 * 41) + p0) * 41 + p1) * 41 + p2;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            final TriangularFace other = (TriangularFace) obj;
            return p0 == other.p0 && p1 == other.p1 && p2 == other.p2;
        }
    }

    static class QuadFace
    {

        private final int p0;
        private final int p1;
        private final int p2;
        private final int p3;
        private final byte orientation;

        public QuadFace(Quad q)
        {
            p0 = q.getVertices()[0];
            p1 = q.getVertices()[1];
            p2 = q.getVertices()[2];
            p3 = q.getVertices()[3];
            orientation = q.getOrientation();
        }

        @Override
        public int hashCode()
        {
            int hash = 5;
            hash = 41 * hash + p0;
            hash = 41 * hash + p1;
            hash = 41 * hash + p2;
            hash = 41 * hash + p3;
            return hash;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            final QuadFace other = (QuadFace) obj;
            return p0 == other.p0 && p1 == other.p1 &&
                p2 == other.p2 && p3 == other.p3;
        }
    }

    private static void mergeTo(HashMap<Segment, Segment> segments, Segment s)
    {
        if (segments.containsKey(s)) {
            Segment s0 = segments.get(s);
            if (s0.normal == null || s.normal == null)
                s0.setAngle(180);
            else {
                float sp = 0;
                for (int k = 0; k < 3; k++)
                    sp += s.normal[k] * s0.normal[k];
                s0.setAngle((float) (CNV * Math.acos(Math.min(1, Math.max(-1, sp)))));
            }
        } else
            segments.put(s, s);
    }

    private static class CreateEdgesDataPart implements Runnable
    {

        private final int nThreads;
        private final int iThread;
        private final int nCellNodes;
        private final int nNodes;
        private final int[] nodes;
        private final FloatLargeArray normals;
        private final HashMap<Segment, Segment> segments;

        public CreateEdgesDataPart(int nThreads, int iThread,
                                   int nCellNodes, int[] nodes, FloatLargeArray normals,
                                   HashMap<Segment, Segment> segments)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.nCellNodes = nCellNodes;
            this.nodes = nodes;
            nNodes = nodes.length / nCellNodes;
            this.normals = normals;
            this.segments = segments;
        }

        @Override
        public void run()
        {
            int[] cellNodes = new int[nCellNodes];
            for (int i = (nNodes * iThread) / nThreads;
                i < (nNodes * (iThread + 1)) / nThreads;
                i++) {
                System.arraycopy(nodes, i * nCellNodes, cellNodes, 0, nCellNodes);
                float[] norm = new float[3];
                LargeArrayUtils.arraycopy(normals, 3 * i, norm, 0, 3);
                for (int j = 0; j < cellNodes.length; j++) {
                    mergeTo(segments,
                            new Segment(cellNodes[j], cellNodes[(j + 1) % nCellNodes], norm));
                }
            }
        }
    }

    /**
     * For a given CellArray[] containing triangles and/or quads creates an array
     * of segments edges, removes duplicate edges
     * and computes dihedrals at these segments.
     *
     * @param boundaryCellArrays a CellArray[] for display (original CellSet.cellArrays in the case
     *                           of two-dimensional cell set or CellSet.boundaryCellArrays in the case of 3D cell set
     * @param triNormals         precomputed normal vector for triangle cells in boundaryCellArrays
     * @param quadNormals        precomputed normal vector for quad cells in boundaryCellArrays
     *
     * @return a Segment CellArray containing edges of the input boundaryCellArrays without duplicate edges
     *         with dihedral edges (in degrees) computed,
     *         in the case of external edges or singular (self-intersection) edges the angle value
     *         is set to 180.
     */
    public static final CellArray generateSurfaceEdgesWithAngles(CellArray[] boundaryCellArrays,
                                                                 FloatLargeArray triNormals, FloatLargeArray quadNormals)
    {
        int nCores = Runtime.getRuntime().availableProcessors();

        HashMap<Segment, Segment> segments = new HashMap<>();
        if (boundaryCellArrays[CellType.TRIANGLE.getValue()] != null) {
            CellArray triangles = boundaryCellArrays[CellType.TRIANGLE.getValue()];
            int nThreads = Math.min(nCores, (triangles.getNCells() * 3) / 100000 + 1);
            if (nThreads == 1)
                new CreateEdgesDataPart(1, 0, 3, triangles.getNodes(), triNormals, segments).run();
            else {
                Future[] futures = new Future[nThreads];
                HashMap<Segment, Segment>[] segmentParts = new HashMap[nThreads];
                for (int iThread = 0; iThread < nThreads; iThread++) {
                    segmentParts[iThread] = new HashMap<>();
                    futures[iThread] = ConcurrencyUtils.submit(
                        new CreateEdgesDataPart(nThreads, iThread, 3, triangles.getNodes(),
                                                triNormals, segmentParts[iThread]));
                }
                try {
                    ConcurrencyUtils.waitForCompletion(futures);
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException();
                }
                for (HashMap<Segment, Segment> segmentPart : segmentParts)
                    for (Segment s : segmentPart.values())
                        mergeTo(segments, s);
            }
        }
        if (boundaryCellArrays[CellType.QUAD.getValue()] != null) {
            CellArray quads = boundaryCellArrays[CellType.QUAD.getValue()];
            int nThreads = Math.min(nCores, (quads.getNCells() * 4) / 100000 + 1);
            if (nThreads == 1)
                new CreateEdgesDataPart(1, 0, 4, quads.getNodes(), quadNormals, segments).run();
            else {
                Future[] futures = new Future[nThreads];
                HashMap<Segment, Segment>[] segmentParts = new HashMap[nThreads];
                for (int iThread = 0; iThread < nThreads; iThread++) {
                    segmentParts[iThread] = new HashMap<>();
                    futures[iThread] = ConcurrencyUtils.submit(
                        new CreateEdgesDataPart(nThreads, iThread, 4, quads.getNodes(),
                                                quadNormals, segmentParts[iThread]));
                }
                try {
                    ConcurrencyUtils.waitForCompletion(futures);
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException();
                }
                for (HashMap<Segment, Segment> segmentPart : segmentParts)
                    for (Segment s : segmentPart.values())
                        mergeTo(segments, s);
            }
        }
        int nEdges = segments.size();
        int[] nodes = new int[2 * nEdges];
        float[] dihedrals = new float[nEdges];
        int iEdge = 0;
        for (Segment s : segments.values()) {
            nodes[2 * iEdge] = s.p0;
            nodes[2 * iEdge + 1] = s.p1;
            dihedrals[iEdge] = s.angle >= 0 ? s.angle : 180;
            iEdge += 1;
        }
        CellArray out = new CellArray(CellType.SEGMENT, nodes, null, null);
        out.setCellDihedrals(dihedrals);
        return out;
    }

    /**
     * Calls CellSet.addGeometryData(coords) for each element of cellSets list.
     * 
     * @param cellSets cell sets
     * @param coords coordinates
     */
    public static void addGeometryDataToCellSets(ArrayList<CellSet> cellSets, FloatLargeArray coords)
    {
        if(cellSets == null || cellSets.isEmpty() || coords == null) {
            return;
        }
        int nthreads = FastMath.min(cellSets.size(), ConcurrencyUtils.getNumberOfThreads());
        int k = cellSets.size() / nthreads;
        Future[] threads = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final int firstIdx = j * k;
            final int lastIdx = (j == nthreads - 1) ? cellSets.size() : firstIdx + k;
            threads[j] = ConcurrencyUtils.submit(() -> {
                for (int i = firstIdx; i < lastIdx; i++) {
                    cellSets.get(i).addGeometryData(coords, false);
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(threads);
        } catch (InterruptedException | ExecutionException ex) {
            cellSets.forEach(cs -> {
                cs.addGeometryData(coords, true);
            });
        }
    }

    private CellSetGeometryUtilities()
    {

    }
}
