/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

/**
 * Physical constants.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public enum PhysicalConstants
{
    //                     description                   symbol       HTML symbol                     value            unit
    ATOMIC_MASS            ("atomic mass constant",      "m_u",       "<html>m<sub>u</html>",         1.660539040E-27,  "kg"),
    AVOGARDO_NA            ("Avogadro constant (N_A)",   "N_A",       "<html>N<sub>A</html>",         6.022140857E23,   "mol-1"),
    AVOGARDO_L9            ("Avogadro constant (L)",     "L",         "<html>L</html>",               6.022140857E23,   "mol-1"),
    BOLTZMANN              ("Boltzmann constant",        "k_B",       "<html>k<sub>B</html>",         1.38064852E-23,   "J K-1"),
    CONDUCTANCE_QUANTUM    ("conductance quantum",       "G_0",       "<html>G<sub>0</html>",         7.7480917310E-5,  "S"),
    ELECTRIC               ("electric constant",         "epsilon_0", "<html>&epsilon<sub>0</html>",  8.854187817E-12,  "F m-1"),
    ELECTRON_MASS          ("electron mass",             "m_e",       "<html>m<sub>e</html>",         9.10938356E-31,   "kg"),
    ELECTRON_VOLT          ("electron volt",             "eV",        "<html>eV</html>",              1.6021766208E-19, "J"),
    ELEMENTARY_CHARGE      ("elementary charge",         "e",         "<html>e</html>",               1.6021766208E-19, "C"),
    FARADAY                ("Faraday constant",          "f",         "<html>f</html>",               96485.33289,      "C mol-1"),
    FINE_STRUCTURE         ("fine-structure constant",   "alpha",     "<html>&alpha</html>",          7.2973525664E-3,  "1"),
    MAGNETIC               ("magnetic constant",         "mu_0",      "<html>&mu<sub>0</html>",       12.566370614E-7,  "N A-2"),
    MAGNETIC_FLUX_QUANTUM  ("magnetic flux quantum",     "Phi_0",     "<html>&Phi<sub>0</html>",      2.067833831E-15,  "Wb"),
    MOLAR_GAS              ("molar gas constant",        "R",         "<html>R</html>",               8.3144598,        "J mol-1 K-1"),
    GRAVITATIONAL_CONSTANT ("gravitational constant",    "G",         "<html>G</html>",               6.67408E-11,      "m3 kg-1 s-2"),
    PLANCK                 ("Planck constant",           "h",         "<html>&#x210F</html>",         6.626070040E-34,  "J s"),
    PROTON_MASS            ("proton mass",               "m_p",       "<html>m<sub>p</html>",         1.672621898E-27,  "kg"),
    RYDBERG                ("Rydberg constant",          "R_inf",     "<html>R<sub>&infin</html>",    10973731.568508,  "m-1"),
    SPEED_OF_LIGH          ("speed of light in vacuum",  "c",         "<html>c</html>",               299792458,        "m s-1"),
    STEFAN_BOLTZMAN        ("Stefan-Boltzmann constant", "sigma",     "<html>&sigma</html>",          5.670367E-8,      "W m-2 K-4");
    
    private final String description;
    private final String symbol;
    private final String htmlSymbol;
    private final double value;
    private final String unit;

    private PhysicalConstants(String description, String symbol, String htmlSymbol, double value, String unit)
    {
        this.description = description;
        this.symbol = symbol;
        this.htmlSymbol = htmlSymbol;
        this.value = value;
        this.unit = unit;
    }
    
/**
 * getter for constant description
 * @return constant description
 */
    public String getDescription()
    {
        return description;
    }
    
/**
 * getter for simplified (ASCII) symbol
 * @return ASCII constant symbol
 */
    public String getSymbol()
    {
        return symbol;
    }
   
/**
 * getter for printable HTML symbol
 * @return HTML constant symbol with greek letters, subscripts etc.
 */
    public String getHtmlSymbol()
    {
        return htmlSymbol;
    }

/**
 * getter for constant numeric value
 * @return constant value in SI units
 */
    public double getValue()
    {
        return value;
    }

/**
 * getter for ASCII unit string
 * @return ASCII unit string 
 */
    public String getUnit()
    {
        return unit;
    }
}
