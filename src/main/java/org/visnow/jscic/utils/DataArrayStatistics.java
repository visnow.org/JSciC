/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.apache.commons.math3.util.FastMath;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayStatistics;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.dataarrays.DataObjectInterface;

/**
 * Statistical operations on DataArrays.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class DataArrayStatistics
{

    private static final double DELTA = 1E-14;

    /**
     * Sum of elements in DataArray. For DataArrays with veclen &gt; 1 this operation calculates a sum vector (sum of each vector component over elements) and
     * returns the array of size [Ntimesteps][veclen] of sum vectors in each time step.
     *
     * @param da input DataArray
     *
     * @return sum of elements in each time step
     */
    public static double[][] sum(DataArray da)
    {
        if (da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX");
        }
        TimeData td = da.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList();
        final int veclen = da.getVectorLength();
        double[][] res = new double[td.getNSteps()][];
        if (veclen == 1) {
            int t = 0;
            for (Float time : timeSeries) {
                LargeArray a = td.getValue(time);
                res[t++] = new double[]{LargeArrayStatistics.sum(a)};
            }
        } else {
            long length = da.getNElements();
            int t = 0;
            for (Float time : timeSeries) {
                final LargeArray a = td.getValue(time);
                double[] sum = new double[veclen];
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                    for (long i = 0; i < length; i++) {
                        for (int v = 0; v < veclen; v++) {
                            sum[v] += a.getDouble(i * veclen + v);
                        }
                    }
                } else {
                    long k = length / nthreads;
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        threads[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call()
                            {
                                double[] lsum = new double[veclen];
                                for (long k = firstIdx; k < lastIdx; k++) {
                                    for (int v = 0; v < veclen; v++) {
                                        lsum[v] += a.getDouble(k * veclen + v);
                                    }
                                }
                                return lsum;
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                        for (int j = 0; j < nthreads; j++) {
                            double[] lsum = (double[]) threads[j].get();
                            for (int v = 0; v < veclen; v++) {
                                sum[v] += lsum[v];
                            }
                        }
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen; v++) {
                                sum[v] += a.getDouble(i * veclen + v);
                            }
                        }
                    }
                }
                res[t++] = sum;
            }
        }
        return res;
    }

    /**
     * Sum of elements in DataArray computed using Kahan's summation algorithm. For DataArrays with veclen &gt; 1 this operation calculates a sum vector (sum of
     * each vector component over elements) and returns the array of size [Ntimesteps][veclen] of sum vectors in each time step.
     *
     * @param da input DataArray
     *
     * @return sum of elements in each time step
     */
    public static double[][] sumKahan(DataArray da)
    {
        if (da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX");
        }
        TimeData td = da.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList();
        final int veclen = da.getVectorLength();
        double[][] res = new double[td.getNSteps()][];
        if (veclen == 1) {
            int t = 0;
            for (Float time : timeSeries) {
                LargeArray a = td.getValue(time);
                res[t++] = new double[]{LargeArrayStatistics.sumKahan(a)};
            }
        } else {
            long length = da.getNElements();
            int t = 0;
            for (Float time : timeSeries) {
                final LargeArray a = td.getValue(time);
                double[] sum = new double[veclen];
                double[] c = new double[veclen];
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                    for (long i = 0; i < length; i++) {
                        for (int v = 0; v < veclen; v++) {
                            double y = a.getDouble(i * veclen + v) - c[v];
                            double tt = sum[v] + y;
                            c[v] = (tt - sum[v]) - y;
                            sum[v] = tt;
                        }
                    }
                } else {
                    long k = length / nthreads;
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        threads[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call()
                            {
                                double[] sum = new double[veclen];
                                double[] c = new double[veclen];
                                for (long k = firstIdx; k < lastIdx; k++) {
                                    for (int v = 0; v < veclen; v++) {
                                        double y = a.getDouble(k * veclen + v) - c[v];
                                        double tt = sum[v] + y;
                                        c[v] = (tt - sum[v]) - y;
                                        sum[v] = tt;
                                    }
                                }
                                return sum;
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                        for (int j = 0; j < nthreads; j++) {
                            double[] lsum = (double[]) threads[j].get();
                            for (int v = 0; v < veclen; v++) {
                                sum[v] += lsum[v];
                            }
                        }
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen; v++) {
                                double y = a.getDouble(i * veclen + v) - c[v];
                                double tt = sum[v] + y;
                                c[v] = (tt - sum[v]) - y;
                                sum[v] = tt;
                            }
                        }
                    }
                }
                res[t++] = sum;
            }
        }
        return res;
    }

    /**
     * Average value of elements in DataArray. For DataArrays with veclen &gt; 1 this operation calculates an average vector (average of each vector component
     * over
     * elements) and returns the array of size [Ntimesteps][veclen] of average vectors in each time step.
     *
     * @param da input DataArray
     *
     * @return average of elements in each time step
     */
    public static double[][] avg(DataArray da)
    {
        if (da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX");
        }
        TimeData td = da.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList();
        final int veclen = da.getVectorLength();
        double[][] res = new double[td.getNSteps()][];
        if (veclen == 1) {
            int t = 0;
            for (Float time : timeSeries) {
                LargeArray a = td.getValue(time);
                res[t++] = new double[]{LargeArrayStatistics.avg(a)};
            }
        } else {
            long length = da.getNElements();
            int t = 0;
            for (Float time : timeSeries) {
                final LargeArray a = td.getValue(time);
                double[] sum = new double[veclen];
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                    for (long i = 0; i < length; i++) {
                        for (int v = 0; v < veclen; v++) {
                            sum[v] += a.getDouble(i * veclen + v);
                        }
                    }
                } else {
                    long k = length / nthreads;
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        threads[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call()
                            {
                                double[] lsum = new double[veclen];
                                for (long k = firstIdx; k < lastIdx; k++) {
                                    for (int v = 0; v < veclen; v++) {
                                        lsum[v] += a.getDouble(k * veclen + v);
                                    }
                                }
                                return lsum;
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                        for (int j = 0; j < nthreads; j++) {
                            double[] lsum = (double[]) threads[j].get();
                            for (int v = 0; v < veclen; v++) {
                                sum[v] += lsum[v];
                            }
                        }
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen; v++) {
                                sum[v] += a.getDouble(i * veclen + v);
                            }
                        }
                    }
                }
                for (int v = 0; v < veclen; v++) {
                    sum[v] /= length;
                }
                res[t++] = sum;
            }
        }
        return res;
    }

    /**
     * Average value of elements in DataArray computed using Kahan's summation algorithm. For DataArrays with veclen &gt; 1 this operation calculates an average
     * vector (average of each vector component over elements) and returns the array of size [Ntimesteps][veclen] of average vectors in each time step.
     *
     * @param da input DataArray
     *
     * @return average of elements in each time step
     */
    public static double[][] avgKahan(DataArray da)
    {
        if (da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX");
        }
        TimeData td = da.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList();
        final int veclen = da.getVectorLength();
        double[][] res = new double[td.getNSteps()][];
        if (veclen == 1) {
            int t = 0;
            for (Float time : timeSeries) {
                LargeArray a = td.getValue(time);
                res[t++] = new double[]{LargeArrayStatistics.avgKahan(a)};
            }
        } else {
            long length = da.getNElements();
            int t = 0;
            for (Float time : timeSeries) {
                final LargeArray a = td.getValue(time);
                double[] sum = new double[veclen];
                double[] c = new double[veclen];
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                    for (long i = 0; i < length; i++) {
                        for (int v = 0; v < veclen; v++) {
                            double y = a.getDouble(i * veclen + v) - c[v];
                            double tt = sum[v] + y;
                            c[v] = (tt - sum[v]) - y;
                            sum[v] = tt;
                        }
                    }
                } else {
                    long k = length / nthreads;
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        threads[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call()
                            {
                                double[] sum = new double[veclen];
                                double[] c = new double[veclen];
                                for (long k = firstIdx; k < lastIdx; k++) {
                                    for (int v = 0; v < veclen; v++) {
                                        double y = a.getDouble(k * veclen + v) - c[v];
                                        double tt = sum[v] + y;
                                        c[v] = (tt - sum[v]) - y;
                                        sum[v] = tt;
                                    }
                                }
                                return sum;
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                        for (int j = 0; j < nthreads; j++) {
                            double[] lsum = (double[]) threads[j].get();
                            for (int v = 0; v < veclen; v++) {
                                sum[v] += lsum[v];
                            }
                        }
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen; v++) {
                                double y = a.getDouble(i * veclen + v) - c[v];
                                double tt = sum[v] + y;
                                c[v] = (tt - sum[v]) - y;
                                sum[v] = tt;
                            }
                        }
                    }
                }
                for (int v = 0; v < veclen; v++) {
                    sum[v] /= length;
                }
                res[t++] = sum;
            }
        }
        return res;
    }

    /**
     * Standard deviation value of elements in DataArray. For DataArrays with veclen &gt; 1 this operation calculates a standard deviation vector (standard
     * deviation of each vector component over elements) and returns the array of size [Ntimesteps][veclen] of standard deviation vectors in each time step.
     *
     * @param da input DataArray
     *
     * @return standard deviation of elements in each time step
     */
    public static double[][] std(DataArray da)
    {
        if (da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX");
        }
        TimeData td = da.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList();
        final int veclen = da.getVectorLength();
        double[][] res = new double[td.getNSteps()][];
        if (veclen == 1) {
            int t = 0;
            for (Float time : timeSeries) {
                LargeArray a = td.getValue(time);
                res[t++] = new double[]{LargeArrayStatistics.std(a)};
            }
        } else {
            long length = da.getNElements();
            int t = 0;
            for (Float time : timeSeries) {
                final LargeArray a = td.getValue(time);
                double[] sum = new double[veclen];
                double[] sum2 = new double[veclen];
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                    for (long i = 0; i < length; i++) {
                        for (int v = 0; v < veclen; v++) {
                            double elem = a.getDouble(i * veclen + v);
                            sum[v] += elem;
                            sum2[v] += elem * elem;
                        }
                    }
                } else {
                    long k = length / nthreads;
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        threads[j] = ConcurrencyUtils.submit(new Callable<double[][]>()
                        {
                            @Override
                            public double[][] call()
                            {
                                double[][] lsum = new double[2][veclen];
                                for (long k = firstIdx; k < lastIdx; k++) {
                                    for (int v = 0; v < veclen; v++) {
                                        double elem = a.getDouble(k * veclen + v);
                                        lsum[0][v] += elem;
                                        lsum[1][v] += elem * elem;
                                    }
                                }
                                return lsum;
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                        for (int j = 0; j < nthreads; j++) {
                            double[][] lsum = (double[][]) threads[j].get();
                            for (int v = 0; v < veclen; v++) {
                                sum[v] += lsum[0][v];
                                sum2[v] += lsum[1][v];
                            }
                        }
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen; v++) {
                                double elem = a.getDouble(i * veclen + v);
                                sum[v] += elem;
                                sum2[v] += elem * elem;
                            }
                        }
                    }
                }
                for (int v = 0; v < veclen; v++) {
                    sum[v] = sqrt(FastMath.max(0.0, (sum2[v] / length) - (sum[v] / length) * (sum[v] / length)));
                }
                res[t++] = sum;
            }
        }
        return res;
    }

    /**
     * Standard deviation value of elements in DataArray computed using Kahan's summation algorithm. For DataArrays with veclen &gt; 1 this operation calculates
     * a
     * standard deviation vector (standard deviation of each vector component over elements) and returns the array of size [Ntimesteps][veclen] of standard
     * deviation vectors in each time step.
     *
     * @param da input DataArray
     *
     * @return standard deviation of elements in each time step
     */
    public static double[][] stdKahan(DataArray da)
    {
        if (da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX");
        }
        TimeData td = da.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList();
        final int veclen = da.getVectorLength();
        double[][] res = new double[td.getNSteps()][];
        if (veclen == 1) {
            int t = 0;
            for (Float time : timeSeries) {
                LargeArray a = td.getValue(time);
                res[t++] = new double[]{LargeArrayStatistics.stdKahan(a)};
            }
        } else {
            long length = da.getNElements();
            int t = 0;
            for (Float time : timeSeries) {
                final LargeArray a = td.getValue(time);
                double[] sum = new double[veclen];
                double[] c = new double[veclen];
                double[] sum2 = new double[veclen];
                double[] c2 = new double[veclen];
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                    for (long i = 0; i < length; i++) {
                        for (int v = 0; v < veclen; v++) {
                            double elem = a.getDouble(i * veclen + v);
                            double y = elem - c[v];
                            double tt = sum[v] + y;
                            c[v] = (tt - sum[v]) - y;
                            sum[v] = tt;

                            y = elem * elem - c2[v];
                            tt = sum2[v] + y;
                            c2[v] = (tt - sum2[v]) - y;
                            sum2[v] = tt;
                        }
                    }
                } else {
                    long k = length / nthreads;
                    Future[] threads = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        threads[j] = ConcurrencyUtils.submit(new Callable<double[][]>()
                        {
                            @Override
                            public double[][] call()
                            {
                                double[][] sum = new double[2][veclen];
                                double[][] c = new double[2][veclen];
                                for (long k = firstIdx; k < lastIdx; k++) {
                                    for (int v = 0; v < veclen; v++) {
                                        double elem = a.getDouble(k * veclen + v);
                                        double y = elem - c[0][v];
                                        double tt = sum[0][v] + y;
                                        c[0][v] = (tt - sum[0][v]) - y;
                                        sum[0][v] = tt;

                                        y = elem * elem - c[1][v];
                                        tt = sum[1][v] + y;
                                        c[1][v] = (tt - sum[1][v]) - y;
                                        sum[1][v] = tt;
                                    }
                                }
                                return sum;
                            }
                        });
                    }
                    try {
                        ConcurrencyUtils.waitForCompletion(threads);
                        for (int j = 0; j < nthreads; j++) {
                            double[][] lsum = (double[][]) threads[j].get();
                            for (int v = 0; v < veclen; v++) {
                                sum[v] += lsum[0][v];
                                sum2[v] += lsum[1][v];
                            }
                        }
                    } catch (InterruptedException | ExecutionException ex) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen; v++) {
                                double elem = a.getDouble(i * veclen + v);
                                double y = elem - c[v];
                                double tt = sum[v] + y;
                                c[v] = (tt - sum[v]) - y;
                                sum[v] = tt;

                                y = elem * elem - c2[v];
                                tt = sum2[v] + y;
                                c2[v] = (tt - sum2[v]) - y;
                                sum2[v] = tt;
                            }
                        }
                    }
                }
                for (int v = 0; v < veclen; v++) {
                    sum[v] = sqrt(FastMath.max(0.0, (sum2[v] / length) - (sum[v] / length) * (sum[v] / length)));
                }
                res[t++] = sum;
            }
        }
        return res;
    }

    /**
     * Minimum value of elements in DataArray. It does not support DataArrays with veclen &gt; 1.
     *
     * @param da input DataArray
     *
     * @return minimum of elements in each time step
     */
    public static double[] min(DataArray da)
    {
        if (da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX || da.getVectorLength() > 1) {
            throw new IllegalArgumentException("da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX || da.getVectorLength() > 1");
        }
        TimeData td = da.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList();
        double[] res = new double[td.getNSteps()];
        int t = 0;
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            res[t++] = LargeArrayStatistics.min(a);
        }
        return res;
    }

    /**
     * Maximum value of elements in DataArray. It does not support DataArrays with veclen &gt; 1.
     *
     * @param da input DataArray
     *
     * @return maximum of elements in each time step
     */
    public static double[] max(DataArray da)
    {
        if (da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX || da.getVectorLength() > 1) {
            throw new IllegalArgumentException("da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX || da.getVectorLength() > 1");
        }
        TimeData td = da.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList();
        double[] res = new double[td.getNSteps()];
        int t = 0;
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            res[t++] = LargeArrayStatistics.max(a);
        }
        return res;
    }

    /**
     * Returns the histogram. If min=max, all data "goes to" the zero bin
     *
     * @param da                     input DataArray
     * @param min                    min value
     * @param max                    max value
     * @param nbin                   number of bins
     * @param ignoreOutsideMinMax    if true, the data outside [min,max) interval are ignored (provided max &gt; min)
     *                               if false, the data outside [min,max) "goes to" the nearest bin (provided max &gt; min)
     * @param timeMask               mask It might depend on time
     * @param computeGlobalHistogram if true, then the global histogram of all time steps is computed
     *
     * @return histogram
     */
    public static long[] histogram(final DataArray da, final double min, final double max, final int nbin, boolean ignoreOutsideMinMax, final TimeData timeMask, boolean computeGlobalHistogram)
    {
        if (nbin < 1) {
            throw new IllegalArgumentException("Number of bins must be greater than 0.");
        }

        if (max < min) {
            throw new IllegalArgumentException("max>=min must be satisfied");
        }

        long[] valueHistogram = new long[nbin];
        final int vlen = da.getVectorLength();
        final double binSize;
        TimeData timeData = da.getTimeData();
        if ((max - min) < DELTA) {
            valueHistogram[0] = da.getNElements();
            return valueHistogram;
        } else {
            binSize = (max - min) / (nbin);
        }

        class HistogramCallableIgnoreOutside implements Callable<long[]>
        {

            private final LargeArray dta;
            private final LargeArray currentMask;
            private final long firstIdx;
            private final long lastIdx;

            HistogramCallableIgnoreOutside(final LargeArray dta, final LargeArray currentMask, final long firstIdx, final long lastIdx)
            {
                this.dta = dta;
                this.currentMask = currentMask;
                this.firstIdx = firstIdx;
                this.lastIdx = lastIdx;
            }

            @Override
            public long[] call() throws Exception
            {
                long[] valueHistogram = new long[nbin];
                if (dta.getType() == LargeArrayType.OBJECT) {
                    if ((vlen == 1) && (currentMask == null)) {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double f = (double) ((DataObjectInterface) dta.get(i)).toFloat();
                            int bin = (int) ((f - min) / binSize);
                            if (bin < 0 || bin > nbin - 1) {
                                continue;
                            }
                            valueHistogram[bin] += 1;
                        } //endfor
                    } else if ((vlen == 1) && (currentMask != null)) {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            if (currentMask.getBoolean(i) == true) {
                                double f = (double) ((DataObjectInterface) dta.get(i)).toFloat();
                                int bin = (int) ((f - min) / binSize);
                                if (bin < 0 || bin > nbin - 1) {
                                    continue;
                                }
                                valueHistogram[bin] += 1;
                            }
                        } //endfor
                    } else if ((vlen > 1) && (currentMask == null)) {

                        for (long i = firstIdx; i < lastIdx; i += vlen) {
                            double f = 0;
                            for (long j = 0; j < vlen; j++) {
                                double tmp = (double) ((DataObjectInterface) dta.get(i + j)).toFloat();
                                f += tmp * tmp;
                            }
                            f = sqrt(f);
                            int bin = (int) ((f - min) / binSize);
                            if (bin < 0 || bin > nbin - 1) {
                                continue;
                            }
                            valueHistogram[bin] += 1;
                        }//endfor
                    } else { // (vlen > 1) && (currentMask != null)
                        for (long i = firstIdx; i < lastIdx; i += vlen) {
                            if (currentMask.getBoolean(i / vlen) == true) {
                                double f = 0;
                                for (long j = 0; j < vlen; j++) {
                                    double tmp = (double) ((DataObjectInterface) dta.get(i + j)).toFloat();
                                    f += tmp * tmp;
                                }
                                f = sqrt(f);
                                int bin = (int) ((f - min) / binSize);
                                if (bin < 0 || bin > nbin - 1) {
                                    continue;
                                }
                                valueHistogram[bin] += 1;
                            }
                        } //endfor
                    }
                } else if ((vlen == 1) && (currentMask == null)) {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        double f = dta.getDouble(i);
                        int bin = (int) ((f - min) / binSize);
                        if (bin < 0 || bin > nbin - 1) {
                            continue;
                        }
                        valueHistogram[bin] += 1;
                    } //endfor
                } else if ((vlen == 1) && (currentMask != null)) {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        if (currentMask.getBoolean(i) == true) {
                            double f = dta.getDouble(i);
                            int bin = (int) ((f - min) / binSize);
                            if (bin < 0 || bin > nbin - 1) {
                                continue;
                            }
                            valueHistogram[bin] += 1;
                        }
                    } //endfor
                } else if ((vlen > 1) && (currentMask == null)) {

                    for (long i = firstIdx; i < lastIdx; i += vlen) {
                        double f = 0;
                        for (long j = 0; j < vlen; j++) {
                            double tmp = dta.getDouble(i + j);
                            f += tmp * tmp;
                        }
                        f = sqrt(f);
                        int bin = (int) ((f - min) / binSize);
                        if (bin < 0 || bin > nbin - 1) {
                            continue;
                        }
                        valueHistogram[bin] += 1;
                    } //endfor
                } else { // (vlen > 1) && (currentMask != null)
                    for (long i = firstIdx; i < lastIdx; i += vlen) {
                        if (currentMask.getBoolean(i / vlen) == true) {
                            double f = 0;
                            for (long j = 0; j < vlen; j++) {
                                double tmp = dta.getDouble(i + j);
                                f += tmp * tmp;
                            }
                            f = sqrt(f);
                            int bin = (int) ((f - min) / binSize);
                            if (bin < 0 || bin > nbin - 1) {
                                continue;
                            }
                            valueHistogram[bin] += 1;
                        }
                    } //endfor
                }
                return valueHistogram;
            }

        }

        class HistogramCallableCoverOutside implements Callable<long[]>
        {

            private final LargeArray dta;
            private final LargeArray currentMask;
            private final long firstIdx;
            private final long lastIdx;

            HistogramCallableCoverOutside(final LargeArray dta, final LargeArray currentMask, final long firstIdx, final long lastIdx)
            {
                this.dta = dta;
                this.currentMask = currentMask;
                this.firstIdx = firstIdx;
                this.lastIdx = lastIdx;
            }

            @Override
            public long[] call() throws Exception
            {
                long[] valueHistogram = new long[nbin];
                if (dta.getType() == LargeArrayType.OBJECT) {
                    if ((vlen == 1) && (currentMask == null)) {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double f = (double) ((DataObjectInterface) dta.get(i)).toFloat();
                            int bin = (int) ((f - min) / binSize);
                            if (bin < 0) {
                                valueHistogram[0] += 1;
                            } else if (bin > nbin - 1) {
                                valueHistogram[nbin - 1] += 1;
                            } else {
                                valueHistogram[bin] += 1;
                            }
                        } //endfor
                    } else if ((vlen == 1) && (currentMask != null)) {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            if (currentMask.getBoolean(i) == true) {
                                double f = (double) ((DataObjectInterface) dta.get(i)).toFloat();
                                int bin = (int) ((f - min) / binSize);
                                if (bin < 0) {
                                    valueHistogram[0] += 1;
                                } else if (bin > nbin - 1) {
                                    valueHistogram[nbin - 1] += 1;
                                } else {
                                    valueHistogram[bin] += 1;
                                }
                            }
                        } //endfor
                    } else if ((vlen > 1) && (currentMask == null)) {
                        for (long i = firstIdx; i < lastIdx; i += vlen) {
                            double f = 0;
                            for (long j = 0; j < vlen; j++) {
                                double tmp = (double) ((DataObjectInterface) dta.get(i + j)).toFloat();
                                f += tmp * tmp;
                            }
                            f = sqrt(f);
                            int bin = (int) ((f - min) / binSize);
                            if (bin < 0) {
                                valueHistogram[0] += 1;
                            } else if (bin > nbin - 1) {
                                valueHistogram[nbin - 1] += 1;
                            } else {
                                valueHistogram[bin] += 1;
                            }
                        } //endfor
                    } else { //(vlen > 1) && (currentMask != null)
                        for (long i = firstIdx; i < lastIdx; i += vlen) {
                            if (currentMask.getBoolean(i / vlen) == true) {
                                double f = 0;
                                for (long j = 0; j < vlen; j++) {
                                    double tmp = (double) ((DataObjectInterface) dta.get(i + j)).toFloat();
                                    f += tmp * tmp;
                                }
                                f = sqrt(f);
                                int bin = (int) ((f - min) / binSize);
                                if (bin < 0) {
                                    valueHistogram[0] += 1;
                                } else if (bin > nbin - 1) {
                                    valueHistogram[nbin - 1] += 1;
                                } else {
                                    valueHistogram[bin] += 1;
                                }
                            }
                        } //endfor
                    }
                } else if ((vlen == 1) && (currentMask == null)) {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        double f = dta.getDouble(i);
                        int bin = (int) ((f - min) / binSize);
                        if (bin < 0) {
                            valueHistogram[0] += 1;
                        } else if (bin > nbin - 1) {
                            valueHistogram[nbin - 1] += 1;
                        } else {
                            valueHistogram[bin] += 1;
                        }
                    } //endfor
                } else if ((vlen == 1) && (currentMask != null)) {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        if (currentMask.getBoolean(i) == true) {
                            double f = dta.getDouble(i);
                            int bin = (int) ((f - min) / binSize);
                            if (bin < 0) {
                                valueHistogram[0] += 1;
                            } else if (bin > nbin - 1) {
                                valueHistogram[nbin - 1] += 1;
                            } else {
                                valueHistogram[bin] += 1;
                            }
                        }
                    } //endfor
                } else if ((vlen > 1) && (currentMask == null)) {
                    for (long i = firstIdx; i < lastIdx; i += vlen) {
                        double f = 0;
                        for (long j = 0; j < vlen; j++) {
                            double tmp = dta.getDouble(i + j);
                            f += tmp * tmp;
                        }
                        f = sqrt(f);
                        int bin = (int) ((f - min) / binSize);
                        if (bin < 0) {
                            valueHistogram[0] += 1;
                        } else if (bin > nbin - 1) {
                            valueHistogram[nbin - 1] += 1;
                        } else {
                            valueHistogram[bin] += 1;
                        }
                    } //endfor
                } else { //(vlen > 1) && (currentMask != null)
                    for (long i = firstIdx; i < lastIdx; i += vlen) {
                        if (currentMask.getBoolean(i / vlen) == true) {
                            double f = 0;
                            for (long j = 0; j < vlen; j++) {
                                double tmp = dta.getDouble(i + j);
                                f += tmp * tmp;
                            }
                            f = sqrt(f);
                            int bin = (int) ((f - min) / binSize);
                            if (bin < 0) {
                                valueHistogram[0] += 1;
                            } else if (bin > nbin - 1) {
                                valueHistogram[nbin - 1] += 1;
                            } else {
                                valueHistogram[bin] += 1;
                            }
                        }
                    } //endfor
                }
                return valueHistogram;
            }
        }

        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        if (computeGlobalHistogram) {
            ArrayList<LargeArray> vals = timeData.getValues();
            for (int step = 0; step < timeData.getNSteps(); step++) {
                final LargeArray dta = vals.get(step);
                final LargeArray currentMask;
                if (timeMask == null) {
                    currentMask = null;
                } else {
                    currentMask = timeMask.getValue(timeData.getTime(step));
                }
                long length = dta.length() / vlen;
                nthreads = (int) FastMath.min(nthreads, length);
                long k = length / nthreads;
                futures = new Future[nthreads];
                if (ignoreOutsideMinMax) {
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k * vlen;
                        final long lastIdx = (j == nthreads - 1) ? length * vlen : firstIdx + k * vlen;
                        futures[j] = ConcurrencyUtils.submit(new HistogramCallableIgnoreOutside(dta, currentMask, firstIdx, lastIdx));
                    }
                } else { //ignoreOutsideMinMax == false
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k * vlen;
                        final long lastIdx = (j == nthreads - 1) ? length * vlen : firstIdx + k * vlen;
                        futures[j] = ConcurrencyUtils.submit(new HistogramCallableCoverOutside(dta, currentMask, firstIdx, lastIdx));
                    }
                }

                try {
                    for (int j = 0; j < nthreads; j++) {
                        long[] res = (long[]) futures[j].get();
                        for (int i = 0; i < nbin; i++) {
                            valueHistogram[i] += res[i];
                        }
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        } else { // computeGlobalHistogram == false
            final LargeArray dta = da.getRawArray();

            final LargeArray currentMask;
            if (timeMask == null) {
                currentMask = null;
            } else {
                currentMask = timeMask.getValue(da.getCurrentTime());
            }
            long length = dta.length() / vlen;
            nthreads = (int) FastMath.min(nthreads, length);
            long k = length / nthreads;
            futures = new Future[nthreads];
            if (ignoreOutsideMinMax) {
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = j * k * vlen;
                    final long lastIdx = (j == nthreads - 1) ? length * vlen : firstIdx + k * vlen;
                    futures[j] = ConcurrencyUtils.submit(new HistogramCallableIgnoreOutside(dta, currentMask, firstIdx, lastIdx));
                }
            } else { //ignoreOutsideMinMax == false
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = j * k * vlen;
                    final long lastIdx = (j == nthreads - 1) ? length * vlen : firstIdx + k * vlen;
                    futures[j] = ConcurrencyUtils.submit(new HistogramCallableCoverOutside(dta, currentMask, firstIdx, lastIdx));
                }
            }
            try {
                for (int j = 0; j < nthreads; j++) {
                    long[] res = (long[]) futures[j].get();
                    for (int i = 0; i < nbin; i++) {
                        valueHistogram[i] += res[i];
                    }
                }
            } catch (InterruptedException | ExecutionException ex) {
                throw new IllegalStateException(ex);
            }
        }

        return valueHistogram;
    }

}
