/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic;

import java.io.Serializable;
import java.util.*;
import java.util.function.BiPredicate;
import org.visnow.jscic.cells.Tetra;
import org.visnow.jscic.cells.SimplexPosition;
import org.visnow.jscic.cells.Triangle;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.MatrixMath;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.utils.EngineeringFormattingUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.utils.ArrayUtils;
import org.visnow.jscic.utils.FloatingPointUtils;
import org.visnow.jscic.utils.VectorMath;

/**
 * Abstract field class. Currently implemented as RegularField and
 * IrregularField.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public abstract class Field extends DataContainer implements Serializable
{

    private static final long serialVersionUID = -1036972370819327784L;
    
    /**
     * Parameter controlling extent of field description
     * SHORT - suitable for tooltip text
     * CONCISE - suitable for module parameters window
     * EXTENDED for use in the "show content" window
     * DEBUG - full information for debug purposes
     */
    public enum DescriptionExtent{SHORT, CONCISE, EXTENDED, DEBUG};

    protected FieldType type;
    protected TimeData timeCoords = null;
    protected TimeData timeMask = null;

    protected FloatLargeArray coords = null;
    protected LogicLargeArray mask = null;
    protected long coordsTimestamp = 0;
    protected long maskTimestamp = 0;
    protected GeoTreeNode geoTree = null;
    protected float[][] cellExtents = null;
    protected String[] axesNames = {"x", "y", "z"};
    protected boolean statisticsComputed = false;
    protected FloatLargeArray normals = null;

    /**
     * trueNSpace &ge; 0 means that the field has cells of dimensions trueNSpace and
     * is contained in subspace spanned by the first trueNSpace coordinates, e.g
     * trueNSpace = 2 if and only if the field is contained in the x0,x1 plane and has triangle or quad cells.
     *
     */
    protected int trueNSpace = -1;

    @Override
    public DataArray addComponent(DataArray dataArray, int position)
    {
        DataArray res = super.addComponent(dataArray, position);
        statisticsComputed = false;
        return res;
    }

    @Override
    public DataArray addComponent(DataArray dataArray)
    {
        DataArray res = super.addComponent(dataArray);
        statisticsComputed = false;
        return res;
    }

    @Override
    public void setComponents(ArrayList<DataArray> data)
    {
        super.setComponents(data);
        statisticsComputed = false;
    }

    @Override
    public void setComponent(DataArray dataArray, int i)
    {
        super.setComponent(dataArray, i);
        statisticsComputed = false;
    }

    @Override
    public void setSchema(DataContainerSchema schema)
    {
        if (!(schema instanceof FieldSchema)) {
            throw new IllegalArgumentException("bad argument for Field.setSchema(schema): must be FieldSchema");
        }
        super.setSchema(schema);
        statisticsComputed = false;
    }

    @Override
    public boolean removeComponent(int i)
    {
        boolean res = super.removeComponent(i);
        statisticsComputed = false;
        return res;
    }

    @Override
    public boolean removeComponent(String name)
    {
        boolean res = super.removeComponent(name);
        statisticsComputed = false;
        return res;
    }

    @Override
    public void removeComponents()
    {
        super.removeComponents();
        statisticsComputed = false;
    }

    @Override
    public int hashCode(float quality)
    {
        int fprint = super.hashCode(quality);
        fprint = 37 * fprint + Objects.hashCode(this.type);
        fprint = 37 * fprint + (this.timeCoords != null ? this.timeCoords.hashCode(quality) : 0);
        fprint = 37 * fprint + (this.timeMask != null ? this.timeMask.hashCode(quality) : 0);
        fprint = 37 * fprint + Objects.hashCode(this.geoTree);
        fprint = 37 * fprint + Arrays.deepHashCode(this.cellExtents);
        fprint = 37 * fprint + Arrays.hashCode(this.axesNames);
        fprint = 37 * fprint + (this.statisticsComputed ? 1 : 0);
        fprint = 37 * fprint + (this.normals != null ? this.normals.hashCode(quality) : 0);
        fprint = 37 * fprint + this.trueNSpace;
        return fprint;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || !(o instanceof Field))
            return false;
        Field f = (Field) o;
        boolean equal = super.equals(o) && this.trueNSpace == f.trueNSpace && this.type == f.type && this.statisticsComputed == f.statisticsComputed;
        if (equal == false) {
            return false;
        }
        if (this.timeCoords != null && f.timeCoords != null) {
            if (!this.timeCoords.equals(f.timeCoords)) return false;
        } else if (this.timeCoords != f.timeCoords) {
            return false;
        }
        if (this.timeMask != null && f.timeMask != null) {
            if (!this.timeMask.equals(f.timeMask)) return false;
        } else if (this.timeMask != f.timeMask) {
            return false;
        }
        if (this.geoTree != null && f.geoTree != null) {
            if (!this.geoTree.equals(f.geoTree)) return false;
        } else if (this.geoTree != f.geoTree) {
            return false;
        }
        if (this.cellExtents != null && f.cellExtents != null) {
            if (!Arrays.deepEquals(this.cellExtents, f.cellExtents)) return false;
        } else if (this.cellExtents != f.cellExtents) {
            return false;
        }
        if (this.axesNames != null && f.axesNames != null) {
            if (!Arrays.equals(this.axesNames, f.axesNames)) return false;
        } else if (this.axesNames != f.axesNames) {
            return false;
        }
        if (this.normals != null && f.normals != null) {
            if (!this.normals.equals(f.normals)) return false;
        } else if (this.normals != f.normals) {
            return false;
        }
        return true;
    }

    /**
     * Returns the description of the Field.
     *
     * @return description of the Field
     */
    public abstract String description();

    /**
     * Returns the description of the Field.
     *
     * @param debug if true, then an extended description is returned
     *
     * @return description of the Field
     */
    public abstract String description(boolean debug);

    /**
     * Formats matrix as table of vectors. Each row of matrix is shown as 1
     * vector in horizontal form prefixed with x:, y: or z: If transpose is true
     * then the columns of matrix instead of rows are shown. If addSize is true,
     * each vector norm (vectoLength&gt;2) or elemet range (vectorLenght=2) is
     * also shown.
     *
     * @param matrix       input data to show in standard matrix format:
     *                     matrix[rowNumber][columnNumber]
     * @param coordsUnits  units of coordinates
     * @param dim          dimension
     * @param vectorLength vector length
     * @param transpose    if true, then columns of matrix instead of rows are
     *                     shown
     * @param addSize      if true, then for vectors of vectorLenght&gt;2 each norm
     *                     is shown, for vectors of vectorLenght=2 range between
     *                     elements is shown
     *
     * @return matrix formatted as XYZ table
     */
    protected static String asXYZTable(float[][] matrix, String[] coordsUnits, int dim, int vectorLength, boolean transpose, boolean addSize)
    {
        String[] axisLabels = new String[]{"x", "y", "z"};
        return asTable(matrix, coordsUnits, axisLabels, dim, vectorLength, transpose, addSize);
    }

    /**
     * Formats matrix as table of vectors. Each row of matrix is shown as 1
     * vector in horizontal form prefixed with given axisLabels If transpose is
     * true then columns of matrix instead of rows are shown. If addSize is true,
     * each vector norm (vectoLength&gt;2) or elemet range (vectorLenght=2) is
     * also shown.
     *
     * @param matrix       input data to show in standard matrix format:
     *                     matrix[rowNumber][columnNumber]
     * @param coordsUnits  units of coordinates
     * @param axisLabels   axis labels
     * @param dim          dimension
     * @param vectorLength vector length
     * @param transpose    if true, then columns of matrix instead of rows are
     *                     shown
     * @param addSize      if true, then for vectors of vectorLenght&gt;2 each norm
     *                     is shown, for vectors of vectorLenght=2 range between
     *                     elements is shown

     *
     * @return matrix formatted as table
     */
    protected static String asTable(float[][] matrix, String[] coordsUnits, String[] axisLabels, int dim, int vectorLength, boolean transpose, boolean addSize)
    {
        float[] vector = new float[vectorLength];

        StringBuilder s = new StringBuilder();

        s.append("<TABLE border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
        for (int row = 0; row < dim; row++) {
            s.append("<tr valign=\"bottom\">");
            s.append("<td align='left'>" + "&nbsp;&nbsp;").append(axisLabels[row]).append(":&nbsp;</td>");
            s.append("<td align='right'>[</td>");

            if (transpose) {
                for (int j = 0; j < vectorLength; j++) {
                    vector[j] = matrix[j][row];
                }
            } else {
                System.arraycopy(matrix[row], 0, vector, 0, vectorLength);
            }

            s.append("<td align='right'>").append(Arrays.toString(EngineeringFormattingUtils.formatInContextHtml(vector)).replaceAll(",", ",&nbsp;</td><td align='right'>").replaceAll("[\\[\\]]", "")).append("</td>");
            s.append("<td align='right'>" + "]" + "</td>");

            if (coordsUnits != null && !coordsUnits[row].isEmpty() && !coordsUnits[row].equals("1")) {
                s.append("<td align='left'>").append("&nbsp;").append(coordsUnits[row]).append("</td>");
            }
            if (addSize) {
                s.append("<td align='left'>&nbsp;&nbsp;&nbsp;");
                if(vectorLength == 2)
                    s.append("&lt;-&gt; = ").append(Arrays.toString(EngineeringFormattingUtils.formatInContextHtml(new double[]{Math.abs(vector[1]-vector[0])})).replaceAll("[\\[\\]]", ""));
                else if(vectorLength > 2)
                    s.append("|").append(axisLabels[row]).append("| = ").append(Arrays.toString(EngineeringFormattingUtils.formatInContextHtml(new double[]{VectorMath.vectorNorm(vector)})).replaceAll("[\\[\\]]", ""));
                if (coordsUnits != null && !coordsUnits[row].isEmpty() && !coordsUnits[row].equals("1"))
                    s.append("&nbsp;").append(coordsUnits[row]);
                s.append("</td>");
            }
            s.append("</tr>");
        }
        s.append("</TABLE>");

        return s.toString();
    }
    
    /**
     * Returns concise description of the Field.
     *
     * @return short description of the Field
     */
    abstract public String conciseDescription();

    /**
     * Returns the short description of the Field.
     *
     * @return short description of the Field
     */
    abstract public String shortDescription();

    @Override
    public FieldSchema getSchema()
    {
        return (FieldSchema)schema;
    }

    /**
     * Returns the type of a field.
     *
     * @return the type of a field
     */
    public FieldType getType()
    {
        return type;
    }

    @Override
    abstract public Field cloneShallow();

    @Override
    abstract public Field cloneDeep();

    /**
     * Returns true if a given field has a structure compatible with this field,
     * false otherwise.
     *
     * @param f input field
     *
     * @return true if a given field has a structure compatible with this field,
     *         false otherwise
     */
    abstract public boolean isStructureCompatibleWith(Field f);

    /**
     * Returns triangulated field.
     *
     * @return triangulated field
     */
    abstract public IrregularField getTriangulated();

    public String[] getUserData()
    {
        return schema.getUserData();
    }

    /**
     * Returns user data.
     *
     * @param userData user data
     */
    public void setUserData(String[] userData)
    {
        schema.setUserData(userData);
    }

    /**
     * Returns the number of time steps.
     *
     * @return number of time steps.
     */
    public int getNFrames()
    {
        int nFrames = 1;
        if (timeCoords != null) {
            nFrames = timeCoords.getNSteps();
        }
        if (timeMask != null && timeMask.getNSteps() > nFrames) {
            nFrames = timeMask.getNSteps();
        }
        for (DataArray dataArray : components) {
            if (dataArray.getNFrames() > nFrames) {
                nFrames = dataArray.getNFrames();
            }
        }
        return nFrames;
    }

    /**
     * Returns the number of nodes in a field.
     *
     * @return number of nodes in a field
     */
    public long getNNodes()
    {
        return getNElements();
    }

    /**
     * Returns the extents of a field.
     *
     * @return extents of a field
     */
    public float[][] getExtents()
    {
        return ArrayUtils.cloneDeep(((FieldSchema) schema).getExtents());
    }

    /**
     * Returns the preferred extents of a field.
     *
     * @return extents of a field
     */
    public float[][] getPreferredExtents()
    {
        return ArrayUtils.cloneDeep(((FieldSchema) schema).getPreferredExtents());
    }

    /**
     * Returns the diameter of a field.
     *
     * @return diameter of a field
     */
    public float getDiameter()
    {
        return ((FieldSchema) schema).getDiameter();
    }

    /**
     * Sets field preferred extents. Linear mapping coefficients between preferred extents and preferred physical extents remain unchanged.
     *
     * @param preferredExtents new value of the field preferred extents.
     */
    public void setPreferredExtents(float[][] preferredExtents)
    {
        ((FieldSchema) schema).setPreferredExtents(preferredExtents);
        timestamp = System.nanoTime();
    }

    /**
     * Sets preferred extents. Linear mapping coefficients between preferred extents and physical preferred extents are computed and stored instead of
     * physicalExtents array.
     *
     * @param preferredExtents         new preferred extents.
     * @param preferredPhysicalExtents new preferred physical extents
     */
    public void setPreferredExtents(float[][] preferredExtents, float[][] preferredPhysicalExtents)
    {
        ((FieldSchema) schema).setPreferredExtents(preferredExtents, preferredPhysicalExtents);
        timestamp = System.nanoTime();
    }

    /**
     * Returns the physical extents
     *
     * @return physical extents
     */
    public float[][] getPhysicalExtents()
    {
        return ((FieldSchema) schema).getPhysicalExtents();
    }

    /**
     * Returns a copy of coefficients for linear mapping of coordinates to physical values.
     *
     * @return a copy of coefficients for linear mapping of coordinates to physical values
     */
    public double[][] getPhysicalExtentsMappingCoefficients()
    {
        return ((FieldSchema) schema).getPhysicalExtentsMappingCoefficients();
    }

    /**
     * Returns the preferred physical extents
     *
     * @return physical extents
     */
    public float[][] getPreferredPhysicalExtents()
    {
        return ((FieldSchema) schema).getPreferredPhysicalExtents();
    }

    /**
     * Returns the indices of scalar components.
     *
     * @return indices of scalar components
     */
    public int[] getScalarComponentIndices()
    {
        int scalarComponentsCount = 0;
        for (int i = 0; i < components.size(); i++) {
            if (components.get(i).getVectorLength() == 1) {
                scalarComponentsCount++;
            }
        }
        if (scalarComponentsCount == 0) {
            return new int[0];
        }

        int[] out = new int[scalarComponentsCount];
        for (int i = 0, c = 0; i < components.size(); i++) {
            if (components.get(i).getVectorLength() == 1) {
                out[c] = i;
                c++;
            }
        }
        return out;
    }

    /**
     * Returns the indices of vector components.
     *
     * @return indices of vector components
     */
    public int[] getVectorComponentIndices()
    {
        int vectorComponentsCount = 0;
        for (int i = 0; i < components.size(); i++) {
            if (components.get(i).getVectorLength() != 1) {
                vectorComponentsCount++;
            }
        }
        if (vectorComponentsCount == 0) {
            return new int[0];
        }

        int[] out = new int[vectorComponentsCount];
        for (int i = 0, c = 0; i < components.size(); i++) {
            if (components.get(i).getVectorLength() != 1) {
                out[c] = i;
                c++;
            }
        }
        return out;
    }

    /**
     * Returns mean values for all components.
     *
     * @return mean values for all components
     */
    public double[] getMeanValues()
    {
        double[] meanVal = new double[getNComponents()];
        for (int n = 0; n < getNComponents(); n++) {
            meanVal[n] = getComponent(n).getMeanValue();
        }
        return meanVal;
    }

    /**
     * Returns mean squared values for all components.
     *
     * @return mean squared values for all components
     */
    public double[] getMeanSquaredValues()
    {
        double[] mean2Val = new double[getNComponents()];
        for (int n = 0; n < getNComponents(); n++) {
            mean2Val[n] = getComponent(n).getMeanSquaredValue();
        }
        return mean2Val;
    }

    /**
     * Returns standard deviation values for all components.
     *
     * @return standard deviation values for all components
     */
    public double[] getStandardDeviationValues()
    {
        double[] sd = new double[getNComponents()];
        for (int n = 0; n < getNComponents(); n++) {
            sd[n] = getComponent(n).getStandardDeviationValue();
        }
        return sd;
    }

    /**
     * Returns histograms for all components for current time moment.
     *
     * @return histograms for all components for current time moment
     */
    public long[][] getCurrentHistograms()
    {
        long[][] hist = new long[getNComponents()][];
        for (int n = 0; n < getNComponents(); n++) {
            hist[n] = getComponent(n).getCurrentHistogram();
        }
        return hist;
    }

    /**
     * Returns histograms for all components for all time steps.
     *
     * @return histograms for all components for all time steps
     */
    public long[][] getHistograms()
    {
        long[][] hist = new long[getNComponents()][];
        for (int n = 0; n < getNComponents(); n++) {
            hist[n] = getComponent(n).getHistogram();
        }
        return hist;
    }

    /**
     * Returns maximum values for all components.
     *
     * @return maximum values for all components
     */
    public double[] getMaxValues()
    {
        double[] maxVal = new double[getNComponents()];
        for (int n = 0; n < getNComponents(); n++) {
            maxVal[n] = getComponent(n).getMaxValue();
        }
        return maxVal;
    }

    /**
     * Returns minimum values for all components.
     *
     * @return minimum values for all components
     */
    public double[] getMinValues()
    {
        double[] minVal = new double[getNComponents()];
        for (int n = 0; n < getNComponents(); n++) {
            minVal[n] = getComponent(n).getMinValue();
        }
        return minVal;
    }

    /**
     * Returns preferred maximum values for all components.
     *
     * @return preferred maximum values for all components
     */
    public double[] getPreferredMaxValues()
    {
        double[] maxVal = new double[getNComponents()];
        for (int n = 0; n < getNComponents(); n++) {
            maxVal[n] = getComponent(n).getPreferredMaxValue();
        }
        return maxVal;
    }

    /**
     * Returns preferred minimum values for all components.
     *
     * @return preferred minimum values for all components
     */
    public double[] getPreferredMinValues()
    {
        double[] minVal = new double[getNComponents()];
        for (int n = 0; n < getNComponents(); n++) {
            minVal[n] = getComponent(n).getPreferredMinValue();
        }
        return minVal;
    }

    /**
     * Returns mask for the current time moment.
     *
     * @return mask for the current time moment
     */
    public LogicLargeArray getCurrentMask()
    {
        return getMask(this.currentTime);
    }

    /**
     * Returns mask for the given time moment.
     *
     * @param time time moment
     *
     * @return mask for the given time moment
     */
    public LogicLargeArray getMask(float time)
    {
        if (!hasMask()) {
            return null;
        }
        return (LogicLargeArray) this.timeMask.getValue(time);
    }

    /**
     * Returns mask for all time steps.
     *
     * @return mask for all time steps
     */
    public TimeData getMask()
    {
        if (!hasMask()) {
            return null;
        }
        return this.timeMask;
    }

    /**
     * Sets mask for the current time moment.
     *
     * @param mask new mask
     */
    public void setCurrentMask(LogicLargeArray mask)
    {
        setMask(mask, this.currentTime);
    }

    /**
     * Sets mask for the given time moment.
     *
     * @param mask new mask
     * @param time time moment
     */
    public void setMask(LogicLargeArray mask, float time)
    {
        if (mask == null) {
            removeMask(time);
        } else {
            if (mask.length() != getNNodes()) {
                throw new IllegalArgumentException("Invalid length of the mask parameter.");
            }
            this.statisticsComputed = false;
            if (this.timeMask == null) {
                this.timeMask = new TimeData(DataArrayType.FIELD_DATA_LOGIC);
            }
            this.timeMask.setValue(mask, time);
            if (this.currentTime == time) {
                this.mask = mask;
            }
            updateExtents();
            this.maskTimestamp = System.nanoTime();
            this.timestamp = System.nanoTime();
        }
    }

    /**
     * Adds the given mask at the end of time mask.
     *
     * @param mask new mask
     */
    public void addMask(LogicLargeArray mask)
    {
        if (mask == null) {
            throw new IllegalArgumentException("Mask cannot be null");
        } else {
            if (mask.length() != getNNodes()) {
                throw new IllegalArgumentException("Invalid length of the mask parameter.");
            }
            if (timeMask == null) {
                this.timeMask = new TimeData(DataArrayType.FIELD_DATA_LOGIC);
            }
            timeMask.addValue(mask);
            updateExtents();
            this.maskTimestamp = System.nanoTime();
            this.timestamp = System.nanoTime();
        }
    }

    /**
     * Removes mask for the given time step.
     *
     * @param time time step
     */
    public void removeMask(float time)
    {
        if (hasMask()) {
            this.statisticsComputed = false;
            this.timeMask.removeValue(time);
            if (this.currentTime == time) {
                this.mask = null;
            }
            updateExtents();
            this.maskTimestamp = System.nanoTime();
            this.timestamp = System.nanoTime();
        }
    }

    /**
     * Returns mask corresponding to a given moment if it is a time step.
     * Otherwise it creates a new times step at a given moment and returns an
     * empty mask.
     *
     * @param time time step
     *
     * @return mask for the given time step
     */
    public LogicLargeArray produceMask(float time)
    {
        LogicLargeArray res = (LogicLargeArray) timeMask.produceValue(time, getNElements());
        updateExtents();
        statisticsComputed = false;
        this.maskTimestamp = System.nanoTime();
        timestamp = System.nanoTime();
        return res;
    }

    /**
     * Sets mask for all time steps
     *
     * @param timeMask mask for all time steps
     */
    public void setMask(TimeData timeMask)
    {
        if (timeMask == null) {
            removeMask();
        } else {
            if (timeMask.getType() != DataArrayType.FIELD_DATA_LOGIC) {
                throw new IllegalArgumentException("timeMask.getType() != DataArrayType.FIELD_DATA_LOGIC");
            }
            if (timeMask.length() != getNNodes()) {
                throw new IllegalArgumentException("Invalid length of the mask parameter.");
            }
            statisticsComputed = false;
            this.timeMask = timeMask;
            this.timeMask.setCurrentTime(currentTime);
            this.mask = (LogicLargeArray) this.timeMask.getCurrentValue();
            updateExtents();
            maskTimestamp = System.nanoTime();
            timestamp = System.nanoTime();
        }
    }

    /**
     * Removes mask for all time steps.
     */
    public void removeMask()
    {
        this.timeMask = null;
        this.mask = null;
        statisticsComputed = false;
        updateExtents();
        maskTimestamp = System.nanoTime();
        timestamp = System.nanoTime();
    }

    /**
     * Returns true if a field has a mask, false otherwise.
     *
     * @return true if a field has a mask, false otherwise
     */
    public boolean hasMask()
    {
        return !(timeMask == null || timeMask.isEmpty());
    }

    /**
     * Interpolates a given data array in this field to the nodes of the mesh
     *
     * @param mesh mesh
     * @param da   data array
     *
     * @return interpolated data array
     */
    abstract public DataArray interpolateDataToMesh(Field mesh, DataArray da);

    /**
     * Returns coordinates for the current time step.
     *
     * @return coordinates for the current time step
     */
    public FloatLargeArray getCurrentCoords()
    {
        if (coords == null) {
            setCurrentTime(currentTime);
        }
        return coords;
    }

    /**
     * Returns coordinates for a given dimension.
     *
     * @param dim dimension (between 0 and 2)
     *
     * @return coordinates for a given dimension
     */
    public FloatLargeArray getCurrentCoords1D(int dim)
    {
        if (dim < 0 || dim > 2) {
            throw new IllegalArgumentException("dim < 0 || dim > 2");
        }
        FloatLargeArray currentCoords = getCurrentCoords();
        FloatLargeArray res = new FloatLargeArray(getNElements(), false);
        for (long j = 0; j < getNElements(); j++) {
            res.setFloat(j, currentCoords.getFloat(j * 3 + dim));
        }
        return res;
    }

    /**
     * Returns coordinates for the given time moment.
     *
     * @param time time moment
     *
     * @return coordinates for the given time moment
     */
    public FloatLargeArray getCoords(float time)
    {
        if (!hasCoords()) {
            return null;
        }
        return (FloatLargeArray) this.timeCoords.getValue(time);
    }

    /**
     * Returns coordinates for all time steps.
     *
     * @return coordinates for all time steps
     */
    public TimeData getCoords()
    {
        if (!hasCoords()) {
            return null;
        }
        return this.timeCoords;
    }

    /**
     * Returns coordinates for a given dimension.
     *
     * @param dim dimension (between 0 and 2)
     *
     * @return coordinates for a given dimension
     */
    public TimeData getCoords1D(int dim)
    {
        if (!hasCoords()) {
            return null;
        }
        if (dim < 0 || dim > 2) {
            throw new IllegalArgumentException("dim < 0 || dim > 2");
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) timeCoords.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = timeCoords.getValues();
        ArrayList<LargeArray> dataSeriesDim = new ArrayList(dataSeries.size());
        for (int i = 0; i < dataSeries.size(); i++) {
            LargeArray elem = dataSeries.get(i);
            LargeArray res = LargeArrayUtils.create(elem.getType(), getNElements(), false);
            for (long j = 0; j < getNElements(); j++) {
                res.setFloat(j, elem.getFloat(j * 3 + dim));
            }
            dataSeriesDim.add(res);
        }
        return new TimeData(timeSeries, dataSeriesDim, currentTime);
    }

    /**
     * Sets coordinates for the current time step.
     *
     * @param coords new coordinates
     */
    public void setCurrentCoords(FloatLargeArray coords)
    {
        setCoords(coords, this.currentTime);
    }

    /**
     * Set coordinates for the given time moment.
     *
     * @param c    new coordinates
     * @param time time moment
     */
    public void setCoords(FloatLargeArray c, float time)
    {
        if (c == null) {
            removeCoords(time);
        } else {
            if (c.length() != 3 * getNElements()) {
                throw new IllegalArgumentException("coords.length() != 3 * nElements");
            }
            if (timeCoords == null) {
                this.timeCoords = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
            }
            timeCoords.setValue(c, time);
            if (this.currentTime == time) {
                this.coords = c;
            }
            updateExtents();
            this.timestamp = this.coordsTimestamp = System.nanoTime();
        }
        checkTrueNSpace();
    }

    /**
     * Adds the given coordinates at the end of time coordinates.
     *
     * @param c new coordinates
     */
    public void addCoords(FloatLargeArray c)
    {
        if (c == null) {
            throw new IllegalArgumentException("coordinates cannot be null");
        } else {
            if (c.length() != 3 * getNElements()) {
                throw new IllegalArgumentException("coords.length() != 3 * nElements");
            }
            if (timeCoords == null) {
                this.timeCoords = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
            }
            timeCoords.addValue(c);
            updateExtents();
            this.timestamp = this.coordsTimestamp = System.nanoTime();
        }
    }

    /**
     * Returns coordinates corresponding to a given moment if it is a time step.
     * Otherwise it creates a new times step at a given moment and returns an
     * empty coordinates (all zeros)
     *
     * @param time time step
     *
     * @return coordinates for the given time step
     */
    public FloatLargeArray produceCoords(float time)
    {
        FloatLargeArray res = (FloatLargeArray) timeCoords.produceValue(time, 3 * getNElements());
        this.timestamp = this.coordsTimestamp = System.nanoTime();
        return res;
    }

    /**
     * Removes coordinates for the given time step.
     *
     * @param time time step
     */
    public void removeCoords(float time)
    {
        if (hasCoords()) {
            this.timeCoords.removeValue(time);
            if (this.currentTime == time) {
                this.coords = null;
            }
            updateExtents();
            this.timestamp = this.coordsTimestamp = System.nanoTime();
        }
    }

    /**
     * Sets coordinates for all time steps.
     *
     * @param timeCoords time coordinates
     */
    public void setCoords(TimeData timeCoords)
    {
        this.timeCoords = timeCoords;
        if (timeCoords == null) {
            removeCoords();
        } else {
            if (timeCoords.getType() != DataArrayType.FIELD_DATA_FLOAT) {
                throw new IllegalArgumentException("timeCoords.getType() != DataArrayType.FIELD_DATA_FLOAT");
            }
            if (timeCoords.length() != 3 * getNNodes()) {
                throw new IllegalArgumentException("Invalid length of the coordinates parameter.");
            }
            this.timeCoords = timeCoords;
            this.timeCoords.setCurrentTime(this.currentTime);
            this.coords = (FloatLargeArray) timeCoords.getCurrentValue();
            updateExtents();
        }
        checkTrueNSpace();
        this.timestamp = this.coordsTimestamp = System.nanoTime();
    }

    /**
     * Removes coordinates for all time steps.
     */
    public void removeCoords()
    {
        this.timeCoords = null;
        this.coords = null;
        updateExtents();
        this.timestamp = this.coordsTimestamp = System.nanoTime();
    }

    /**
     * Returns true if a field has coordinates, false otherwise.
     *
     * @return true if a field has coordinates, false otherwise
     */
    public boolean hasCoords()
    {
        return timeCoords != null && !timeCoords.isEmpty();
    }

    @Override
    public void setCurrentTime(float currentTime)
    {
        super.setCurrentTime(currentTime);
        if (timeCoords != null && !timeCoords.isEmpty()) {
            timeCoords.setCurrentTime(currentTime);
            coords = (FloatLargeArray) timeCoords.getCurrentValue();
            timestamp = coordsTimestamp = System.nanoTime();
        }
        if (timeMask != null && !timeMask.isEmpty()) {
            timeMask.setCurrentTime(currentTime);
            mask = (LogicLargeArray) timeMask.getCurrentValue();
            timestamp = maskTimestamp = System.nanoTime();
        }
    }

    /**
     * Returns trajectory of a given node.
     *
     * @param node node index
     *
     * @return trajectory of a given node
     */
    public FloatLargeArray getTrajectory(long node)
    {
        if (!hasCoords()) {
            return null;
        }
        int nFrames = timeCoords.getNSteps();
        FloatLargeArray tr = new FloatLargeArray((long) nFrames * (long) 3, false);
        for (int i = 0; i < nFrames; i++) {
            for (int k = 0; k < 3; k++) {
                tr.setFloat(3 * i + k, timeCoords.getValues().get(i).getFloat(3 * node + k));
            }
        }
        return tr;
    }

    /**
     * Recomputes field extents.
     */
    private void updateExtents()
    {
        updateExtents(false);
    }

    /**
     * Recomputes field extents.
     *
     * @param ignoreMask if true, then the mask is ignored
     */
    private void updateExtents(boolean ignoreMask)
    {
        if (timeCoords == null || timeCoords.isEmpty()) {
            return;
        }
        float[][] extents = new float[2][3];

        for (int i = 0; i < 3; i++) {
            extents[0][i] = FloatingPointUtils.MAX_NUMBER_FLOAT;
            extents[1][i] = FloatingPointUtils.MIN_NUMBER_FLOAT;
        }

        float f;
        int nValid = 0;
        for (int k = 0; k < timeCoords.getNSteps(); k++) {
            LogicLargeArray currentMask = null;
            if (!ignoreMask && timeMask != null) {
                currentMask = (LogicLargeArray) timeMask.getValue(timeCoords.getTime(k));
            }
            FloatLargeArray c = (FloatLargeArray) timeCoords.getValues().get(k);
            for (long i = 0; i < getNElements(); i++) {
                if (currentMask != null && !currentMask.getBoolean(i)) {
                    continue; //skip invalid nodes
                }
                nValid += 1;
                for (int j = 0; j < 3; j++) {
                    f = c.getFloat(i * 3 + j);
                    if (extents[0][j] > f) {
                        extents[0][j] = f;
                    }
                    if (extents[1][j] < f) {
                        extents[1][j] = f;
                    }
                }
            }
        }
        if (nValid == 0) {
            for (int i = 0; i < 3; i++) {
                extents[1][i] = 1;
                extents[0][i] = -1;
            }
        }

        switch (extents[0].length) {
            case 3:
                if (extents[0][0] == extents[1][0] &&
                    extents[0][1] == extents[1][1] &&
                    extents[0][2] == extents[1][2]) { // preferredMinValue == preferredMaxValue
                    extents[0][0] -= 0.5f;
                    extents[1][0] += 0.5f;
                    extents[0][1] -= 0.5f;
                    extents[1][1] += 0.5f;
                    extents[0][2] -= 0.5f;
                    extents[1][2] += 0.5f;
                }
                break;
            case 2:
                if (extents[0][0] == extents[1][0] &&
                    extents[0][1] == extents[1][1]) { // preferredMinValue == preferredMaxValue
                    extents[0][0] -= 0.5f;
                    extents[1][0] += 0.5f;
                    extents[0][1] -= 0.5f;
                    extents[1][1] += 0.5f;
                }
                break;
            case 1:
                if (extents[0][0] == extents[1][0]) { // preferredMinValue == preferredMaxValue
                    extents[0][0] -= 0.5f;
                    extents[1][0] += 0.5f;
                }
                break;

        }

        ((FieldSchema) schema).setExtents(extents);
        timestamp = System.nanoTime();
    }

    /**
     * Recomputes field preferredExtents.
     */
    public void updatePreferredExtents()
    {
        updatePreferredExtents(false);
    }

    /**
     * Recomputes field preferredExtents.
     *
     * @param ignoreMask if true, then the mask is ignored
     */
    public void updatePreferredExtents(boolean ignoreMask)
    {
        if (timeCoords == null || timeCoords.isEmpty()) {
            return;
        }
        float[][] preferredExtents = new float[2][3];

        for (int i = 0; i < 3; i++) {
            preferredExtents[0][i] = FloatingPointUtils.MAX_NUMBER_FLOAT;
            preferredExtents[1][i] = FloatingPointUtils.MIN_NUMBER_FLOAT;
        }

        float f;
        int nValid = 0;
        for (int k = 0; k < timeCoords.getNSteps(); k++) {
            LogicLargeArray currentMask = null;
            if (!ignoreMask && timeMask != null) {
                currentMask = (LogicLargeArray) timeMask.getValue(timeCoords.getTime(k));
            }
            FloatLargeArray c = (FloatLargeArray) timeCoords.getValues().get(k);
            for (long i = 0; i < getNElements(); i++) {
                if (currentMask != null && !currentMask.getBoolean(i)) {
                    continue; //skip invalid nodes
                }
                nValid += 1;
                for (int j = 0; j < 3; j++) {
                    f = c.getFloat(i * 3 + j);
                    if (preferredExtents[0][j] > f) {
                        preferredExtents[0][j] = f;
                    }
                    if (preferredExtents[1][j] < f) {
                        preferredExtents[1][j] = f;
                    }
                }
            }
        }
        if (nValid == 0) {
            for (int i = 0; i < 3; i++) {
                preferredExtents[1][i] = 1;
                preferredExtents[0][i] = -1;
            }
        }

        switch (preferredExtents[0].length) {
            case 3:
                if (preferredExtents[0][0] == preferredExtents[1][0] &&
                    preferredExtents[0][1] == preferredExtents[1][1] &&
                    preferredExtents[0][2] == preferredExtents[1][2]) { // preferredMinValue == preferredMaxValue
                    preferredExtents[0][0] -= 0.5f;
                    preferredExtents[1][0] += 0.5f;
                    preferredExtents[0][1] -= 0.5f;
                    preferredExtents[1][1] += 0.5f;
                    preferredExtents[0][2] -= 0.5f;
                    preferredExtents[1][2] += 0.5f;
                }
                break;
            case 2:
                if (preferredExtents[0][0] == preferredExtents[1][0] &&
                    preferredExtents[0][1] == preferredExtents[1][1]) { // preferredMinValue == preferredMaxValue
                    preferredExtents[0][0] -= 0.5f;
                    preferredExtents[1][0] += 0.5f;
                    preferredExtents[0][1] -= 0.5f;
                    preferredExtents[1][1] += 0.5f;
                }
                break;
            case 1:
                if (preferredExtents[0][0] == preferredExtents[1][0]) { // preferredMinValue == preferredMaxValue
                    preferredExtents[0][0] -= 0.5f;
                    preferredExtents[1][0] += 0.5f;
                }
                break;

        }

        setPreferredExtents(preferredExtents);
        for (int i = 0; i < trueNSpace; i++) {
            schema.addPseudoComponentSchema(new DataArraySchema(((FieldSchema) schema).COORD_NAMES[i], "", new HashMap <String, String[]>(), 
                                                                DataArrayType.FIELD_DATA_FLOAT, 1, 1, false,
                                                                preferredExtents[0][i], preferredExtents[1][i],
                                                                ((FieldSchema) schema).getPreferredExtents()[0][i],
                                                                ((FieldSchema) schema).getPreferredExtents()[1][i],
                                                                ((FieldSchema) schema).getPreferredPhysicalExtents()[0][i],
                                                                ((FieldSchema) schema).getPreferredPhysicalExtents()[1][i],
                                                                FloatingPointUtils.MAX_NUMBER_FLOAT, FloatingPointUtils.MAX_NUMBER_FLOAT, FloatingPointUtils.MAX_NUMBER_FLOAT, null, false));
        }
        for (int i = 0; i < trueNSpace; i++) {
            schema.addPseudoComponentSchema(new DataArraySchema(FieldSchema.COORD_NAMES[i], "", new HashMap <String, String[]>(),
                                                                DataArrayType.FIELD_DATA_FLOAT, 1, 1, false,
                                                                preferredExtents[0][i], preferredExtents[1][i],
                                                                ((FieldSchema) schema).getPreferredExtents()[0][i],
                                                                ((FieldSchema) schema).getPreferredExtents()[1][i],
                                                                ((FieldSchema) schema).getPreferredPhysicalExtents()[0][i],
                                                                ((FieldSchema) schema).getPreferredPhysicalExtents()[1][i],
                                                                (preferredExtents[0][i] + preferredExtents[1][i]) / 2.,
                                                                (preferredExtents[0][i] + preferredExtents[1][i]) / 2.,
                                                                (preferredExtents[0][i] + preferredExtents[1][i]) / 2., null, false));
        }
        timestamp = System.nanoTime();
    }

    /**
     * Returns node normals of this field
     *
     * @return normals
     */
    public FloatLargeArray getNormals()
    {
        return normals;
    }

    /**
     * Sets node normals for this filed
     *
     * @param normals new normals
     */
    public void setNormals(FloatLargeArray normals)
    {
        this.normals = normals;
        timestamp = System.nanoTime();
    }

    /**
     * Compares two Fields. Returns true if all components are compatible in
     * this field and the input field, false otherwise.
     *
     * @param	f field to be compared
     *
     * @return	true if all components are compatible in both fields, false
     *         otherwise
     */
    public boolean isDataCompatibleWith(Field f)
    {
        if (f == null) 
            return false;
        return schema.isCompatibleWith(f.getSchema());
    }

     /** Compares two Fields.
     * Returns true if user defined attributes are compatible in this field and in the input field, false otherwise.
     *
     * @param	f field to be compared
     * @param   compat a lambda specifying data component equivalence, e.g. 
     * <code>(u, v) -&gt; (u.isNumeric == v.isNumeric) &amp;&amp; (u.getVectorLength() == v.getVectorLength())</code>
     *
     * @return	true if all components are compatible in both schemes, false otherwise.
     */
    public boolean isDataCompatibleWith(Field f, BiPredicate<DataArraySchema, DataArraySchema> compat)
    {
        return schema.isCompatibleWith(f.schema, compat);
    }
    
    /**
     * Compares two Fields. Returns true if all components are compatible in
     * this field and the input field, false otherwise. Component ranges are
     * also compared.
     *
     * @param	f field to be compared
     *
     * @return	true if all components are compatible in both fields, false
     *         otherwise
     */
    public boolean isFullyCompatibleWith(Field f)
    {
        if (f == null) {
            return false;
        }
        return schema.isCompatibleWith(f.getSchema(), true, true);
    }

    /**
     * Compares two field schemes. Returns true if all components are compatible
     * in the schema for this field and the input schema, false otherwise.
     *
     * @param	s field schema to be compared
     *
     * @return	true if all components are compatible in both fields, false
     *         otherwise
     */
    public boolean isDataCompatibleWith(FieldSchema s)
    {
        return schema.isCompatibleWith(s);
    }

    /**
     * Updates the time stamp for coordinates to the current time.
     */
    public void updateCoordsTimestamp()
    {
        coordsTimestamp = System.nanoTime();
    }

    /**
     * Returns true if coordinates have changed since a given time stamp, false
     * otherwise.
     *
     * @param timestamp time stamp.
     *
     * @return true if coordinates have changed since a given time stamp, false
     *         otherwise
     */
    public boolean coordsChangedSince(long timestamp)
    {
        return this.coordsTimestamp > timestamp;
    }

    /**
     * Returns the time stamp for coordinates.
     *
     * @return time stamp for coordinates
     */
    public long getCoordsTimestamp()
    {
        return this.coordsTimestamp;
    }

    /**
     * Updates the time stamp for mask to the current time.
     */
    public void updateMaskTimestamp()
    {
        maskTimestamp = System.nanoTime();
    }

    /**
     * Returns true if coordinates have changed since a given time stamp, false
     * otherwise.
     *
     * @param timestamp time stamp.
     *
     * @return true if coordinates have changed since a given time stamp, false
     *         otherwise
     */
    public boolean maskChangedSince(long timestamp)
    {
        return this.maskTimestamp > timestamp;
    }

    /**
     * Returns the time stamp for mask.
     *
     * @return time stamp for mask
     */
    public long getMaskTimestamp()
    {
        return this.maskTimestamp;
    }

    /**
     * Returns binary tree of cells in this field.
     *
     * @return binary tree of cells
     */
    public GeoTreeNode getGeoTree()
    {
        return geoTree;
    }

    /**
     * Creates binary tree of cells in this field.
     */
    abstract public void createGeoTree();

    /**
     * Returns position of a given point in this field.
     *
     * @param p point coordinates
     *
     * @return position of a given point in this field
     */
    abstract public SimplexPosition getFieldCoords(float[] p);

    /**
     * Computes position of a given point in this field.
     *
     * @param p      point coordinates
     * @param result position of the point in this field
     *
     * @return true, if a point is within the field, false otherwise
     */
    abstract public boolean getFieldCoords(float[] p, SimplexPosition result);

    @Override
    public String toString()
    {
        final String TAB = "    ";
        return "Field ( " + super.toString() + TAB + "data=" + this.components + TAB + "nNodes=" + this.getNElements() + " )";
    }

    /**
     * Returns multiline string representation of this field.
     *
     * @return multiline string representation
     */
    abstract public String toMultilineString();

    /**
     * Returns the names of the axes.
     *
     * @return names of the axes
     */
    public String[] getAxesNames()
    {
        return axesNames;
    }

    /**
     * Sets the names of the axes.
     *
     * @param axesNames new names of the axes
     */
    public void setAxesNames(String[] axesNames)
    {
        if (axesNames != null && axesNames.length != 3) {
            this.axesNames = null;
        } else {
            this.axesNames = axesNames;
        }
        timestamp = System.nanoTime();
    }

    /**
     * Returns barycentric coordinates of a given point in a given tetrahedron
     * for current coordinates.
     *
     * @param tet tetrahedon
     * @param p   point coordinates
     *
     * @return array of barycentric coordinates
     */
    float[] getBarycentricCoords(Tetra tet, float[] p)
    {
        if (tet == null || timeCoords == null || timeCoords.isEmpty()) {
            return null;
        }
        //throw new IllegalArgumentException("tet == null || timeCoords == null || timeCoords.isEmpty()"); to nie jest błąd to ma tak działać
        FloatLargeArray c = (FloatLargeArray) timeCoords.getValue(currentTime);
        int[] verts = tet.getVertices();
        long l = 3 * (long) verts[0];
        float[][] A = new float[3][3];
        float[] v0 = new float[3];
        float[] b = new float[3];
        for (int i = 0; i < 3; i++) {
            v0[i] = c.getFloat(l + i);
        }
        for (int i = 0; i < 3; i++) {
            b[i] = p[i] - v0[i];
            for (int j = 0; j < 3; j++) {
                A[i][j] = c.getFloat(3 * (long) verts[j + 1] + i) - v0[i];
            }
        }
        float[] x;
        try {
            x = MatrixMath.lsolve(A, b);
        } catch (IllegalArgumentException ex) {
            return null;
        }
        if (x == null || x[0] < 0 || x[1] < 0 || x[2] < 0 || x[0] + x[1] + x[2] > 1) {
            return null;
        }

        float[] res = new float[4];
        System.arraycopy(x, 0, res, 1, 3);
        res[0] = 1 - (x[0] + x[1] + x[2]);
        return res;
    }

    /**
     * Returns barycentric coordinates of a given point in a given triangle in
     * XY-plane for current coordinates.
     *
     * @param triangle triangle
     * @param p        point coordinates
     *
     * @return array of barycentric coordinates
     */
    protected float[] getBarycentricCoords(Triangle triangle, float[] p)
    {
        if (triangle == null || timeCoords == null || timeCoords.isEmpty()) {
            return null;
        }
        //throw new IllegalArgumentException("tet == null || timeCoords == null || timeCoords.isEmpty()"); to nie jest błąd to ma tak działać
        FloatLargeArray c = (FloatLargeArray) timeCoords.getValue(currentTime);
        int[] verts = triangle.getVertices();
        long l = 3 * (long) verts[0];
        float[][] A = new float[2][2];
        float[] v0 = new float[2];
        float[] b = new float[2];
        for (int i = 0; i < 2; i++) {
            v0[i] = c.getFloat(l + i);
        }
        for (int i = 0; i < 2; i++) {
            b[i] = p[i] - v0[i];
            for (int j = 0; j < 2; j++) {
                A[i][j] = c.getFloat(3 * (long) verts[j + 1] + i) - v0[i];
            }
        }
        float[] x;
        try {
            x = MatrixMath.lsolve(A, b);
        } catch (IllegalArgumentException ex) {
            return null;
        }
        if (x == null || x[0] < 0 || x[1] < 0 || x[0] + x[1] > 1) {
            return null;
        }
        float[] res = new float[3];
        System.arraycopy(x, 0, res, 1, 2);
        res[0] = 1 - (x[0] + x[1]);
        return res;
    }

    /**
     * Returns the smallest time step among coordinates, mask and data.
     *
     * @return the smallest time step
     */
    public float getStartTime()
    {
        float t = FloatingPointUtils.MAX_NUMBER_FLOAT;
        if (timeCoords != null && !timeCoords.isEmpty() && timeCoords.getStartTime() < t) {
            t = timeCoords.getStartTime();
        }
        if (timeMask != null && !timeMask.isEmpty() && timeMask.getStartTime() < t) {
            t = timeMask.getStartTime();
        }
        for (DataArray dataArray : components) {
            if (dataArray.getStartTime() < t) {
                t = dataArray.getStartTime();
            }
        }
        return t;
    }

    /**
     * Returns the largest time step among coordinates, mask and data.
     *
     * @return the largest time step
     */
    public float getEndTime()
    {
        float t = FloatingPointUtils.MIN_NUMBER_FLOAT;
        if (timeCoords != null && !timeCoords.isEmpty() && timeCoords.getEndTime() > t) {
            t = timeCoords.getEndTime();
        }
        if (timeMask != null && !timeMask.isEmpty() && timeMask.getEndTime() > t) {
            t = timeMask.getEndTime();
        }
        for (DataArray dataArray : components) {
            if (dataArray.getEndTime() > t) {
                t = dataArray.getEndTime();
            }
        }
        return t;
    }

    /**
     * Returns true if a given times step exists in time coordinates, false
     * otherwise.
     *
     * @param t time step
     *
     * @return true if a given times step exists in time coordinates, false
     *         otherwise
     */
    public boolean isCoordTimestep(float t)
    {
        return timeCoords != null && !timeCoords.isEmpty() && timeCoords.isTimestep(t);
    }

    /**
     * Returns true if a given times step exists in time mask, false otherwise.
     *
     * @param t time step
     *
     * @return true if a given times step exists in time mask, false otherwise
     */
    public boolean isMaskTimestep(float t)
    {
        return timeMask != null && !timeMask.isEmpty() && timeMask.isTimestep(t);
    }

    /**
     * Get all times steps from coordinates, mask and data.
     *
     * @return all times steps from coordinates, mask and data
     */
    @Override
    public float[] getTimesteps()
    {
        Set<Float> tSteps = new HashSet<>();
        if (timeCoords != null && !timeCoords.isEmpty()) {
            for (Float t : timeCoords.getTimesAsList()) {
                tSteps.add(t);
            }
        }
        if (timeMask != null && !timeMask.isEmpty()) {
            for (Float t : timeMask.getTimesAsList()) {
                tSteps.add(t);
            }
        }
        for (DataArray da : components) {
            for (Float t : da.getTimeSeries()) {
                tSteps.add(t);
            }
        }
        float[] tStep = new float[tSteps.size()];
        int i = 0;
        for (Float t : tSteps) {
            tStep[i] = t;
            i += 1;
        }
        Arrays.sort(tStep);
        return tStep;
    }

    /**
     * Returns true if coordinates or mask or data are time dependent, false
     * otherwise
     *
     * @return true if coordinates or mask or data are time dependent, false
     *         otherwise
     */
    @Override
    public boolean isTimeDependant()
    {
        if (timeCoords != null && timeCoords.getNSteps() > 1) {
            return true;
        }
        if (timeMask != null && timeMask.getNSteps() > 1) {
            return true;
        }
        for (DataArray da : components) {
            if (da.isTimeDependant()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if this field has a time unit, false otherwise.
     *
     * @return true if this field has a time unit, false otherwise
     */
    public boolean isTimeUnitless()
    {
        String tunit = ((FieldSchema) schema).getTimeUnit();
        return tunit == null || tunit.isEmpty() || tunit.equals("1");
    }

    /**
     * Returns a time unit.
     *
     * @return time unit
     */
    public String getTimeUnit()
    {
        return ((FieldSchema) schema).getTimeUnit();
    }

    /**
     * Sets time unit.
     *
     * @param timeUnit new time unit
     */
    public void setTimeUnit(String timeUnit)
    {
        ((FieldSchema) schema).setTimeUnit(timeUnit);
        timestamp = System.nanoTime();
    }

    /**
     * Returns true if this field has units of coordinates, false otherwise.
     *
     * @return true if this field has units of coordinates, false otherwise
     */
    public boolean isCoordsUnitless()
    {
        String[] cunit = ((FieldSchema) schema).getCoordsUnits();
        if (cunit == null) {
            return true;
        }
        boolean res = true;
        for (int i = 0; i < 3; i++) {
            res = res && (cunit[i].isEmpty() || cunit[i].equals("1"));
        }
        return res;
    }

    /**
     * Returns true if the units of all coordinates are equal, false otherwise.
     *
     * @return true if this field has units of coordinates, false otherwise
     */
    public boolean hasCoordsUnitsEqual()
    {
        String[] cunit = ((FieldSchema) schema).getCoordsUnits();
        if (cunit == null) {
            return true;
        }
        return cunit[0].equals(cunit[1]) && cunit[0].equals(cunit[2]);
    }

    /**
     * Returns the unit of coordinates for a given dimension.
     *
     * @param dim dimension (0, 1, or 2)
     *
     * @return the unit of coordinates for a given dimension
     */
    public String getCoordsUnit(int dim)
    {
        return ((FieldSchema) schema).getCoordsUnit(dim);
    }

    /**
     * Returns units of coordinates.
     *
     * @return units of coordinates
     */
    public String[] getCoordsUnits()
    {
        return ((FieldSchema) schema).getCoordsUnits();
    }

    /**
     * Sets units of coordinates.
     *
     * @param coordsUnits new units of coordinates
     */
    public void setCoordsUnits(String[] coordsUnits)
    {
        ((FieldSchema) schema).setCoordsUnits(coordsUnits);
        timestamp = System.nanoTime();
    }

    /**
     * Sets the unit of coordinates.
     *
     * @param coordsUnit new unit of coordinates
     */
    public void setCoordsUnit(String coordsUnit)
    {
        ((FieldSchema) schema).setCoordsUnit(coordsUnit);
        timestamp = System.nanoTime();
    }

    /**
     * Returns 3 if the field has 3D cells, 2 if the field has 2D cells and is
     * contained in XY-plane, 1 if the field has 1D cells and is contained in X
     * axis, -1 otherwise.
     *
     * @return -1, 1, 2, or 3
     */
    public int getTrueNSpace()
    {
        return trueNSpace;
    }

    /**
     * Returns true if this field has at least one numeric component, false
     * otherwise.
     *
     * @return true if this field has at least one numeric component, false
     *         otherwise
     */
    public boolean hasNumericComponent()
    {
        for (DataArray dataArray : components) {
            if (dataArray.isNumeric()) {
                return true;
            }
        }
        return false;
    }


    /**
     * Returns true if this field has at least one proper vector component
     * (vectorLength == trueNSpace), false otherwise.
     *
     * @return true if this field has at least one proper vector component
     *         (vectorLength == trueNSpace), false otherwise
     */
    public boolean hasProperVectorComponent()
    {
        checkTrueNSpace();
        for (DataArray dataArray : components) {
            if (dataArray.isNumeric() && dataArray.getVectorLength() == trueNSpace) {
                return true;
            }
        }
        return false;
    }

    /**
     * Computes true nspace variable.
     */
    abstract public void checkTrueNSpace();

    /**
     * Returns node indices of this field.
     *
     * @param axis specification: 0, 1, or 2
     *
     * @return node indices
     */
    public abstract LongLargeArray getIndices(int axis);

    /**
     * Returns true if getNNodes() &gt; LargeArray.getMaxSizeOf32bitArray(),
     * false otherwise.
     *
     * @return true if getNNodes() &gt; LargeArray.getMaxSizeOf32bitArray(),
     *         false otherwise
     */
    public boolean isLarge()
    {
        return getNElements() > LargeArray.getMaxSizeOf32bitArray();
    }

    /**
     * Tests if this Field is contained in a plane orthogonal to one of the x/y/z axes
     * and returns the axis index representing plane orientation.
     *
     * @return axis index;
     * 2 for the plane parallel to the XY plane,
     * 1 for the plane parallel to the XZ plane,
     * 0 for the plane parallel to the YZ plane.
     * If no such plane exists, return value is -1
     */
    public int getPlaneOrientationAxis() {
        double eps = this.getDiameter() / 10000;
        boolean[] isOrtho = new boolean[]{true, true, true};
        float[] p0 = new float[3];
        LargeArrayUtils.arraycopy(coords, 0, p0, 0, 3);
        for (long i = 0; i < coords.length(); i += 3) {
            for (int j = 0; j < 3; j++)
                if (Math.abs(coords.getFloat(i + j) - p0[j]) > eps) {
                    isOrtho[j] = false;
                }
            if (!(isOrtho[0] || isOrtho[1] || isOrtho[2]))
                return -1;
        }
        if(isOrtho[0])
            return 0;
        if(isOrtho[1])
            return 1;
        if(isOrtho[2])
            return 2;
        return -1;
    }

}
