/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic;

import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArray;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.BiPredicate;
import org.visnow.jscic.utils.StringUtils;

/**
 * Holds general information about component schemes.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class DataContainerSchema implements java.io.Serializable
{

    private static final long serialVersionUID = -5679350979428063767L;

    /**
     * Number of elements if each component.
     */
    protected long nElements = -1;

    /**
     * Name of the container.
     */
    protected String name;

    /**
     * User specified data.
     */
    private String[] userData;

    /**
     * A list holding schemes of all data components in the field.
     */
    protected ArrayList<DataArraySchema> componentSchemas = new ArrayList<>();

    /**
     * A list holding schemes of pseudo-components (indices, coordinates, mask etc.) in the field.
     */
    protected ArrayList<DataArraySchema> pseudoComponentSchemas = new ArrayList<>();

    /**
     * Creates a new instance of DataContainerSchema.
     *
     * @param name name of the schema
     */
    public DataContainerSchema(String name)
    {
        if (name == null || name.isEmpty()) throw new IllegalArgumentException("Data container name cannot be empty");
        this.name = StringUtils.canonicalizeName(name);
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || !(o instanceof DataContainerSchema))
            return false;
        DataContainerSchema dc = (DataContainerSchema) o;
        if (this.nElements != dc.nElements) return false;
        if (!this.name.equals(dc.name)) return false;
        if (this.userData != null && dc.userData != null) {
            if (!Arrays.deepEquals(this.userData, dc.userData)) return false;
        } else if (this.userData != dc.userData) {
            return false;
        }
        if (!this.componentSchemas.equals(dc.componentSchemas)) return false;
        if (!this.pseudoComponentSchemas.equals(dc.pseudoComponentSchemas)) return false;
        return true;
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.name);
        hash = 89 * hash + (int) (this.nElements ^ (this.nElements >>> 32));
        hash = 89 * hash + Arrays.deepHashCode(this.userData);
        hash = 89 * hash + Objects.hashCode(this.componentSchemas);
        hash = 89 * hash + Objects.hashCode(this.pseudoComponentSchemas);
        return hash;
    }

    /**
     * Returns true if the container is empty, false otherwise.
     *
     * @return true if the container is empty, false otherwise
     */
    public boolean isEmpty()
    {
        return componentSchemas.isEmpty();
    }

    /**
     * Adds new schema at specified position.
     *
     * @param pos position where new schema will be added. If pos is greater or equal current number of schemes,
     *            new schema will be added at the end.
     * @param	s	  new schema, this reference is used internally
     */
    public void addDataArraySchema(int pos, DataArraySchema s)
    {
        componentSchemas.add(pos, s);
    }

    /**
     * Adds new schema at the end of the list.
     *
     * @param	s	new schema, this reference is used internally
     */
    public void addDataArraySchema(DataArraySchema s)
    {
        componentSchemas.add(s);
    }

    /**
     * Removes specified schema.
     *
     * @param s schema
     */
    public void removeDataArraySchema(DataArraySchema s)
    {
        componentSchemas.remove(s);
    }

    /**
     * Add new pseudo-component schema at the end of the list.
     *
     * @param	s	new schema, this reference is used internally
     */
    public void addPseudoComponentSchema(DataArraySchema s)
    {
        pseudoComponentSchemas.add(s);
    }

    /**
     * Removes specified schema.
     *
     * @param s schema
     */
    public void removePseudoComponentSchema(DataArraySchema s)
    {
        pseudoComponentSchemas.remove(s);
    }

    /**
     * Adds schemes extracted from data components.
     *
     * @param	data	list of components
     */
    public void setComponentSchemasFromData(ArrayList<DataArray> data)
    {
        componentSchemas.clear();
        for (int i = 0; i < data.size(); i++)
            componentSchemas.add(data.get(i).getSchema());
    }

    /**
     * Returns schema at specified position.
     *
     * @param i position
     *
     * @return schema
     */
    public DataArraySchema getSchema(int i)
    {
        return componentSchemas.get(i);
    }

    /**
     * Returns schema at specified name.
     *
     * @param name name of requested component
     *
     * @return schema with the specified name, null if no such component exists
     */
    public DataArraySchema getSchema(String name)
    {
        for (DataArraySchema componentSchema : componentSchemas)
            if (name.equals(componentSchema.getName()))
                return componentSchema;
        return null;
    }

    /**
     * Returns a list of schemes. This method returns a reference to the internal list.
     *
     * @return	list of schemes
     */
    public ArrayList<DataArraySchema> getComponentSchemas()
    {
        return componentSchemas;
    }

    /**
     * Returns the number of pseudo-components.
     *
     * @return number of pseudo-components
     */
    public int getNPseudoComponents()
    {
        return pseudoComponentSchemas.size();
    }

    /**
     * Returns a pseudo-component schema at specified position.
     *
     * @param i position
     *
     * @return schema
     */
    public DataArraySchema getPseudoComponentSchema(int i)
    {
        if (i < 0 || i >= pseudoComponentSchemas.size())
            throw new IllegalArgumentException("i < 0 || i >= pseudoComponentSchemas.size()");
        return pseudoComponentSchemas.get(i);
    }

    /**
     * Returns a pseudo-component schema for the given pseudo-component.
     *
     * @param name pseudo-component name
     *
     * @return schema
     */
    public DataArraySchema getPseudoComponentSchema(String name)
    {
        for (DataArraySchema pseudocomponentSchema : pseudoComponentSchemas)
            if (name.equals(pseudocomponentSchema.getName()))
                return pseudocomponentSchema;
        return null;
    }

    /**
     * Returns all pseudo-component schemes.
     *
     * @return	all pseudo-component schemes
     */
    public ArrayList<DataArraySchema> getPseudoComponentSchemas()
    {
        return pseudoComponentSchemas;
    }

    /**
     * Sets the list of component schemes.
     *
     * @param	componentSchemas list of component schemes, this reference is used internally
     */
    public void setComponentSchemas(ArrayList<DataArraySchema> componentSchemas)
    {
        this.componentSchemas = componentSchemas;
    }

    /**
     * Sets the list of pseudo-component schemes.
     *
     * @param	pseudoComponentSchemas list of component schemes, this reference is used internally
     */
    public void setPseudoComponentSchemas(ArrayList<DataArraySchema> pseudoComponentSchemas)
    {
        this.pseudoComponentSchemas = pseudoComponentSchemas;
    }

    /**
     * Compares two DataContainerSchemas. Returns true if all components are compatible in this schema and the input schema, false otherwise.
     *
     * @param	s                    data container to be compared
     * @param	checkComponentNames  flag to include components name checking
     * @param checkComponentRanges flag to include components ranges checking
     *
     * @return	true if all components are compatible in both schemes, false otherwise.
     */
    public boolean isCompatibleWith(DataContainerSchema s, boolean checkComponentNames, boolean checkComponentRanges)
    {
        if (s == null)
            return false;

        if (componentSchemas.size() != s.getComponentSchemas().size())
            return false;
        for (int i = 0; i < componentSchemas.size(); i++)
            if (!componentSchemas.get(i).isCompatibleWith(s.getComponentSchemas().get(i), checkComponentNames, checkComponentRanges))
                return false;
        return true;
    }

    /**
     * Compares two DataContainerSchemas. Returns true if all components are compatible in this schema and the input schema, false otherwise.
     *
     * @param	s                   data container to be compared
     * @param	checkComponentNames flag to include components name checking
     *
     * @return	true if all components are compatible in both schemes, false otherwise.
     */
    public boolean isCompatibleWith(DataContainerSchema s, boolean checkComponentNames)
    {
        return isCompatibleWith(s, checkComponentNames, false);
    }

    /**
     * Compares two DataContainerSchemas. Returns true if all components are compatible in this schema and the input schema, false otherwise.
     *
     * @param	s data container to be compared
     *
     * @return	true if all components are compatible in both schemes, false otherwise.
     */
    public boolean isCompatibleWith(DataContainerSchema s)
    {
        //FIXME: should be false but along with OutFieldVisualisationModule.prepareOutputGeometry
        //(!outField.isStructureCompatibleWith(fieldGeometry.getField());) suspends VisNow
        return isCompatibleWith(s, true);
    }

    /**
     * Compares two DataContainerSchemas.
     * Returns true user defined component attributes are compatible in this schema and in the input schema, false otherwise.
     *
     * @param	s data container to be compared
     * @param   compat a lambda specifying data component equivalence, e.g. 
     * <code>(u, v) -&gt; (u.isNumeric == v.isNumeric) &amp;&amp; (u.getVectorLength() == v.getVectorLength())</code>
     *
     * @return	true if all components are compatible in both schemes, false otherwise.
     */
    public boolean isCompatibleWith(DataContainerSchema s, BiPredicate<DataArraySchema, DataArraySchema> compat)
    {
        if (s.getNComponents() != componentSchemas.size())
            return false;
        for (DataArraySchema componentSchema : componentSchemas) {
            DataArraySchema sSchema = s.getSchema(componentSchema.getName());
            if (sSchema == null || !compat.test(componentSchema, sSchema))
                        return false; 
        }
        return true;
    }

    /**
     * Returns the number of elements in each component.
     *
     * @return number of elements in each component
     */
    public long getNElements()
    {
        return nElements;
    }

    /**
     * Sets the number of elements in each component.
     *
     * @param nElements number of elements
     */
    public void setNElements(long nElements)
    {
        if(this.nElements < 0) {
            if(nElements >= 0) {
                this.nElements = nElements;
            }
            else {
                throw new IllegalArgumentException("The number of elements has to be non-negative.");
            }
        }
        else if (this.nElements != nElements) {
            throw new IllegalArgumentException("The number of elements was already set.");
        }
    }

    /**
     * Returns the name of the container.
     *
     * @return name of the container
     */
    public String getName()
    {
        return name;
    }

    /**
     * Seta the name of the container.
     *
     * @param name new name
     */
    public void setName(String name)
    {
        if (name == null || name.isEmpty()) throw new IllegalArgumentException("Data container name cannot be empty");
        this.name = StringUtils.canonicalizeName(name);
    }

    /**
     * Returns the user data.
     *
     * @return user data
     */
    public String[] getUserData()
    {
        return userData;
    }

    /**
     * Set the user data.
     *
     * @param userData new user data
     */
    public void setUserData(String[] userData)
    {
        this.userData = userData;
    }

    /**
     * Returns the value of user data at specified index.
     *
     * @param index index
     *
     * @return the value of user data at specified index
     */
    public String getUserData(int index)
    {
        return this.userData[index];
    }

    /**
     * Set the value of user data at specified index.
     *
     * @param index    index
     * @param userData new value of user data at specified index
     */
    public void setUserData(int index, String userData)
    {
        this.userData[index] = userData;
    }

    /**
     * Returns the number of components.
     *
     * @return number of components
     */
    public int getNComponents()
    {
        if (componentSchemas == null || componentSchemas.isEmpty())
            return 0;
        return componentSchemas.size();
    }

    /**
     * Returns the schema at specified index.
     *
     * @param i index
     *
     * @return schema at specified index
     */
    public DataArraySchema getComponentSchema(int i)
    {
        if (componentSchemas == null || componentSchemas.isEmpty())
            return null;
        if (i < 0 || i >= componentSchemas.size())
            throw new IllegalArgumentException("i < 0 || i >= componentSchemas.size()");
        return componentSchemas.get(i);
    }

    /**
     * Returns the schema with specified name of component.
     *
     * @param name name of component
     *
     * @return schema with specified name of component
     */
    public DataArraySchema getComponentSchema(String name)
    {
        if (componentSchemas == null || componentSchemas.isEmpty())
            return null;
        for (DataArraySchema schema : componentSchemas)
            if (schema.getName().equals(name))
                return schema;
        return null;
    }

    /**
     * check if at least one of the components is a category type component
     * @return true if there is a category component, false otherwise
     */
    public boolean hasCategoryComponent()
    {
        for (DataArraySchema componentSchema : componentSchemas)
            if (componentSchema.isCategoryArraySchema())
                return true;
        return false;
    }

    /**
     * check if at least one of the components is a cumulated category type component
     * @return true if there is a category component, false otherwise
     */
    public boolean hasCumulatedCategoryComponent()
    {
        for (DataArraySchema componentSchema : componentSchemas)
            if (componentSchema.isCumulatedCategoryArraySchema())
                return true;
        return false;
    }

    /**
     * Returns a deep copy of this instance.
     *
     * @return a deep copy of this instance
     */
    public DataContainerSchema cloneDeep()
    {
        DataContainerSchema clone = new DataContainerSchema(this.name);
        ArrayList<DataArraySchema> componentSchemasClone = new ArrayList<>();
        if (componentSchemas != null && componentSchemas.size() > 0) {
            for (DataArraySchema item : componentSchemas) componentSchemasClone.add(item.cloneDeep());
        }
        clone.componentSchemas = componentSchemasClone;
        clone.nElements = this.nElements;
        ArrayList<DataArraySchema> pseudoComponentSchemasClone = new ArrayList<>();
        if (pseudoComponentSchemas != null && pseudoComponentSchemas.size() > 0) {
            for (DataArraySchema item : pseudoComponentSchemas) pseudoComponentSchemasClone.add(item.cloneDeep());
        }
        clone.pseudoComponentSchemas = pseudoComponentSchemasClone;
        if (userData != null) {
            String[] newUserData = new String[userData.length];
            System.arraycopy(userData, 0, newUserData, 0, newUserData.length);
            clone.setUserData(newUserData);
        }
        return clone;
    }

}
