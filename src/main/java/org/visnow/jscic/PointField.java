/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.cells.SimplexPosition;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.EngineeringFormattingUtils;

/**
 * A data structure composed of scattered (unconnected) nodes with data
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class PointField extends Field {

    private static final long serialVersionUID = 777139305065775296L;

    /**
     * Create a new instance of PointField.
     *
     * @param schema field schema
     */
    public PointField(PointFieldSchema schema) {
        if (schema == null) {
            throw new IllegalArgumentException("schema cannot be null");
        }
        this.schema = schema;
        type = FieldType.FIELD_POINT;
        timeCoords = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        coordsTimestamp = System.nanoTime();
        timestamp = System.nanoTime();
    }

    /**
     * Create a new instance of PointField.
     *
     * @param nNodes number of nodes
     */
    public PointField(long nNodes) {
        if (nNodes <= 0) {
            throw new IllegalArgumentException("The number of nodes must be positive.");
        }
        schema = new PointFieldSchema();
        schema.setNElements(nNodes);
        type = FieldType.FIELD_POINT;
        timeCoords = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        timestamp = coordsTimestamp = System.nanoTime();
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        if (isLarge()) {
            s.append("Large ");
        }
        s.append(String.format("point field, %d nodes, ", getNElements()));
        if (getNFrames() > 1) {
            s.append(getNFrames()).append(" time frames, ");
        }
        s.append(components.size()).append(" data components");
        return s.toString();
    }

    @Override
    public String toMultilineString() {
        StringBuilder s = new StringBuilder();
        if (isLarge()) {
            s.append(String.format("Large point field, %d nodes", getNElements()));
        } else {
            s.append(String.format("Point field, %d nodes", getNElements()));
        }
        if (getNFrames() > 1) {
            s.append(getNFrames()).append("<p> time frames, ");
        }
        s.append(components.size()).append(" data components");
        return s.toString();
    }

    @Override
    public String shortDescription() {
        StringBuilder s = new StringBuilder();
        s.append("<html>");
        s.append(getNElements()).append(" nodes");

        if (getNFrames() > 1) {
            s.append("<br>").append(getNFrames()).append(" timesteps");
        }
        s.append("<br>").append(components.size()).append(" components");
        s.append("</html>");
        return s.toString();
    }
    
    
    @Override
    public String conciseDescription()
    {
        StringBuffer s = new StringBuffer();
        s.append("<p>Name: ").append(schema.getName()).append("</p>");
        if (isLarge())
            s.append("Large ");        
        s.append("Point field, ");
        s.append(NumberFormat.getNumberInstance(Locale.US).format(getNElements())).append(" nodes");
        if (trueNSpace > 0) {
            s.append(", true ").append(trueNSpace).append("-dim ");
        }
        if (getNFrames() > 1) 
            s.append(", ").append(getNFrames()).append(" timesteps");
        s.append("<p>");
        
        s.append("<p>Geometric extents:</p>");
        s.append(asXYZTable(getPreferredExtents(), null, 3, 2, true, true));
        
        if (components.size() > 0) {
            s.append("<p><BR>Node data components:");
            s.append("<TABLE border=\"0\" cellspacing=\"5\">");
                s.append("<TR valign='top' align='right'><TD align = 'left'>Name</td><TD>Vector<br/>length</td><td>Type</td></tr>");
            
            for (int i = 0; i < components.size(); i++) {
                s.append(getComponent(i).conciseDescription());
            }
            s.append("</TABLE>");
        }
        
        return "<html>" + s + "</html>";
    }



    @Override
    public String description() {
        return description(false);
    }

    @Override
    public String description(boolean debug) {
        StringBuffer s = new StringBuffer();
        s.append("<p>Name: ").append(schema.getName()).append("</p>");
        if (isLarge()) {
            s.append("Large ");
        }
        s.append("Point field, ");
        s.append(NumberFormat.getNumberInstance(Locale.US).format(getNElements())).append(" nodes");
        if (getNFrames() > 1) {
            s.append(", ").append(getNFrames()).append(" timesteps<p>");
            s.append("Time range ");
            s.append(EngineeringFormattingUtils.formatHtml(getStartTime()));
            if (!isTimeUnitless()) {
                s.append(" ").append(getTimeUnit());
            }
            s.append(":");
            s.append(EngineeringFormattingUtils.formatHtml(getEndTime()));
            if (!isTimeUnitless()) {
                s.append(" ").append(getTimeUnit());
            }
            s.append("</p><p>Current time: ");
            s.append(EngineeringFormattingUtils.formatHtml(getCurrentTime()));
            if (!isTimeUnitless()) {
                s.append(" ").append(getTimeUnit());
            }
            s.append("</p>");
        }

        if (schema.getUserData() != null) {
            s.append("<p>user data:");
            for (String u : schema.getUserData()) {
                s.append("<p>").append(u);
            }
        }

        if (timeCoords.getNSteps() > 1)
            s.append("<p>" + timeCoords.getNSteps() + " coordinate time steps</p>");

        if (timeMask != null && !timeMask.isEmpty()) {
            s.append("<p>with mask</p>");
            if (timeMask.getNSteps() > 1)
                s.append("<p>" + timeMask.getNSteps()   + " mask time steps</p>");
        }
        
        s.append("<TABLE border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
        s.append("<tr valign=\"bottom\">");
        s.append("<td align='left'>" + "&nbsp;&nbsp;").append("Geometric extents").append(":&nbsp;</td>");
        s.append("<td align='left'>" + "&nbsp;&nbsp;").append("Physical extents").append(":&nbsp;</td>");
        if (debug) {
            s.append("<td align='left'>" + "&nbsp;&nbsp;").append("True geometric extents").append(":&nbsp;</td>");
            s.append("<td align='left'>" + "&nbsp;&nbsp;").append("True physical extents").append(":&nbsp;</td>");
        }
        s.append("</tr>");
        s.append("<tr valign=\"top\">");
        s.append("<td align='left'>" + "&nbsp;&nbsp;");
        s.append(asXYZTable(getPreferredExtents(), null, 3, 2, true, true));
        s.append(":&nbsp;</td>");
        s.append("<td align='left'>" + "&nbsp;&nbsp;");
        String[] axisLabels = getAxesNames().clone();
        if(!axisLabels[0].equals("x"))
            axisLabels[0] += " (x)";
        if(!axisLabels[1].equals("y"))
            axisLabels[1] += " (y)";
        if(!axisLabels[2].equals("z"))
            axisLabels[2] += " (z)";
        s.append(asTable(getPreferredPhysicalExtents(), getCoordsUnits(), axisLabels, 3, 2, true, true));
        s.append(":&nbsp;</td>");
        if (debug) {
            s.append("<td align='left'>" + "&nbsp;&nbsp;");
            s.append(asXYZTable(getExtents(), null, 3, 2, true, true));
            s.append(":&nbsp;</td>");
            s.append("<td align='left'>" + "&nbsp;&nbsp;");
            s.append(asXYZTable(getPhysicalExtents(), null, 3, 2, true, true));
            s.append(":&nbsp;</td>");
        }
        s.append("</tr>");
        s.append("</TABLE>");
        
        if (!components.isEmpty()) {
            s.append("<p><BR>Node data components:");
            s.append("<TABLE border=\"0\" cellspacing=\"5\">");
        if (debug) {
            s.append("<TR valign='top' align='right'>").
              append("<TD align = 'left'>Name</td><TD>Vector<br/>length</td><td>Type</td><td>Time<br/>steps</td>").
              append("<td>Min</td><td>Max</td><td>Physical<br/>min</td><td>Physical<br/>max</td>").
              append("<td>Preferred<br/>min</td><td>Preferred<br/>max</td>").
              append("<td>Preferred<br/>physical<br/>min</td><td>Preferred<br/>physical<br/>max</td>").
              append("<td>Unit</td><td align=\"left\" valign=\"top\">User data</td></tr>");
        } else {
            s.append("<TR valign='top' align='right'>").
              append("<TD align = 'left'>Name</td><TD>Vector<br/>length</td><td>Type</td><td>Time<br/>steps</td>").
              append("<td>Min</td><td>Max</td><td>Physical<br/>min</td><td>Physical<br/>max</td><td>Unit</td></tr>");
        }
            for (int i = 0; i < components.size(); i++) {
                s.append(getComponent(i).description(debug));
            }
            s.append("</TABLE>");
        }
        return "<html>" + s + "</html>";
    }

    @Override
    public PointField cloneShallow() {
        PointField clone = new PointField((PointFieldSchema) schema.cloneDeep());
        clone.type = type;
        clone.geoTree = this.geoTree != null ? this.geoTree.cloneShallow() : null;
        clone.axesNames = this.axesNames != null ? this.axesNames.clone() : null;
        clone.statisticsComputed = this.statisticsComputed;
        clone.trueNSpace = this.trueNSpace;

        if (this.timeCoords != null && !timeCoords.isEmpty()) {
            clone.setCoords(this.timeCoords.cloneShallow());
        }
        if (this.timeMask != null && !timeMask.isEmpty()) {
            clone.setMask(this.timeMask.cloneShallow());
        }

        if (components != null && !components.isEmpty()) {
            ArrayList<DataArray> componentsClone = new ArrayList<>();
            for (DataArray dataArray : components) {
                componentsClone.add(dataArray.cloneShallow());
            }
            clone.components = componentsClone;
        }
        clone.setCurrentTime(this.currentTime);
        clone.coordsTimestamp = this.coordsTimestamp;
        clone.maskTimestamp = this.maskTimestamp;
        clone.timestamp = this.timestamp;
        return clone;
    }

    @Override
    public Field cloneDeep() {
        PointField clone = new PointField((PointFieldSchema) schema.cloneDeep());
        clone.type = type;
        clone.geoTree = this.geoTree != null ? this.geoTree.cloneDeep() : null;
        clone.axesNames = this.axesNames != null ? this.axesNames.clone() : null;
        clone.statisticsComputed = this.statisticsComputed;
        clone.trueNSpace = this.trueNSpace;

        if (this.timeCoords != null && !timeCoords.isEmpty()) {
            clone.setCoords(this.timeCoords.cloneDeep());
        }
        if (this.timeMask != null && !timeMask.isEmpty()) {
            clone.setMask(this.timeMask.cloneDeep());
        }

        if (components != null && !components.isEmpty()) {
            ArrayList<DataArray> componentsClone = new ArrayList<>();
            for (DataArray dataArray : components) {
                componentsClone.add(dataArray.cloneDeep());
            }
            clone.components = componentsClone;
        }
        clone.setCurrentTime(this.currentTime);
        clone.coordsTimestamp = this.coordsTimestamp;
        clone.maskTimestamp = this.maskTimestamp;
        clone.timestamp = this.timestamp;
        return clone;
    }

    @Override
    public boolean isStructureCompatibleWith(Field f) {
        return f instanceof PointField && f.getNNodes() == getNNodes();
    }

    @Override
    public boolean isDataCompatibleWith(Field f) {
        return f != null && f instanceof PointField && super.isDataCompatibleWith(f);
    }

    @Override
    public PointFieldSchema getSchema()
    {
        return (PointFieldSchema)schema;
    }

    /**
     * If possible (i.e. if this field is small, a trivial irregular field with
     * the point cells (zero-dimensional simplices) only is returned. If this
     * field is large, nothing can be done and a corresponding exception is
     * thrown.
     *
     * @return this field "triangulated" i.e. converted to irregular field with
     * point cells if possible
     */
    @Override
    public IrregularField getTriangulated() throws IllegalArgumentException {
        return this.toIrregularField();
    }

    /**
     * If the field is not Large, a trivial irregular field is created by adding
     * a single cell set containing only a trivial POINT cell array. Large
     * irregular fields are not supported currently, thus an exception is
     * thrown.
     *
     * @return this field "triangulated" i.e. converted to irregular field with
     * point cells if possible
     */
    public IrregularField toIrregularField() {
        if (isLarge()) {
            throw new IllegalArgumentException("Large field can not be converted to IrregularField");
        }
        IrregularField triangulated = new IrregularField(getNNodes());
        triangulated.setCoords(getCoords().cloneShallow());
        if (getMask() != null) {
            triangulated.setMask(getMask().cloneShallow());
        }
        for (DataArray component : components) {
            triangulated.addComponent(component.cloneShallow());
        }
        int[] indices = new int[(int) getNNodes()];
        for (int i = 0; i < indices.length; i++) {
            indices[i] = i;
        }
        CellArray pts = new CellArray(CellType.POINT, indices, null, null);
        CellSet cs = new CellSet("dummy");
        cs.setCellArray(pts);
        triangulated.addCellSet(cs);
        return triangulated;
    }

    @Override
    public DataArray interpolateDataToMesh(Field mesh, DataArray da) {
        throw new UnsupportedOperationException("No default interpolation available");
    }

    /**
     * Currently, the construction of the geo tree as intended for regular and
     * irregular fields with positive trueNSpace is pointless. The geo tree is a
     * tool for fast finding a cell containing a given point - in the case of
     * the PointField that would mean finding a node identical to the argument.
     * Thus (perhaps) a specific point geo tree structure could be inserted here
     * but its functionality would be different of that of geo tree
     */
    @Override
    public void createGeoTree() {
    }

    /**
     * Since the PointField has only zero-dimensional cells, the only possible
     * implementation of this abstract method would be a check if p is a node in
     * the field - at present, I believe it is useless.
     *
     * @return Since there are formally no cells in the point field, null is
     * always returned.
     */
    @Override
    public SimplexPosition getFieldCoords(float[] p) {
        return null;
    }

    /**
     * Since the PointField has only zero-dimensional cells, the only possible
     * implementation of this abstract method would be a check if p is a node in
     * the field - at present, I believe it is useless.
     *
     * @return Since there are formally no cells in the point field, false is
     * always returned.
     */
    @Override
    public boolean getFieldCoords(float[] p, SimplexPosition result) {
        return false;
    }

    /**
     * trueNSpace &ge; 0 means that the field has cells of dimensions trueNSpace
     * and is contained in subspace spanned by the first trueNSpace coordinates,
     * e.g trueNSpace = 2 if and only if the field is contained in the x0,x1
     * plane and has triangle or quad cells. Thus, since the point field has
     * only separate points (0-dim cells) the only case when trueNSpace could be
     * 0 is when the field has only one node of coordinates (0,0,0)
     */
    @Override
    public void checkTrueNSpace() {
        this.trueNSpace = -1;
    }

    @Override
    public LongLargeArray getIndices(int axis) {
        LongLargeArray out = new LongLargeArray(getNElements(), false);
        for (long i = 0; i < out.length(); i++) {
            out.setLong(i, i);
        }
        return out;
    }

}
